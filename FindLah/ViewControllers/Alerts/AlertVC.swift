//
//  AlertVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 23/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

protocol AlertDelegate {
    func btnYes(type: String)
    func btnNo(type: String)
    func btnOk(type: String)
}

class AlertVC: UIViewController {

    var AlertDelegate: AlertDelegate?
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var btnNo: UIButton!
    @IBOutlet var btnOk: UIButton!
    
    var strTitle: String = ""
    var strDescription: String = ""
    var isHideOk: Bool = false
    var strUserType = ""
    var type: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if strUserType == FINDER {
            
        } else if strUserType == SEEKER || AppUtilities.sharedInstance.userType == SEEKER {
            btnOk.backgroundColor = REDCOLOR
            btnYes.backgroundColor = REDCOLOR
            btnNo.backgroundColor = REDCOLOR
            lblTitle.textColor = REDCOLOR
        }
        lblTitle.text = strTitle
        lblDescription.text = strDescription
        if isHideOk {
            btnOk.isHidden = true
        }
        
        if type == "needlogin" {
            btnYes.setTitle("Login Now", for: .normal)
            btnNo.setTitle("Not Now", for: .normal)
        }
        if type == "payreward" {
            btnOk.setTitle("Pay Reward", for: .normal)
        }
        
        if type == "forpush" {
            btnYes.setTitle("Settings", for: .normal)
            btnNo.setTitle("Cancel", for: .normal)
        }
    }
    
    @IBAction func btnYes(_ sender: Any) {
        self.AlertDelegate?.btnYes(type: type)
    }
    
    @IBAction func btnNo(_ sender: Any) {
        self.AlertDelegate?.btnNo(type: type)
    }
    
    @IBAction func btnOk(_ sender: Any) {
        self.AlertDelegate?.btnOk(type: type)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
