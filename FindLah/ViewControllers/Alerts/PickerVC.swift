//
//  PickerVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 17/08/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

protocol pickerVCDelegate {
    func btnSave(arrDetail: NSMutableArray,type: String)
    func btnCancel(type: String)
}

class PickerVC: UIViewController {

    var pickerVCDelegate : pickerVCDelegate?
    @IBOutlet var tblList: UITableView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblMsg: UILabel!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnSave: UIButton!
    
    var strUserType = ""
    var listType = ""
    var arrData: NSMutableArray = NSMutableArray()
    var arrSelectedData: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if strUserType == SEEKER || AppUtilities.sharedInstance.userType == SEEKER {
            btnCancel.backgroundColor = REDCOLOR
            btnSave.backgroundColor = REDCOLOR
            lblTitle.textColor = REDCOLOR
        }
        
        lblMsg.text = ""
        if listType == "categorymainlist" || listType == "categoryforfilter" {
            lblTitle.text = "Category"
        } else if listType == "categorysublist" {
            lblMsg.text = "Select up to two sub-categories."
            lblTitle.text = "Categories"
        } else if listType == "regionlist" || listType == "regionforfilter" {
            lblTitle.text = "Region"
        } else if listType == "banklist" {
            lblTitle.text = "Bank"
        } else if listType == "popstation" {
            lblTitle.text = "POPStation"
        }
        
        if listType == "popstation" {
            tblList.register(UINib(nibName: "POPStationCell", bundle: nil), forCellReuseIdentifier: "POPStationCell")
        } else {
            tblList.register(UINib(nibName: "PickerCell", bundle: nil), forCellReuseIdentifier: "PickerCell")
        }
        
    }

    @IBAction func btnCancel(_ sender: Any) {
        self.pickerVCDelegate?.btnCancel(type: listType)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        self.pickerVCDelegate?.btnSave(arrDetail: arrSelectedData, type: listType)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension PickerVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if listType == "popstation" {
            let cell : POPStationCell = tableView.dequeueReusableCell(withIdentifier: "POPStationCell", for: indexPath) as! POPStationCell
            cell.imgCheck.image = UIImage(named:"ic_uncheked")
            let dictData = arrData.object(at: indexPath.row) as! NSDictionary
            print(dictData)
            if arrSelectedData.contains(dictData) {
                cell.imgCheck.image = UIImage(named:"ic_checked")
            }
//            cell.imgPOPStation.sd_setImage(with:URL(string:dictData.object(forKey: "popstation_image") as! String), placeholderImage:nil, options:.continueInBackground)
            cell.imgPOPStation.setImageWith(URL.init(string:dictData.object(forKey: "popstation_image") as! String), placeholderImage: nil, options: .continueInBackground, completed: { (img, error, cachType, url) in
                if(img == nil)
                {
                    cell.imgPOPStation.image = nil
                }
                else
                {
                    cell.imgPOPStation.image = img
                }
            }, usingActivityIndicatorStyle: .gray)
            cell.lblPOPTitle.text = dictData.object(forKey: "popstation_name") as? String
            cell.lblPOPAddress.text = dictData.object(forKey: "popstation_address") as? String
            cell.lblDetails.text = dictData.object(forKey: "popstation_desc") as? String
            
            let arrDaysAvailable: NSMutableArray = NSMutableArray()
            let dictAvailableDays = dictData.object(forKey: "popstation_days_available") as! NSDictionary
            let arrKeys = dictAvailableDays.allKeys as NSArray
            for i in 0..<arrKeys.count {
                let strKey = arrKeys.object(at: i) as! String
                let dictDay = dictAvailableDays.object(forKey: strKey) as! NSDictionary
                let strTime = String(format:"%@ - %@",dictDay.object(forKey: "open") as? String ?? "0", dictDay.object(forKey: "close") as? String ?? "0")
                let strAvailabelDays = String(format: "%@ :\n   %@\n", strKey.uppercased(),strTime)
                arrDaysAvailable.add(strAvailabelDays)
            }
            print(arrDaysAvailable)
            cell.lblOpenCloseTime.text = arrDaysAvailable.componentsJoined(by: "\n")
            
            //cell.btnPopImage.addTarget(self, action: #selector(btnPopImage(sender:)), for: .touchUpInside)
            
            return cell
        } else {
            let cell : PickerCell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath) as! PickerCell
            
            cell.imgCheck.image = UIImage(named:"ic_uncheked")
            
            if listType == "categorymainlist" || listType == "categorysublist" || listType == "categoryforfilter" || listType == "banklist" {
                let dictData = arrData.object(at: indexPath.row) as! NSDictionary
                if arrSelectedData.contains(dictData) {
                    if AppUtilities.sharedInstance.userType == SEEKER {
                        cell.imgCheck.image = UIImage(named:"redcheck")
                    } else {
                        cell.imgCheck.image = UIImage(named:"ic_checked")
                    }
                }
                if listType == "banklist" {
                    cell.lblName.text = dictData.object(forKey: "bank_name") as? String
                } else {
                    cell.lblName.text = dictData.object(forKey: "category_name") as? String
                }
            } else if listType == "regionlist" || listType == "regionforfilter" {
                cell.lblName.text = arrData.object(at: indexPath.row) as? String
                if arrSelectedData.contains(cell.lblName.text!) {
                    if AppUtilities.sharedInstance.userType == SEEKER {
                        cell.imgCheck.image = UIImage(named:"redcheck")
                    } else {
                        cell.imgCheck.image = UIImage(named:"ic_checked")
                    }
                }
            } else if listType == "regionforfilter" {
                cell.lblName.text = arrData.object(at: indexPath.row) as? String
                if arrSelectedData.contains(cell.lblName.text!) {
                    if strUserType == SEEKER {
                        cell.imgCheck.image = UIImage(named:"redcheck")
                    } else {
                        cell.imgCheck.image = UIImage(named:"ic_checked")
                    }
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if listType == "regionlist" {
            arrSelectedData = NSMutableArray.init()
            arrSelectedData.add(arrData.object(at: indexPath.row) as! String)
            tblList.reloadData()
            return
        }
        
        if listType == "regionforfilter" {
            let strRegion = arrData.object(at: indexPath.row) as! String
            if arrSelectedData.contains(strRegion) {
                arrSelectedData.remove(strRegion)
            } else {
                arrSelectedData.add(strRegion)
            }
            tblList.reloadData()
            return
        }
        
        let dictData = arrData.object(at: indexPath.row) as! NSDictionary
        
        if listType == "categoryforfilter" {
            if arrSelectedData.contains(dictData) {
                arrSelectedData.remove(dictData)
            } else {
                arrSelectedData.add(dictData)
            }
            tblList.reloadData()
            return
        }
        if listType == "categorymainlist" || listType == "banklist" || listType == "popstation" {
            if listType == "categorymainlist" {
                if arrSelectedData.contains(dictData) {
                    arrSelectedData.remove(dictData)
                } else {
                    arrSelectedData = NSMutableArray.init()
                    arrSelectedData.add(dictData)
                }
            } else {
                arrSelectedData = NSMutableArray.init()
                arrSelectedData.add(dictData)
            }
            
            DispatchQueue.main.async {
                self.tblList.reloadData()
            }
            
            return
        }
        
        if arrSelectedData.contains(dictData) {
            arrSelectedData.remove(dictData)
        } else {
            if listType == "categorysublist" {
                if arrSelectedData.count >= 2 { return }
            }
            arrSelectedData.add(dictData)
        }
        tblList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if listType == "popstation" {
            return UITableViewAutomaticDimension
        }
        return 60
    }
    
    /*
    @objc func btnPopImage(sender:UIButton) {
        let point = tblList.convert(CGPoint.zero, from: sender)
        let indexPath = tblList.indexPathForRow(at: point)
        
        let dictData = arrData.object(at: (indexPath?.row)!) as! NSDictionary
        let strAttachmentUrl = dictData.object(forKey: "popstation_image") as! String
        
        let imageInfo = JTSImageInfo()
        imageInfo.imageURL = URL(string: strAttachmentUrl)
        imageInfo.referenceRect = sender.frame
        imageInfo.referenceView = sender.superview
        let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .image, backgroundStyle: .blurred)
        imageViewer?.show(from: self, transition: .fromOriginalPosition)
    }*/
}
