//
//  WhoBuddyVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 18/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

protocol whoBuddyDelegate {
    func btnDone()
    func btnCancelSelection()
}

class WhoBuddyVC: UIViewController {

    var whoBuddyDelegate: whoBuddyDelegate?

    @IBOutlet var btnFounders1: UIButton!
    @IBOutlet var btnLosters1: UIButton!
    @IBOutlet var lblFounders1: UILabel!
    @IBOutlet var lblLosters1: UILabel!

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnDone: UIButton!
    
    @IBOutlet var btnCancel: UIButton!
    var UNSELECTEDCOLOR: UIColor = Color_Hex(hex: "DEDEDE")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AppUtilities.sharedInstance.userType == FINDER {
            btnFounders(btnFounders1)
        } else if AppUtilities.sharedInstance.userType == SEEKER {
            btnLosters(btnLosters1)
        } else {
            btnFounders(btnFounders1)
        }
    }

    
    @IBAction func btnFounders(_ sender: UIButton) {
        sender.isSelected = true
        lblTitle.textColor = BLUECOLOR
        btnDone.backgroundColor = BLUECOLOR
        btnCancel.backgroundColor = BLUECOLOR
        btnFounders1.isSelected = true
        btnLosters1.isSelected = false
        lblLosters1.textColor = UNSELECTEDCOLOR
        lblFounders1.textColor = BLUECOLOR
    }
    
    @IBAction func btnLosters(_ sender: UIButton) {
        sender.isSelected = true
        lblTitle.textColor = REDCOLOR
        btnDone.backgroundColor = REDCOLOR
        btnCancel.backgroundColor = REDCOLOR
        btnFounders1.isSelected = false
        btnLosters1.isSelected = true
        lblLosters1.textColor = REDCOLOR
        lblFounders1.textColor = UNSELECTEDCOLOR
    }
    
    @IBAction func btnDone(_ sender: Any) {
        if btnFounders1.isSelected {
            AppUtilities.sharedInstance.userType = FINDER
        } else if btnLosters1.isSelected {
            AppUtilities.sharedInstance.userType = SEEKER
        }
        
        AppUtilities.sharedInstance.saveData(data: AppUtilities.sharedInstance.userType as AnyObject, key: kUserType)
        setCursorColor()
        self.whoBuddyDelegate?.btnDone()
    }

    @IBAction func btnCancel(_ sender: Any) {
        self.whoBuddyDelegate?.btnCancelSelection()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
