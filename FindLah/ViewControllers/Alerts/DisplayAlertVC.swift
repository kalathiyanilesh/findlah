//
//  DisplayAlertVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 23/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

protocol displayAlertDelegate {
    func btnYes(type: String)
    func btnNo(type: String)
    func btnOk(type: String)
}

class DisplayAlertVC: UIViewController {

    var displayAlertDelegate: displayAlertDelegate?
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var btnNo: UIButton!
    @IBOutlet var btnOk: UIButton!
    
    var strTitle: String = ""
    var strDescription: String = ""
    var isHideOk: Bool = false
    var strUserType = ""
    var type: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if strUserType == FINDER {
            
        } else if strUserType == SEEKER || AppUtilities.sharedInstance.userType == SEEKER {
            btnOk.backgroundColor = REDCOLOR
            btnYes.backgroundColor = REDCOLOR
            btnNo.backgroundColor = REDCOLOR
            lblTitle.textColor = REDCOLOR
        }
        
        lblTitle.text = strTitle
        lblDescription.text = strDescription
        if isHideOk {
            btnOk.isHidden = true
        }
        
//        if type == "paymentsuccess" {
//            lblDescription.font = UIFont(name: "Montserrat-Medium", size: 19)
//        }
    }

    @IBAction func btnYes(_ sender: Any) {
        self.displayAlertDelegate?.btnYes(type: type)
    }
    
    @IBAction func btnNo(_ sender: Any) {
        self.displayAlertDelegate?.btnNo(type: type)
    }
    
    @IBAction func btnOk(_ sender: Any) {
        self.displayAlertDelegate?.btnOk(type: type)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
