//
//  POPStationCell.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 29/09/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class POPStationCell: UITableViewCell {

    @IBOutlet var imgCheck: UIImageView!
    @IBOutlet var imgPOPStation: UIImageView!
    @IBOutlet var lblPOPTitle: UILabel!
    @IBOutlet var lblPOPAddress: UILabel!
    @IBOutlet var lblDetails: UILabel!
    @IBOutlet var lblOpenCloseTime: UILabel!
   // @IBOutlet var btnPopImage: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
