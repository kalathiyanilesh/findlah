//
//  LostFindCell.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 17/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class LostFindCell: UITableViewCell {

    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var imgItem: UIImageView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var btnDetail: UIButton!
    @IBOutlet var imgDetail: UIImageView!
    @IBOutlet var lblReward: UILabel!
    @IBOutlet var lblTimeAgo: UILabel!
    @IBOutlet var btnMore: UIButton!
    @IBOutlet var imgReward: UIImageView!
    @IBOutlet var lblImageType: UILabel!
    @IBOutlet var lblTrackNo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setDataToRow(dictReport: NSDictionary) {
//        self.imgItem.sd_setImage(with:URL(string:dictReport.object(forKey: "report_image_1") as! String), placeholderImage:nil, options:.continueInBackground)
        self.imgItem.setImageWith(URL.init(string:dictReport.object(forKey: "report_image_1") as! String), placeholderImage: nil, options: .continueInBackground, completed: { (img, error, cachType, url) in
            if(img == nil)
            {
                self.imgItem.image = nil
            }
            else
            {
                self.imgItem.image = img
            }
        }, usingActivityIndicatorStyle: .gray)
        
//        self.imgUser.sd_setImage(with:URL(string:dictReport.object(forKey: "profile_img") as! String), placeholderImage:UserProfilePlaceHolder, options:.continueInBackground)
        self.imgUser.setImageWith(URL.init(string:dictReport.object(forKey: "profile_img") as! String), placeholderImage: nil, options: .continueInBackground, completed: { (img, error, cachType, url) in
            if(img == nil)
            {
                self.imgUser.image = nil
            }
            else
            {
                self.imgUser.image = img
            }
        }, usingActivityIndicatorStyle: .gray)
        
        self.lblTrackNo.text = "Tracking no: \(dictReport.object(forKey: "tracking_no") as? String ?? "")"
        self.lblUserName.text = dictReport.object(forKey: "name") as? String
        
        self.lblTitle.text = dictReport.object(forKey: "title") as? String
        self.lblDescription.text = dictReport.object(forKey: "description") as? String
        
        self.lblReward.text = "$0"
        if dictReport.object(forKey: "is_reward") as! String == "1" {
            self.lblReward.text = "$\(dictReport.object(forKey: "amount") as? String ?? "0")"
        }
        
        let strDate = dictReport.object(forKey: "creation_datetime") as? String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: strDate!)
        
        self.lblTimeAgo.text = timeAgoSinceDate(date: date!, numericDates: true)
        
        if dictReport.object(forKey: "ri1_is_actual") as! String == "1" {
            self.lblImageType.text = "Actual"
        } else {
            self.lblImageType.text = "Reference"
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
