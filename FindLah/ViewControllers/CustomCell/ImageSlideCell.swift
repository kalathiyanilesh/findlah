//
//  ImageSlideCell.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 15/08/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class ImageSlideCell: UICollectionViewCell {

    @IBOutlet var imgItem: UIImageView!
    @IBOutlet var viewUpload: UIView!
    @IBOutlet var viewImage: UIView!
    @IBOutlet var lblUploadDesc: UILabel!
    @IBOutlet var btnAddImage: UIButton!
    @IBOutlet var lblImageType: UILabel!
    @IBOutlet var imgBackCell: UIImageView!
    @IBOutlet var btnActual: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
