//
//  PickerCell.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 17/08/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class PickerCell: UITableViewCell {

    @IBOutlet var imgCheck: UIImageView!
    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var imgPOPStation: UIImageView!
    @IBOutlet var lblPOPTitle: UILabel!
    @IBOutlet var lblPOPAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
