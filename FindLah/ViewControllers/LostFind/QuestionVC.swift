//
//  QuestionVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 19/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

protocol delegateDetail {
    func navigateToBack()
}

protocol delegateUpdateStatus {
    func updateStatus(dictStatus: NSDictionary, index: Int)
}

class QuestionVC: UIViewController {

    var pageMenu : CAPSPageMenu?
    var delegateDetail: delegateDetail?
    var delegateUpdateStatus: delegateUpdateStatus?
    
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var viewQuestion: UIView!
    @IBOutlet var lblQuestionNo: UILabel!
    @IBOutlet var lblTotalQuestion: UILabel!
    @IBOutlet var viewThankYou: UIView!
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblTopTitle: UILabel!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnPrevious: UIButton!
    @IBOutlet var lblSmallLine: UILabel!
    @IBOutlet var btnSubmit: UIButton!
    
    var index: Int = 0
    var isCallApi: Bool = false
    var isUpdate: Bool = false
    var isFromDetail: Bool = false
    var dictReport: NSDictionary = NSDictionary()
    var isFromFinder: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(dictReport)
        viewThankYou.alpha = 0
        
        AppUtilities.sharedInstance.arrQuesAns = NSMutableArray.init()
        for i in 1..<6 {
            let strQusKey = String(format:"hq_%d",i)
            let strQuestion = dictReport.object(forKey: strQusKey) as! String
            if strQuestion.count > 0 {
                let dictQA: NSMutableDictionary = NSMutableDictionary()
                dictQA.setObject(strQuestion, forKey: "question" as NSCopying)
                dictQA.setObject("", forKey: "answer" as NSCopying)
                AppUtilities.sharedInstance.arrQuesAns.add(dictQA)
            }
        }
        
        btnSubmit.isHidden = AppUtilities.sharedInstance.arrQuesAns.count == 1 ? false : true

        lblTotalQuestion.text = "TO \(AppUtilities.sharedInstance.arrQuesAns.count)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
        imgBack.image = AppUtilities.sharedInstance.userType == SEEKER ? UIImage(named:"img_bg") : UIImage(named:"ic_background")
        if AppUtilities.sharedInstance.userType == SEEKER {
            btnDone.backgroundColor = REDCOLOR
            btnNext.backgroundColor = REDCOLOR
            btnPrevious.backgroundColor = REDCOLOR
            btnSubmit.backgroundColor = REDCOLOR
            lblSmallLine.backgroundColor = REDCOLOR
        }*/
        
        imgBack.image = !isFromFinder ? UIImage(named:"img_bg") : UIImage(named:"ic_background")
        if !isFromFinder {
            btnDone.backgroundColor = REDCOLOR
            btnNext.backgroundColor = REDCOLOR
            btnPrevious.backgroundColor = REDCOLOR
            btnSubmit.backgroundColor = REDCOLOR
            lblSmallLine.backgroundColor = REDCOLOR
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setPageMenu()
    }

    //MARK:- BUTTON ACTION
    @IBAction func btnClose(_ sender: Any) {
        close()
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        self.view.endEditing(true)
        setAnswer()
        checkAndCallAnsQuesReq()
    }
    
    @IBAction func btnNext(_ sender: Any) {
        self.view.endEditing(true)
        setAnswer()
        pageMenu?.moveToPage((pageMenu?.currentPageIndex)!+1)
        lblQuestionNo.text = String(format:"%02d",(pageMenu?.currentPageIndex)!+1)
        setQuestion()
    }
    
    func setAnswer() {
        AppUtilities.sharedInstance.isPostAns = true
        NotificationCenter.default.post(name: Notification.Name("setAnswer"), object: (pageMenu?.currentPageIndex)!)
    }
    
    @IBAction func btnPrevious(_ sender: Any) {
        btnNext.setTitle("Next", for: .normal)
        isCallApi = false
        pageMenu?.moveToPage((pageMenu?.currentPageIndex)!-1)
        lblQuestionNo.text = String(format:"%02d",(pageMenu?.currentPageIndex)!+1)
    }
    
    @IBAction func btnDone(_ sender: Any) {
        close()
    }
    
    func close() {
        if isUpdate {
            let dictStatus: NSDictionary = ["request_status": "0",
                                            "user_id": AppUtilities.sharedInstance.getUserID()]
            if isFromDetail {
                self.delegateDetail?.navigateToBack()
                self.delegateUpdateStatus?.updateStatus(dictStatus: dictStatus, index: index)
            } else {
                self.delegateUpdateStatus?.updateStatus(dictStatus: dictStatus, index: index)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func setQuestion() {
        btnNext.setTitle("Next", for: .normal)
        if (pageMenu?.currentPageIndex)! >= AppUtilities.sharedInstance.arrQuesAns.count-1 {
            btnNext.setTitle("Submit", for: .normal)
            if isCallApi {
                checkAndCallAnsQuesReq()
            } else {
                isCallApi = true
            }
        } else {
            isCallApi = false
        }
    }
    
    func checkAndCallAnsQuesReq() {
        var anyAns: Bool = false
        for i in 0..<AppUtilities.sharedInstance.arrQuesAns.count {
            let dictData = AppUtilities.sharedInstance.arrQuesAns.object(at: i) as! NSMutableDictionary
            let answer = dictData.object(forKey: "answer") as! String
            if answer.count > 0 {
                anyAns = true
            } else {
                anyAns = false
                break
            }
        }
        if anyAns {
            self.lblTopTitle.text = "Admin Verification.."
            self.imgIcon.image = UIImage(named:"ic_admin")
            UIView.animate(withDuration: 0.35, animations: {
                self.viewThankYou.alpha = 1
            })
            self.sendAddReportQAReq()
        } else {
            self.openAnsRequiredAlert()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension QuestionVC: AlertDelegate {
    func openAnsRequiredAlert() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = "All questions answer must be required."
        vc.type = "ansrequired"
        vc.strUserType = isFromFinder ? FINDER : SEEKER
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "ansrequired" {
                
            }
        }
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
}

extension QuestionVC {
    func sendAddReportQAReq() {
        var strQuesAns = ""
        convert(arrData: AppUtilities.sharedInstance.arrQuesAns) { (strValue) in
            strQuesAns = strValue
        }
        print(strQuesAns)
        let dictParam: NSDictionary = ["user_id":AppUtilities.sharedInstance.getUserID(),
                                       "report_id":dictReport.object(forKey: "report_id") as! String,
                                       "que_ans":strQuesAns]
        
        print(dictParam)
        
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIAddReportQA,
            parameters: dictParam,
            showLoader:false)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    self.isUpdate = true
                    //AppUtilities.sharedInstance.isCallFinderList = true
                    //AppUtilities.sharedInstance.isCallSeekerList = true
                    AppUtilities.sharedInstance.arrQuesAns = NSMutableArray.init()
                }
            }
        }
    }
}

extension QuestionVC: CAPSPageMenuDelegate {
    func setPageMenu() {
        var arrController : [UIViewController] = []
        
        for i in 0..<AppUtilities.sharedInstance.arrQuesAns.count {
            let dict: NSDictionary = AppUtilities.sharedInstance.arrQuesAns.object(at: i) as! NSDictionary
            let askQues = loadVC(strStoryboardId: SB_LOFI, strVCId: idAskQuestionVC) as! AskQuestionVC
            askQues.Height = viewQuestion.frame.size.height
            askQues.strQuestion = dict.object(forKey: "question") as! String
            askQues.strAnswer = dict.object(forKey: "answer") as! String
            askQues.page = i
            arrController.append(askQues)
        }
        
        let parameters: [CAPSPageMenuOption] = [.hideTopMenuBar(true)]
        
        let frame = CGRect(x: 0, y: 0, width: viewQuestion.frame.size.width, height: viewQuestion.frame.size.height)
        
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu?.delegate = self
        pageMenu?.controllerScrollView.isScrollEnabled = false
        self.addChildViewController(pageMenu!)
        viewQuestion.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        //print("willMoveToPage",index)
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        lblQuestionNo.text = String(format:"%02d",index+1)
    }
}
