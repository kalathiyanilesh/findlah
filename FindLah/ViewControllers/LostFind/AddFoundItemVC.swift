//
//  AddFoundItemVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 20/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit
import GooglePlaces

protocol delegateAddEditReport {
    func updateRecord(dictData: NSDictionary, index: Int, isEdit: Bool)
}

class AddFoundItemVC: UIViewController, UIImagePickerControllerDelegate {
    
    var delegateAddEditReport: delegateAddEditReport?
    var dictResponseData: NSDictionary = NSDictionary()

    @IBOutlet var txtDescription: IQTextView!
    @IBOutlet var txtAnswer1: IQTextView!
    @IBOutlet var txtAnswer2: IQTextView!
    @IBOutlet var txtAnswer3: IQTextView!
    @IBOutlet var txtAnswer4: IQTextView!
    @IBOutlet var txtAnswer5: IQTextView!

    @IBOutlet var txtQuestion1: UITextField!
    @IBOutlet var txtQuestion2: UITextField!
    @IBOutlet var txtQuestion3: UITextField!
    @IBOutlet var txtQuestion4: UITextField!
    @IBOutlet var txtQuestion5: UITextField!
    @IBOutlet var txtCategory1: UITextField!
    @IBOutlet var txtCategory2: UITextField!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var txtLocation1: UITextField!
    @IBOutlet var txtLocation2: UITextField!
    @IBOutlet var txtLocation3: UITextField!

    @IBOutlet var viewReward: UIView!
    @IBOutlet var viewAmount: UIView!
    @IBOutlet var viewQuestAns: UIView!
    @IBOutlet var viewLoccation1: UIView!
    @IBOutlet var viewLoccation2: UIView!
    @IBOutlet var viewQus1: UIView!
    @IBOutlet var viewQus2: UIView!
    @IBOutlet var viewQus3: UIView!
    @IBOutlet var viewQus4: UIView!
    @IBOutlet var viewQus5: UIView!
    @IBOutlet var viewAns1: UIView!
    @IBOutlet var viewAns2: UIView!
    @IBOutlet var viewAns3: UIView!
    @IBOutlet var viewAns4: UIView!
    @IBOutlet var viewAns5: UIView!

    @IBOutlet var lblTittle: UILabel!
    @IBOutlet var lblwhenlose: UILabel!

    @IBOutlet var lblDelTypDesc: UILabel!
    
    @IBOutlet var txtCourierUnitNo: UITextField!
    @IBOutlet var viewCourierUnitNo: UIView!
    @IBOutlet var heightCourierMainView: NSLayoutConstraint!
    
    @IBOutlet var heightCourierUnitNo: NSLayoutConstraint!
    @IBOutlet var heightQuesAns: NSLayoutConstraint!
    @IBOutlet var heightReward: NSLayoutConstraint!
    @IBOutlet var heightView1: NSLayoutConstraint!
    @IBOutlet var heightView2: NSLayoutConstraint!
    @IBOutlet var heightView3: NSLayoutConstraint!
    @IBOutlet var heightView4: NSLayoutConstraint!
    @IBOutlet var heightView5: NSLayoutConstraint!
    @IBOutlet var heightLocation: NSLayoutConstraint!
    
    @IBOutlet var imgLoc1: UIImageView!
    @IBOutlet var imgLoc2: UIImageView!
    @IBOutlet var imgLoc3: UIImageView!
    @IBOutlet var imgBack: UIImageView!
    
    @IBOutlet var btnYesReward: UIButton!
    @IBOutlet var btnNoReward: UIButton!
    @IBOutlet var btnAddQuestion: UIButton!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var btnAddLocation: UIButton!
    @IBOutlet var txtFrom: UITextField!
    @IBOutlet var txtTo: UITextField!
    @IBOutlet var txtRegion: UITextField!
    
    @IBOutlet var collImages: UICollectionView!
    
    @IBOutlet var viewDelType: UIView!
    @IBOutlet var yDelType: NSLayoutConstraint!
    @IBOutlet var heightDelType: NSLayoutConstraint!
    @IBOutlet var viewPopStation: UIView!
    @IBOutlet var txtPOPStation: UITextField!
    
    @IBOutlet var btnContactMe: UIButton!
    @IBOutlet var btnPopStation: UIButton!
    @IBOutlet var btnCourier: UIButton!
    
    //FOR COURIER
    
    @IBOutlet var txtCourierAddress: UITextField!
    @IBOutlet var txtCourierDate: UITextField!
    @IBOutlet var txtCourierFromTime: UITextField!
    @IBOutlet var txtCourierToTime: UITextField!
    @IBOutlet var viewCourier: UIView!
    
    var strDelType = DELIVERY_TYPE.COURIER.rawValue
    var lat_courier: Double = 0
    var long_courier: Double = 0
    
    var FromTimeDate: Date = Date()
    var ToTimeDate: Date = Date()
    
    var dictPOPStation: NSDictionary = NSDictionary()
    var arrPopStation: NSMutableArray = NSMutableArray()
    var arrImages: NSMutableArray = NSMutableArray()
    
    var toMinDate: Date = Date()
    var strReportUserType = ""

    var catId_1 = ""
    var catId_2 = ""
    var catId_3 = ""
    
    var question = 1
    var lattitude1: Double = 0
    var longitude1: Double = 0
    var lattitude2: Double = 0
    var longitude2: Double = 0
    var lattitude3: Double = 0
    var longitude3: Double = 0
    var totalHeightQA: CGFloat = 39.0

    var index: Int = 0
    var isEdit: Bool = false
    var textFieldLoc = UITextField()
    var dictReport: NSDictionary = NSDictionary()
    
    @IBOutlet var viewUniteOne: UIView!
    @IBOutlet var heightUnitOne: NSLayoutConstraint!
    @IBOutlet var txtUniteOne: UITextField!
    
    @IBOutlet var viewUniteTwo: UIView!
    @IBOutlet var heightUnitTwo: NSLayoutConstraint!
    @IBOutlet var txtUniteTwo: UITextField!
    
    @IBOutlet var viewUniteThree: UIView!
    @IBOutlet var heightUnitThree: NSLayoutConstraint!
    @IBOutlet var txtUniteThree: UITextField!
 

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        strReportUserType = AppUtilities.sharedInstance.userType
        if isEdit {
            print(dictReport)
            strReportUserType = dictReport.object(forKey: "report_type") as! String
        }
        
        if strReportUserType == SEEKER {
            strDelType = DELIVERY_TYPE.CONTACTME.rawValue
        }
        
        setUpView()
        
        if isEdit {
            btnAddQuestion.isHidden = true
            let dict: NSMutableDictionary = NSMutableDictionary()
            dict.setObject("0", forKey: "id" as NSCopying)
            dict.setObject(UIImage(), forKey: "image" as NSCopying)
            dict.setObject(dictReport.object(forKey: "report_image_1") as! String, forKey: "image_link" as NSCopying)
            if dictReport.object(forKey: "ri1_is_actual") as! String == "1" {
                dict.setObject("Actual", forKey: "type" as NSCopying)
            } else {
                dict.setObject("Reference", forKey: "type" as NSCopying)
            }
            arrImages.insert(dict, at: 0)
            arrImages.removeLastObject()
            //if arrImages.count == 4 { arrImages.removeLastObject() }
            collImages.reloadData()
            
            if let region = dictReport.object(forKey: "region") as? String {
                txtRegion.text = region
            }

            txtDescription.text = dictReport.object(forKey: "description") as? String

            txtFrom.text = dictReport.object(forKey: "from_date") as? String
            txtTo.text = dictReport.object(forKey: "to_date") as? String
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateFormatter.timeZone = TimeZone.current
            toMinDate = dateFormatter.date(from: txtFrom.text!)!
            
            txtCategory1.text = dictReport.object(forKey: "category_name") as? String
  
            let cat2 = dictReport.object(forKey: "category_name_1") as! String
            let cat3 = dictReport.object(forKey: "category_name_2") as! String
            
            let arrCat = NSMutableArray()
            if cat2.count > 0 { arrCat.add(cat2) }
            if cat3.count > 0 { arrCat.add(cat3) }
            txtCategory2.text = (arrCat as NSArray).componentsJoined(by: ", ")
            
            catId_1 = dictReport.object(forKey: "category_id") as! String
            catId_2 = dictReport.object(forKey: "category_id_1") as! String
            catId_3 = dictReport.object(forKey: "category_id_2") as! String
            
            if catId_1.count > 0 {
                let pred : NSPredicate = NSPredicate(format: "category_id = %@", catId_1)
                let arrTemp = AppUtilities.sharedInstance.arrCatHint.filtered(using: pred) as NSArray
                if arrTemp.count > 0 {
                    let dictData = arrTemp.object(at: 0) as! NSDictionary
                    setCatHintQus(dictData: dictData)
                }
            }
            
            print(heightLocation.constant)
            
            txtLocation1.text = dictReport.object(forKey: "address_1") as? String
            txtLocation2.text = dictReport.object(forKey: "address_2") as? String
            txtLocation3.text = dictReport.object(forKey: "address_3") as? String
            
            viewLoccation1.isHidden = true
            viewLoccation2.isHidden = true
            self.heightLocation.constant = 67
            
            if txtLocation2.text?.count > 0 {
                viewLoccation1.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.heightLocation.constant = self.heightLocation.constant + 8 + 39
                    self.view.layoutIfNeeded()
                })
            }
            
            if txtLocation3.text?.count > 0 {
                viewLoccation2.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.heightLocation.constant = self.heightLocation.constant + 8 + 39
                    self.view.layoutIfNeeded()
                })
            }
            
            let strUnitOne = dictReport.object(forKey: "unitno_1") as? String ?? ""
            let strUnitTwo = dictReport.object(forKey: "unitno_2") as? String ?? ""
            let strUnitThree = dictReport.object(forKey: "unitno_3") as? String ?? ""
            
            viewUniteOne.isHidden = true
            viewUniteTwo.isHidden = true
            viewUniteThree.isHidden = true
            
            if txtLocation1.text!.count > 0 {
                txtUniteOne.text = strUnitOne
                txtUniteOne.isEnabled = true
                heightUnitOne.constant = 47
                viewUniteOne.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.heightLocation.constant = self.heightLocation.constant + self.heightUnitOne.constant
                    self.view.layoutIfNeeded()
                })
            } else {
                txtUniteOne.text = ""
                txtUniteOne.isEnabled = false
                heightUnitOne.constant = 0
                viewUniteOne.isHidden = true
            }
            
            if txtLocation2.text!.count > 0 {
                txtUniteTwo.text = strUnitTwo
                txtUniteTwo.isEnabled = true
                heightUnitTwo.constant = 47
                viewUniteTwo.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.heightLocation.constant = self.heightLocation.constant + self.heightUnitTwo.constant
                    self.view.layoutIfNeeded()
                })
            } else {
                txtUniteTwo.text = ""
                txtUniteTwo.isEnabled = false
                heightUnitTwo.constant = 0
                viewUniteTwo.isHidden = true
            }
            
            if txtLocation3.text!.count > 0 {
                txtUniteThree.text = strUnitThree
                txtUniteThree.isEnabled = true
                heightUnitThree.constant = 47
                viewUniteThree.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.heightLocation.constant = self.heightLocation.constant + self.heightUnitThree.constant
                    self.view.layoutIfNeeded()
                })
            } else {
                txtUniteThree.text = ""
                txtUniteThree.isEnabled = false
                heightUnitThree.constant = 0
                viewUniteThree.isHidden = true
            }
            
            lattitude1 = Double(dictReport.object(forKey: "latitude_1") as! String) ?? 0.0
            longitude1 = Double(dictReport.object(forKey: "longitude_1") as! String) ?? 0.0
            lattitude2 = Double(dictReport.object(forKey: "latitude_2") as! String) ?? 0.0
            longitude2 = Double(dictReport.object(forKey: "longitude_2") as! String) ?? 0.0
            lattitude3 = Double(dictReport.object(forKey: "latitude_3") as! String) ?? 0.0
            longitude3 = Double(dictReport.object(forKey: "longitude_3") as! String) ?? 0.0
            
            txtAmount.text = dictReport.object(forKey: "amount") as? String
            
            txtQuestion1.text = dictReport.object(forKey: "hq_1") as? String
            txtQuestion2.text = dictReport.object(forKey: "hq_2") as? String
            txtQuestion3.text = dictReport.object(forKey: "hq_3") as? String
            txtQuestion4.text = dictReport.object(forKey: "hq_4") as? String
            txtQuestion5.text = dictReport.object(forKey: "hq_5") as? String
            
            txtAnswer1.text = dictReport.object(forKey: "hq_answer_1") as? String
            txtAnswer2.text = dictReport.object(forKey: "hq_answer_2") as? String
            txtAnswer3.text = dictReport.object(forKey: "hq_answer_3") as? String
            txtAnswer4.text = dictReport.object(forKey: "hq_answer_4") as? String
            txtAnswer5.text = dictReport.object(forKey: "hq_answer_5") as? String
            
            if dictReport.object(forKey: "is_reward") as! String == "1" {
                btnYesReward(btnYesReward)
            } else {
                btnNoReward(btnNoReward)
            }
            
            setEditQuesAns()
            
            if strReportUserType == FINDER {
                let strDelType = dictReport.object(forKey: "delivery_type") as! String
                if strDelType == DELIVERY_TYPE.CONTACTME.rawValue {
                    clearDelTypeButtonColors()
                } else if strDelType == DELIVERY_TYPE.POPSTATION.rawValue {
                    setUpPopStationData()
                    dictPOPStation = dictReport.object(forKey: "popstation_data") as! NSDictionary
                    let strName = dictPOPStation.object(forKey: "popstation_name") as? String
                    let strAddress = dictPOPStation.object(forKey: "popstation_address") as? String
                    txtPOPStation.text = String(format: "%@\n%@",strName!,strAddress!)
                } else if strDelType == DELIVERY_TYPE.COURIER.rawValue {
                    setUpCourierData()
                    let dictCourier = dictReport.object(forKey: "courier_data") as! NSDictionary
                    txtCourierAddress.text = dictCourier.object(forKey: "courier_address") as? String
                    txtCourierUnitNo.text = dictCourier.object(forKey: "courier_unitno") as? String
                    viewCourierUnitNo.isHidden = true
                    if txtCourierAddress.text!.count > 0 {
                        heightCourierUnitNo.constant = 47
                        viewCourierUnitNo.isHidden = false
                        UIView.animate(withDuration: 0.3, animations: {
                            self.heightCourierMainView.constant = self.heightCourierMainView.constant + self.heightCourierUnitNo.constant
                            self.heightDelType.constant = self.heightDelType.constant + self.heightCourierUnitNo.constant
                            self.view.layoutIfNeeded()
                        })
                    } else {
                        txtCourierUnitNo.text = ""
                        txtCourierUnitNo.isEnabled = false
                        heightCourierUnitNo.constant = 0
                    }
                    
                    lat_courier = Double(dictCourier.object(forKey: "courier_lat") as! String)!
                    long_courier = Double(dictCourier.object(forKey: "courier_long") as! String)!
                    let strPickUpTime = dictCourier.object(forKey: "courier_pickup_time") as! String
                    let strTimes = strPickUpTime.replacingOccurrences(of: " ", with: "")
                    let arrTime = strTimes.components(separatedBy: "to") as NSArray
                    if arrTime.count == 2 {
                        txtCourierFromTime.text = arrTime.object(at: 0) as? String
                        txtCourierToTime.text = arrTime.object(at: 1) as? String
                    }
                    txtCourierDate.text = dictCourier.object(forKey: "courier_date") as? String
                    let strFromDate = String(format: "%@ %@",txtCourierDate.text!,txtCourierFromTime.text!)
                    let strToDate = String(format: "%@ %@",txtCourierDate.text!,txtCourierToTime.text!)
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy hh:mma"
                    dateFormatter.timeZone = TimeZone.current
                    FromTimeDate = dateFormatter.date(from: strFromDate)!
                    ToTimeDate = dateFormatter.date(from: strToDate)!
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        imgBack.image = strReportUserType == SEEKER ? UIImage(named:"img_bg") : UIImage(named:"ic_background")
    }


    //MARK:- UIBUTTON ACTION
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFrom(_ sender: Any) {
        self.view.endEditing(true)
        
        var currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: "UTC")!
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.year = -150
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        
        if txtTo.text!.count > 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateFormatter.timeZone = TimeZone.current
            currentDate = dateFormatter.date(from: txtTo.text!)!
        }

        DatePicker.sharedInstance.showDateTimePicker(title: "Select Time", pickerMode: .date, maxDate: currentDate, minDate: minDate, style: .actionSheet) { (date) in
            print(date)
            self.toMinDate = date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateFormatter.timeZone = TimeZone.current
            self.txtFrom.text = dateFormatter.string(from: date)
        }
    }
    
    @IBAction func btnTo(_ sender: Any) {
        self.view.endEditing(true)
        if TRIM(string: txtFrom.text ?? "").count == 0 { return }
        DatePicker.sharedInstance.showDateTimePicker(title: "Select Time", pickerMode: .date, maxDate: Date(), minDate: toMinDate, style: .actionSheet) { (date) in
            print(date)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateFormatter.timeZone = TimeZone.current
            self.txtTo.text = dateFormatter.string(from: date)
        }
    }
    
    @IBAction func btnLoc1(_ sender: Any) {
        textFieldLoc = txtLocation1
        openGoogleAutoComplete()
    }
    @IBAction func btnLoc2(_ sender: Any) {
        textFieldLoc = txtLocation2
        openGoogleAutoComplete()
    }
    @IBAction func btnLoc3(_ sender: Any) {
        textFieldLoc = txtLocation3
        openGoogleAutoComplete()
    }
    
    @IBAction func btnAddLocation(_ sender: Any) {
        self.view.endEditing(true)
        if heightLocation.constant < 209 {
            if viewLoccation1.isHidden {
                viewLoccation1.isHidden = false
            } else {
                viewLoccation2.isHidden = false
            }
            UIView.animate(withDuration: 0.3, animations: {
                self.heightLocation.constant = self.heightLocation.constant + 8 + 39
                self.view.layoutIfNeeded()
            }) { (finished) in
            }
        }
    }
    
    @IBAction func btnRegion(_ sender: Any) {
        let arrSelected: NSMutableArray = NSMutableArray()
        if txtRegion.text?.count > 0 {
            arrSelected.add(txtRegion.text!)
        }
        
        let arrAddress: NSMutableArray = ["North", "South", "East", "West", "Central", "Northeast", "Northwest", "Southeast", "Southwest"]
        openList(array: arrAddress, arrSelected: arrSelected, type: "regionlist")
    }
    
    @IBAction func btnCategory(_ sender: UIButton) {
        if sender.tag == 99 {
            let arrSelected: NSMutableArray = NSMutableArray()
            let predict = NSPredicate(format: "category_id = %@", catId_1)
            let arrTmp = AppUtilities.sharedInstance.arrCatHint.filtered(using: predict) as NSArray
            if arrTmp.count > 0 {
                arrSelected.add(arrTmp.object(at: 0) as! NSDictionary)
            }
            openList(array: AppUtilities.sharedInstance.arrCatHint, arrSelected: arrSelected, type: "categorymainlist")
        } else if sender.tag == 100 {
            if txtCategory1.text!.count <= 0 {
                return
            }
            let arrSelected: NSMutableArray = NSMutableArray()
            let predict = NSPredicate(format: "category_id = %@", catId_2)
            let arrTmp = AppUtilities.sharedInstance.arrCatHint.filtered(using: predict) as NSArray
            if arrTmp.count > 0 {
                arrSelected.add(arrTmp.object(at: 0) as! NSDictionary)
            }
            
            let predict1 = NSPredicate(format: "category_id = %@", catId_3)
            let arrTmp1 = AppUtilities.sharedInstance.arrCatHint.filtered(using: predict1) as NSArray
            if arrTmp1.count > 0 {
                arrSelected.add(arrTmp1.object(at: 0) as! NSDictionary)
            }
            
            let predictSelected = NSPredicate(format: "category_id = %@", catId_1)
            let arrTmpSelected = AppUtilities.sharedInstance.arrCatHint.filtered(using: predictSelected) as NSArray
            if arrTmpSelected.count > 0 {
                let dict: NSDictionary = arrTmpSelected.object(at: 0) as! NSDictionary
                let arrData: NSMutableArray = NSMutableArray.init(array: AppUtilities.sharedInstance.arrCatHint)
                arrData.remove(dict)
                openList(array: arrData, arrSelected: arrSelected, type: "categorysublist")
            } else {
                openList(array: AppUtilities.sharedInstance.arrCatHint, arrSelected: arrSelected, type: "categorysublist")
            }
            
            
        }
    }
    
    @IBAction func btnDone(_ sender: Any) {
        if validate() {
            sendAddEditReportReq()
        }
    }

    @IBAction func btnYesReward(_ sender: Any) {
        self.view.endEditing(true)
        btnYesReward.isSelected = true
        btnNoReward.isSelected = false
        viewAmount.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.heightReward.constant = 96
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.txtAmount.isEnabled = true
        }
    }
    
    @IBAction func btnNoReward(_ sender: Any) {
        self.view.endEditing(true)
        btnYesReward.isSelected = false
        btnNoReward.isSelected = true
        viewAmount.isHidden = true
        UIView.animate(withDuration: 0.3, animations: {
            self.heightReward.constant = 43
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.txtAmount.isEnabled = false
        }
    }
    
    @IBAction func btnAddQuestion(_ sender: Any) {
        self.view.endEditing(true)
    
        question = question + 1
        if question > 5 { return }
        totalHeightQA = totalHeightQA + 39 + 8
        if question == 2 {
            viewQus2.isHidden = false
            setQuestionHeight(constantTmp: heightView2, constantValTmp:39)
        } else if question == 3 {
            viewQus3.isHidden = false
            setQuestionHeight(constantTmp: heightView3, constantValTmp:39)
        } else if question == 4 {
            viewQus4.isHidden = false
            setQuestionHeight(constantTmp: heightView4, constantValTmp:39)
        } else if question == 5 {
            viewQus5.isHidden = false
            setQuestionHeight(constantTmp: heightView5, constantValTmp:39)
        }
        
        setQuestionHeight(constantTmp: heightQuesAns, constantValTmp:totalHeightQA)
    }
    
    @IBAction func btnAnswer(_ sender: UIButton) {
        //if isEdit { return }
        self.view.endEditing(true)
        sender.isEnabled = false
        if sender.tag == 1 {
            totalHeightQA = totalHeightQA + 65 + 8
            viewAns1.isHidden = false
            setQuestionHeight(constantTmp: heightView1, constantValTmp:112)
        } else if sender.tag == 2 {
            totalHeightQA = totalHeightQA + 73
            viewAns2.isHidden = false
            setQuestionHeight(constantTmp: heightView2, constantValTmp:112)
        } else if sender.tag == 3 {
            totalHeightQA = totalHeightQA + 73
            viewAns3.isHidden = false
            setQuestionHeight(constantTmp: heightView3, constantValTmp:112)
        } else if sender.tag == 4 {
            totalHeightQA = totalHeightQA + 73
            viewAns4.isHidden = false
            setQuestionHeight(constantTmp: heightView4, constantValTmp:112)
        } else if sender.tag == 5 {
            totalHeightQA = totalHeightQA + 73
            viewAns5.isHidden = false
            setQuestionHeight(constantTmp: heightView5, constantValTmp:112)
        }
        
        setQuestionHeight(constantTmp: heightQuesAns, constantValTmp:totalHeightQA)
    }
    
    func setQuestionHeight(constantTmp: NSLayoutConstraint, constantValTmp: CGFloat) {
        UIView.animate(withDuration: 0.3) {
            constantTmp.constant = constantValTmp
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnContactMe(_ sender: UIButton) {
        if isEdit { return }
        clearDelTypeButtonColors()
    }
    
    @IBAction func btnPopStation(_ sender: UIButton) {
        if isEdit { return }
        setUpPopStationData()
    }
    
    func setUpPopStationData() {
        UIView.animate(withDuration: 0.3) {
            self.heightDelType.constant = 149
            self.view.layoutIfNeeded()
        }
        lblDelTypDesc.text = "*Select one of the 140 drop boxes islandwide. MUST Drop-off within 24-hours after details are sent"
        strDelType = DELIVERY_TYPE.POPSTATION.rawValue
        viewPopStation.isHidden = false
        viewCourier.isHidden = true
        btnContactMe.backgroundColor = UIColor.clear
        btnPopStation.backgroundColor = BLUECOLOR
        btnCourier.backgroundColor = UIColor.clear
        btnContactMe.setTitleColor(BLUECOLOR, for: .normal)
        btnPopStation.setTitleColor(UIColor.white, for: .normal)
        btnCourier.setTitleColor(BLUECOLOR, for: .normal)
    }
    
    @IBAction func btnCourier(_ sender: UIButton) {
        if isEdit { return }
        setUpCourierData()
    }
    
    func setUpCourierData() {
        lblDelTypDesc.text = "*Arrange for courier pick-up at your convenience. Selected items only"
        UIView.animate(withDuration: 0.3) {
            self.heightDelType.constant = 262
            self.view.layoutIfNeeded()
        }
        strDelType = DELIVERY_TYPE.COURIER.rawValue
        viewPopStation.isHidden = true
        viewCourier.isHidden = false
        btnContactMe.backgroundColor = UIColor.clear
        btnPopStation.backgroundColor = UIColor.clear
        btnCourier.backgroundColor = BLUECOLOR
        btnContactMe.setTitleColor(BLUECOLOR, for: .normal)
        btnPopStation.setTitleColor(BLUECOLOR, for: .normal)
        btnCourier.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func btnSelectPOPStation(_ sender: UIButton) {
        if isEdit { return }
        let arrSelected: NSMutableArray = NSMutableArray()
        if txtPOPStation.text?.count > 0 {
            arrSelected.add(dictPOPStation)
        }
        openList(array: arrPopStation, arrSelected: arrSelected, type: "popstation")
    }
    
    @IBAction func btnCourierAddress(_ sender: Any) {
        if isEdit { return }
        textFieldLoc = txtCourierAddress
        openGoogleAutoComplete()
    }
    
    @IBAction func btnCourierDate(_ sender: Any) {
        if isEdit { return }
        
        let ThreeDaysLater = Calendar.current.date(byAdding: .day, value: 3, to: Date())
        
        let alert = UIAlertController(style: .actionSheet, title: "Select date")
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: ThreeDaysLater, maximumDate: nil) { date in
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "dd/MM/yyyy"
            self.txtCourierDate.text = dateFormat.string(from: date)
            print(self.txtCourierDate.text!)
        }
        alert.addAction(title: "OK", style: .cancel)
        alert.show()
    }
    
    @IBAction func btnCourierFromTime(_ sender: Any) {
        if isEdit { return }
        var maxDate: Date?
        if txtCourierToTime.text?.count > 0 {
            maxDate = ToTimeDate
            maxDate = GetSpecificTimeDate(time: "09:00pm", date: ToTimeDate)
        } else {
            maxDate = GetSpecificTimeDate(time: "09:00pm", date: Date())
        }
        
        let date = GetSpecificTimeDate(time: "10:00am", date: Date())

        let alert = UIAlertController(style: .actionSheet, title: "Select Time")
        alert.addDatePicker(mode: .time, date: Date(), minimumDate: date, maximumDate: maxDate) { date in
            self.FromTimeDate = date
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "hh:mma"
            self.txtCourierFromTime.text = dateFormat.string(from: date)
        }
        alert.addAction(title: "OK", style: .cancel)
        alert.show()
    }
    
    @IBAction func btnCourierToTime(_ sender: Any) {
        if isEdit { return }
    
        let minDate = GetSpecificTimeDate(time: "09:00pm", date: Date())
        
        let alert = UIAlertController(style: .actionSheet, title: "Select Time")
        alert.addDatePicker(mode: .time, date: Date(), minimumDate: self.FromTimeDate, maximumDate: minDate) { date in
            self.ToTimeDate = date
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "hh:mma"
            self.txtCourierToTime.text = dateFormat.string(from: date)
        }
        alert.addAction(title: "OK", style: .cancel)
        alert.show()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension AddFoundItemVC {
    func getMinMaxValue(needMin:Bool) -> Double {
        
        var min1: Double = 0
        var min2: Double = 0
        var min3: Double = 0
        
        var max1: Double = 0
        var max2: Double = 0
        var max3: Double = 0
        
        if catId_1.count > 0 {
            let pred : NSPredicate = NSPredicate(format: "category_id = %@", catId_1)
            let arrTemp = AppUtilities.sharedInstance.arrCatHint.filtered(using: pred) as NSArray
            if arrTemp.count > 0 {
                let dictData = arrTemp.object(at: 0) as! NSDictionary
                min1 = Double(dictData.object(forKey: "min_reward_amount") as! String)!
                max1 = Double(dictData.object(forKey: "max_reward_amount") as! String)!
            }
        }
        
        if catId_2.count > 0 {
            let pred : NSPredicate = NSPredicate(format: "category_id = %@", catId_2)
            let arrTemp = AppUtilities.sharedInstance.arrCatHint.filtered(using: pred) as NSArray
            if arrTemp.count > 0 {
                let dictData = arrTemp.object(at: 0) as! NSDictionary
                min2 = Double(dictData.object(forKey: "min_reward_amount") as! String)!
                max2 = Double(dictData.object(forKey: "max_reward_amount") as! String)!
            }
        }
        
        if catId_3.count > 0 {
            let pred : NSPredicate = NSPredicate(format: "category_id = %@", catId_3)
            let arrTemp = AppUtilities.sharedInstance.arrCatHint.filtered(using: pred) as NSArray
            if arrTemp.count > 0 {
                let dictData = arrTemp.object(at: 0) as! NSDictionary
                min3 = Double(dictData.object(forKey: "min_reward_amount") as! String)!
                max3 = Double(dictData.object(forKey: "max_reward_amount") as! String)!
            }
        }
        if needMin {
            return min1 + min2 + min3
        } else {
            return max1 + max2 + max3
        }
    }
}

extension AddFoundItemVC: pickerVCDelegate {

    func openList(array: NSMutableArray, arrSelected: NSMutableArray, type: String) {
        self.view.endEditing(true)
        let vc : PickerVC = PickerVC(nibName: "PickerVC", bundle: nil)
        vc.pickerVCDelegate = self
        vc.arrData = array
        vc.arrSelectedData = arrSelected
        vc.listType = type
        vc.strUserType = strReportUserType
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnSave(arrDetail: NSMutableArray, type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        
        print(arrDetail)
        
        if arrDetail.count <= 0 {
            if type == "categorymainlist" {
                self.catId_1 = ""
                self.txtCategory1.text = ""
            } else if type == "categorysublist" {
                self.catId_2 = ""
                self.catId_3 = ""
                self.txtCategory2.text = ""
                self.txtAmount.text = String(format:"%.2f",getMinMaxValue(needMin: true))
            } else if type == "popstation" {
                dictPOPStation = NSDictionary.init()
                txtPOPStation.text = ""
            }
            return
        }
        
        if type == "categorymainlist" {
            let dictData = arrDetail.object(at: 0) as! NSDictionary
            txtCategory1.text = dictData.object(forKey: "category_name") as? String
            self.setCatHintQus(dictData: dictData)
            self.txtAmount.text = String(format:"%.2f",getMinMaxValue(needMin: true))
        } else if type == "categorysublist" {
            let arrCats = arrDetail.value(forKeyPath: "category_name") as! NSArray
            txtCategory2.text = arrCats.componentsJoined(by: ", ")
            let dictData = arrDetail.object(at: 0) as! NSDictionary
            self.catId_2 = dictData.object(forKey: "category_id") as! String
            self.catId_3 = ""
            if arrDetail.count == 2 {
                let dictCat = arrDetail.object(at: 1) as! NSDictionary
                self.catId_3 = dictCat.object(forKey: "category_id") as! String
            }
            self.txtAmount.text = String(format:"%.2f",getMinMaxValue(needMin: true))
        } else if type == "regionlist" {
            txtRegion.text = arrDetail.object(at: 0) as? String
        } else if type == "popstation" {
            dictPOPStation = arrDetail.object(at: 0) as! NSDictionary
            let strName = dictPOPStation.object(forKey: "popstation_name") as? String
            let strAddress = dictPOPStation.object(forKey: "popstation_address") as? String
            txtPOPStation.text = String(format: "%@\n%@",strName!,strAddress!)
        }
    }
    
    func btnCancel(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }

}

extension AddFoundItemVC: ImagePickerDelegate{
    
    func openImagePicker(type: String) {
        self.view.endEditing(true)
        ImagePicker.sharedInstance.delegate = self
        ImagePicker.sharedInstance.strUserType = strReportUserType
        ImagePicker.sharedInstance.selectImage(sender: type)
    }
    
    func pickImageComplete(_ imageData: UIImage, sender: String)
    {
        let dict: NSMutableDictionary = NSMutableDictionary()
        dict.setObject("0", forKey: "id" as NSCopying)
        dict.setObject(imageData, forKey: "image" as NSCopying)
        dict.setObject("", forKey: "image_link" as NSCopying)
        if sender == "actualimage" {
            dict.setObject("Actual", forKey: "type" as NSCopying)
        } else {
            dict.setObject("Reference", forKey: "type" as NSCopying)
        }
        arrImages.insert(dict, at: 0)
        arrImages.removeLastObject()
        //if arrImages.count == 4 { arrImages.removeLastObject() }
        collImages.reloadData()
    }
}

extension AddFoundItemVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageSlideCell", for: indexPath) as! ImageSlideCell
        let dictData = arrImages.object(at: indexPath.row) as! NSDictionary
        let strType = dictData.object(forKey: "type") as! String
        if strType == "uploadShowing" {
            cell.imgBackCell.image = strReportUserType == SEEKER ? UIImage(named:"img_bg") : UIImage(named:"ic_background")
            cell.viewUpload.isHidden = false
            cell.viewImage.isHidden = true
            cell.btnAddImage.addTarget(self, action: #selector(btnAddImage(_:)), for:.touchUpInside)
            if strReportUserType == SEEKER {
                cell.lblUploadDesc.text = "PLEASE UPLOAD A REFERENCE OR ACTUAL PHOTO OF THE LOST ITEM"
            } else {
                cell.lblUploadDesc.text = "PLEASE UPLOAD A REFERENCE OR ACTUAL PHOTO OF THE FOUND ITEM"
            }
        }
        else {
            cell.viewUpload.isHidden = true
            cell.viewImage.isHidden = false
            cell.lblImageType.text = dictData.object(forKey: "type") as? String
            if (dictData.object(forKey: "image_link") as! String).count > 0 {
//                cell.imgItem.sd_setImage(with:URL(string:dictData.object(forKey: "image_link") as! String), placeholderImage:nil, options:.continueInBackground)
                cell.imgItem.setImageWith(URL.init(string:dictData.object(forKey: "image_link") as! String), placeholderImage: nil, options: .continueInBackground, completed: { (img, error, cachType, url) in
                    if(img == nil)
                    {
                        cell.imgItem.image = nil
                    }
                    else
                    {
                        cell.imgItem.image = img
                    }
                }, usingActivityIndicatorStyle: .gray)
            } else {
                cell.imgItem.image = dictData.object(forKey: "image") as? UIImage
            }

            cell.btnActual.addTarget(self, action: #selector(btnActual(sender:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.view.size.width, height: collImages.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        setItemImage()
    }
    
    @objc func btnActual(sender:UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: collImages)
        let indexPath = collImages.indexPathForItem(at: buttonPosition)
        print(indexPath?.row ?? "Not Found")
        
        let dict = (arrImages.object(at: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Actual Image", style: .default)
        { _ in
            dict.setObject("Actual", forKey: "type" as NSCopying)
            self.arrImages.replaceObject(at: indexPath!.row, with: dict.copy() as! NSDictionary)
            self.collImages.reloadData()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Reference Image", style: .default)
        { _ in
            dict.setObject("Reference", forKey: "type" as NSCopying)
            self.arrImages.replaceObject(at: indexPath!.row, with: dict.copy() as! NSDictionary)
            self.collImages.reloadData()
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @objc func btnAddImage(_ sender:UIButton)
    {
        setItemImage()
    }
    
    func setItemImage() {
        self.view.endEditing(true)
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Actual Image", style: .default)
        { _ in
            self.openImagePicker(type: "actualimage")
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Reference Image", style: .default)
        { _ in
            self.openImagePicker(type: "referenceimage")
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
}

extension AddFoundItemVC: GMSAutocompleteViewControllerDelegate {
    
    func openGoogleAutoComplete() {
        self.view.endEditing(true)
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place address: \(place.coordinate.latitude)")
        print("Place attributions: \(place.coordinate.longitude)")
        
        if textFieldLoc == txtLocation1 {
            lattitude1 = place.coordinate.latitude
            longitude1 = place.coordinate.longitude
            
            if heightUnitOne.constant <= 0 {
                txtUniteOne.isEnabled = true
                heightUnitOne.constant = 47
                viewUniteOne.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.heightLocation.constant = self.heightLocation.constant + self.heightUnitOne.constant
                    self.view.layoutIfNeeded()
                })
            }
            
        } else if textFieldLoc == txtLocation2 {
            lattitude2 = place.coordinate.latitude
            longitude2 = place.coordinate.longitude
            
            if heightUnitTwo.constant <= 0 {
                txtUniteTwo.isEnabled = true
                heightUnitTwo.constant = 47
                viewUniteTwo.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.heightLocation.constant = self.heightLocation.constant + self.heightUnitTwo.constant
                    self.view.layoutIfNeeded()
                })
            }
            
        } else if textFieldLoc == txtLocation3 {
            lattitude3 = place.coordinate.latitude
            longitude3 = place.coordinate.longitude
            
            if heightUnitThree.constant <= 0 {
                txtUniteThree.isEnabled = true
                heightUnitThree.constant = 47
                viewUniteThree.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.heightLocation.constant = self.heightLocation.constant + self.heightUnitThree.constant
                    self.view.layoutIfNeeded()
                })
            }
            
        } else if textFieldLoc == txtCourierAddress {
            lat_courier = place.coordinate.latitude
            long_courier = place.coordinate.longitude
            
            if heightCourierUnitNo.constant <= 0 {
                txtCourierUnitNo.isEnabled = true
                heightCourierUnitNo.constant = 47
                viewCourierUnitNo.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.heightCourierMainView.constant = self.heightCourierMainView.constant + self.heightCourierUnitNo.constant
                    self.heightDelType.constant = self.heightDelType.constant + self.heightCourierUnitNo.constant
                    self.view.layoutIfNeeded()
                })
            }
        }
        textFieldLoc.text = place.formattedAddress ?? ""
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func setCatHintQus(dictData: NSDictionary) {
        self.catId_1 = dictData.object(forKey: "category_id") as! String
        let arrHint = dictData.object(forKey: "hintQuestion_data") as! NSArray
        for i in 0..<arrHint.count {
            let dict = arrHint.object(at: i) as! NSDictionary
            let txtFeild = self.view.viewWithTag(55+i) as! UITextField
            txtFeild.placeholder = dict.object(forKey: "hint_question_name") as? String
        }
    }
}

extension AddFoundItemVC {
    
    func sendAddEditReportReq()
    {
        self.view.endEditing(true)
        
        var strIsActual1 = "0"
        let dict = arrImages.object(at: 0) as! NSDictionary
        let strType = dict.object(forKey: "type") as! String
        if strType == "Actual" {
            strIsActual1 = "1"
        }
        var imageParam = ""
        var imageItem = UIImage()
        if (dict.object(forKey: "image_link") as! String).count <= 0 {
            imageParam = "report_image_1"
            imageItem = dict.object(forKey: "image") as! UIImage
        }
        
        var strAmount = ""
        var strIsReward = "0"
        if btnYesReward.isSelected {
            strIsReward = "1"
            strAmount = txtAmount.text!
        }
        
        let title = strReportUserType == SEEKER ? "Lost \(txtCategory1.text!)" : "Found \(txtCategory1.text!)"
        
        
        var strPopStationID = ""
        var strCourierAdd = ""
        var strCourierDate = ""
        var strCourierPickupTime = ""
        var strCourierStatus = ""
        var strCourierLat = ""
        var strCourierLong = ""
        
        if strDelType == DELIVERY_TYPE.POPSTATION.rawValue {
            strPopStationID = dictPOPStation.object(forKey: "popstation_id") as! String
        }
        if strDelType == DELIVERY_TYPE.COURIER.rawValue {
            strCourierAdd = txtCourierAddress.text!
            strCourierLat = "\(lat_courier)"
            strCourierLong = "\(long_courier)"
            strCourierDate = txtCourierDate.text!
            strCourierStatus = "0"
            strCourierPickupTime = String(format:"%@ to %@",txtCourierFromTime.text!, txtCourierToTime.text!)
        }
        var strReportId = ""
        if isEdit {
            strReportId = dictReport.object(forKey: "report_id") as! String
        }
        
        let dictParam : NSDictionary = ["report_id":strReportId,
                                        "user_id":AppUtilities.sharedInstance.getUserID(),
                                        "report_type":strReportUserType,
                                        "title":title,
                                        "description":txtDescription.text!,
                                        "from_date":txtFrom.text!,
                                        "to_date":txtTo.text!,
                                        "category_id":catId_1,
                                        "category_id_1":catId_2,
                                        "category_id_2":catId_3,
                                        "is_reward":strIsReward,
                                        "amount":strAmount,
                                        "address_1":txtLocation1.text!,
                                        "address_2":txtLocation2.text!,
                                        "address_3":txtLocation3.text!,
                                        "region":txtRegion.text!,
                                        "latitude_1":"\(lattitude1)",
                                        "longitude_1":"\(longitude1)",
                                        "latitude_2":"\(lattitude2)",
                                        "longitude_2":"\(longitude2)",
                                        "latitude_3":"\(lattitude3)",
                                        "longitude_3":"\(longitude3)",
                                        "hq_1":txtQuestion1.text!,
                                        "hq_answer_1":txtAnswer1.text!,
                                        "hq_2":txtQuestion2.text!,
                                        "hq_answer_2":txtAnswer2.text!,
                                        "hq_3":txtQuestion3.text!,
                                        "hq_answer_3":txtAnswer3.text!,
                                        "hq_4":txtQuestion4.text!,
                                        "hq_answer_4":txtAnswer4.text!,
                                        "hq_5":txtQuestion5.text!,
                                        "hq_answer_5":txtAnswer5.text!,
                                        "ri1_is_actual":strIsActual1,
                                        "delivery_type":strDelType,
                                        "popstation_id":strPopStationID,
                                        "courier_address":strCourierAdd,
                                        "courier_date":strCourierDate,
                                        "courier_status":strCourierStatus,
                                        "courier_pickup_time":strCourierPickupTime,
                                        "courier_lat":strCourierLat,
                                        "courier_long":strCourierLong,
                                        "courier_unitno":txtCourierUnitNo.text!,
                                        "unitno_1":txtUniteOne.text!,
                                        "unitno_2":txtUniteTwo.text!,
                                        "unitno_3":txtUniteThree.text!]
        
        print("My Param", dictParam)

        HttpRequestManager.sharedInstance.requestWithPostMultipartParam(
            endpointurl: Server_URL,
            service:APIAddEditReport,
            parameters: dictParam,
            image: imageItem,
            imageParam:imageParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    if responseDict?.object(forKey: kSuccess) as! String == "1" {
                        self.dictResponseData = responseDict?.object(forKey: "user_info") as! NSDictionary
                        self.updateData()
                        self.openSuccessAlert()
                    } else {
                        showMessage(responseDict?.object(forKey: kMessage) as! String)
                    }
                }
                else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
    
    func sendGetPopStationReq() {
        let dictParam: NSDictionary = ["latitude":lat_currnt,
                                       "longitude":long_currnt]
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIGetPOPStation,
            parameters: dictParam,
            showLoader:false)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                print(error?.localizedDescription ?? "error is nil")
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    self.arrPopStation = arrayOfFilteredBy(arr: responseDict?.object(forKey: kData) as! NSArray).mutableCopy() as! NSMutableArray
                }
            }
        }
    }
    
    
}

extension AddFoundItemVC: displayAlertDelegate, AlertDelegate {
    
    func openSuccessAlert() {
        var desc = strReportUserType == SEEKER ? "Your Lost Report has been successfully created. We will notify you once admin verifies it." : "Your Found Report has been successfully created. We will notify you once admin verifies it."
        if isEdit {
            desc = strReportUserType == SEEKER ? "Your lost item report edited successfully!" : "Your found item report edited successfully!"
        }
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = "Successful"
        vc.strDescription = desc
        vc.type = "simplealert"
        vc.strUserType = strReportUserType
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }

    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "simplealert" {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func updateData() {
        if self.isEdit {
            self.delegateAddEditReport?.updateRecord(dictData: self.dictResponseData, index: self.index, isEdit: true)
        } else {

            /*
            if self.strReportUserType == FINDER {
                AppUtilities.sharedInstance.isGetFinderList = true
                NotificationCenter.default.post(name: Notification.Name("getFinderList"), object: nil)
            } else {
                AppUtilities.sharedInstance.isGetLosterList = true
                NotificationCenter.default.post(name: Notification.Name("getLosterList"), object: nil)
            }*/
        }
    }
}

extension AddFoundItemVC {
    func validate() -> Bool {
        let dict = arrImages.object(at: 0) as! NSDictionary
        let strType = dict.object(forKey: "type") as! String
        if strType == "uploadShowing" {
            if strReportUserType == SEEKER {
                showMessage("Please upload reference or actual image of item which you lost.")
            } else {
                showMessage("Please upload a photo of the item you found.")
            }
            return false
        }
        if !validateTxtViewLength(txtDescription, withMessage: EnterDescription) {
            return false
        }
        if !validateTxtFieldLength(txtFrom, withMessage: EnterFrom) {
            return false
        }
        if !validateTxtFieldLength(txtTo, withMessage: EnterTo) {
            return false
        }
        if !validateTxtFieldLength(txtCategory1, withMessage: EnterCategory) {
            return false
        }
        
        if !validateTxtFieldLength(txtLocation1, withMessage: EnterAddress) {
            return false
        }
        
        if txtLocation3.text?.count > 0 && txtLocation2.text!.count <= 0 {
            showMessage("Need to Complete first above location.")
            return false
        }
        
        if !validateTxtFieldLength(txtRegion, withMessage: EnterRegion) {
            return false
        }
        
        if btnYesReward.isSelected {
            if TRIM(string: txtAmount.text ?? "").count == 0 {
                if !validateTxtFieldLength(txtAmount, withMessage: EnterReward) {
                    return false
                }
            }
            if TRIM(string: txtAmount.text ?? "") == "0" {
                showMessage(EnterMinReward)
                return false
            }
            
            /*
            if self.MinCat1Amnt != 0 && self.MinCat1Amnt > Double(TRIM(string: txtAmount.text ?? "0")) {
                showMessage("Minimum \(self.MinCat1Amnt) reward is required.")
                return false
            }*/
            
            let amountText: Double = Double(TRIM(string: txtAmount.text ?? "0"))!
            let minVal: Double = getMinMaxValue(needMin: true)
            
            if amountText < minVal {
                showMessage("Minimum \(minVal) reward is required.")
                return false
            }
            
            let maxVal: Double = getMinMaxValue(needMin: false)
            if amountText > maxVal {
                showMessage("Maximum \(maxVal) reward is required.")
                return false
            }
        }
        
        if TRIM(string: txtQuestion1.text ?? "").count == 0 && TRIM(string: txtQuestion2.text ?? "").count == 0 && TRIM(string: txtQuestion3.text ?? "").count == 0 && TRIM(string: txtQuestion4.text ?? "").count == 0 && TRIM(string: txtQuestion5.text ?? "").count == 0 {
            if !validateTxtFieldLength(txtQuestion1, withMessage: EnterQuestion){
                return false
            }
        }
        
        if TRIM(string: txtQuestion1.text ?? "").count > 0 && TRIM(string: txtAnswer1.text ?? "").count == 0 {
            if !validateTxtViewLength(txtAnswer1, withMessage: EnterAnswer){
                return false
            }
        }
        if TRIM(string: txtQuestion2.text ?? "").count > 0 && TRIM(string: txtAnswer2.text ?? "").count == 0 {
            if !validateTxtViewLength(txtAnswer2, withMessage: EnterAnswer){
                return false
            }
        }
        if TRIM(string: txtQuestion3.text ?? "").count > 0 && TRIM(string: txtAnswer3.text ?? "").count == 0 {
            if !validateTxtViewLength(txtAnswer3, withMessage: EnterAnswer){
                return false
            }
        }
        if TRIM(string: txtQuestion4.text ?? "").count > 0 && TRIM(string: txtAnswer4.text ?? "").count == 0 {
            if !validateTxtViewLength(txtAnswer4, withMessage: EnterAnswer){
                return false
            }
        }
        if TRIM(string: txtQuestion5.text ?? "").count > 0 && TRIM(string: txtAnswer5.text ?? "").count == 0 {
            if !validateTxtViewLength(txtAnswer5, withMessage: EnterAnswer) {
                return false
            }
        }
        
        txtDescription.text = TRIM(string: txtDescription.text ?? "")
        txtAmount.text = TRIM(string: txtAmount.text ?? "")
        txtQuestion1.text = TRIM(string: txtQuestion1.text ?? "")
        txtQuestion2.text = TRIM(string: txtQuestion2.text ?? "")
        txtQuestion3.text = TRIM(string: txtQuestion3.text ?? "")
        txtQuestion4.text = TRIM(string: txtQuestion4.text ?? "")
        txtQuestion5.text = TRIM(string: txtQuestion5.text ?? "")
        txtAnswer1.text = TRIM(string: txtAnswer1.text ?? "")
        txtAnswer2.text = TRIM(string: txtAnswer2.text ?? "")
        txtAnswer3.text = TRIM(string: txtAnswer3.text ?? "")
        txtAnswer4.text = TRIM(string: txtAnswer4.text ?? "")
        txtAnswer5.text = TRIM(string: txtAnswer5.text ?? "")
        
        if txtQuestion2.text?.count > 0 {
            if txtQuestion1.text!.count <= 0 {
                showMessage("Need to Complete first question and answer.")
                return false
            }
        }
        
        if txtQuestion3.text?.count > 0 {
            if txtQuestion1.text!.count <= 0 {
                showMessage("Need to Complete first question and answer.")
                return false
            }
            if txtQuestion2.text!.count <= 0 {
                showMessage("Need to Complete second question and answer.")
                return false
            }
        }
        
        if txtQuestion4.text?.count > 0 {
            if txtQuestion1.text!.count <= 0 {
                showMessage("Need to Complete first question and answer.")
                return false
            }
            if txtQuestion2.text!.count <= 0 {
                showMessage("Need to Complete second question and answer.")
                return false
            }
            if txtQuestion3.text!.count <= 0 {
                showMessage("Need to Complete third question and answer.")
                return false
            }
        }
        
        if txtQuestion5.text?.count > 0 {
            if txtQuestion1.text!.count <= 0 {
                showMessage("Need to Complete first question and answer.")
                return false
            }
            if txtQuestion2.text!.count <= 0 {
                showMessage("Need to Complete second question and answer.")
                return false
            }
            if txtQuestion3.text!.count <= 0 {
                showMessage("Need to Complete third question and answer.")
                return false
            }
            if txtQuestion4.text!.count <= 0 {
                showMessage("Need to Complete fourth question and answer.")
                return false
            }
        }
        
        if strDelType == DELIVERY_TYPE.POPSTATION.rawValue {
            if !validateTxtFieldLength(txtPOPStation, withMessage: EnterPOPStation){
                return false
            }
        }
        
        if strDelType == DELIVERY_TYPE.COURIER.rawValue {
            if !validateTxtFieldLength(txtCourierAddress, withMessage: EnterCourierAdd){
                return false
            }
            if !validateTxtFieldLength(txtCourierDate, withMessage: EnterCourierDate){
                return false
            }
            if !validateTxtFieldLength(txtCourierFromTime, withMessage: EnterCourierFormTime){
                return false
            }
            if !validateTxtFieldLength(txtCourierToTime, withMessage: EnterCourierToTime){
                return false
            }
        }
        
        return true
    }
}

extension AddFoundItemVC {
    func setUpView() {
        let dict: NSMutableDictionary = ["type":"uploadShowing"]
        arrImages.add(dict)
        collImages.register(UINib.init(nibName: "ImageSlideCell", bundle: nil), forCellWithReuseIdentifier: "ImageSlideCell")
        
        //txtDescription.placeholder = "Only describe the item based on the image uploaded. Ensure both image and description do not give away too much info such as exact location, exact date, uniqueness of item, making it easy for anyone to make a false claim."
        if strReportUserType == SEEKER {
            lblTittle.text = isEdit ? "EDIT LOST ITEM REPORT" : "UPLOAD LOST ITEM REPORT"
            imgLoc1.image = UIImage(named:"ic_pinred")
            imgLoc2.image = UIImage(named:"ic_pinred")
            imgLoc3.image = UIImage(named:"ic_pinred")
            btnAddLocation.setImage(UIImage(named:"ic_plusred"), for: .normal)
            
            btnAddQuestion.setImage(UIImage(named:"ic_plusred"), for: .normal)
            btnYesReward.setImage(UIImage(named:"redcheck"), for: .selected)
            btnNoReward.setImage(UIImage(named:"redcheck"), for: .selected)
            btnDone.backgroundColor = REDCOLOR
            for i in 1..<6 {
                let btnAns: UIButton = self.view.viewWithTag(i) as! UIButton
                btnAns.backgroundColor = REDCOLOR
            }
            yDelType.constant = 0
            heightDelType.constant = 0
            viewDelType.isHidden = true
        } else {
            sendGetPopStationReq()
            lblTittle.text = isEdit ? "EDIT FOUND ITEM REPORT" : "UPLOAD FOUND ITEM REPORT"
            lblwhenlose.text = "When did you find the item?"
            
            setUpCourierData()
        }
        
        if !isEdit {
            
            viewLoccation1.isHidden = true
            viewLoccation2.isHidden = true
            viewUniteOne.isHidden = true
            viewUniteTwo.isHidden = true
            viewUniteThree.isHidden = true
            viewCourierUnitNo.isHidden = true
            
            txtLocation1.isEnabled = false
            txtLocation2.isEnabled = false
            txtUniteOne.isEnabled = false
            txtUniteTwo.isEnabled = false
            txtUniteThree.isEnabled = false
            
            heightUnitOne.constant = 0
            heightUnitTwo.constant = 0
            heightUnitThree.constant = 0
            heightCourierUnitNo.constant = 0
            heightCourierMainView.constant = 137
            
            heightLocation.constant = 67
            heightQuesAns.constant = 39
            
            heightView1.constant = 39
            heightView2.constant = 0
            heightView3.constant = 0
            heightView4.constant = 0
            heightView5.constant = 0
            
            viewQus2.isHidden = true
            viewQus3.isHidden = true
            viewQus4.isHidden = true
            viewQus5.isHidden = true
            viewAns1.isHidden = true
            viewAns2.isHidden = true
            viewAns3.isHidden = true
            viewAns4.isHidden = true
            viewAns5.isHidden = true
        }
    }
    
    func setEditQuesAns() {
        viewQus2.isHidden = true
        viewQus3.isHidden = true
        viewQus4.isHidden = true
        viewQus5.isHidden = true
        viewAns1.isHidden = true
        viewAns2.isHidden = true
        viewAns3.isHidden = true
        viewAns4.isHidden = true
        viewAns5.isHidden = true
        
        totalHeightQA = 0
        btnAddQuestion.isHidden = false
        let viewHeight: CGFloat = 112.0
        heightView1.constant = 0
        if txtQuestion1.text?.count > 0 {
            question = 1
            heightView1.constant = 112
            viewAns1.isHidden = false
            viewQus1.isHidden = false
            heightQuesAns.constant = viewHeight
            
            let btn = self.view.viewWithTag(1) as! UIButton
            btn.isEnabled = false
        }
        
        heightView2.constant = 0
        if txtQuestion2.text?.count > 0 {
            question = 2
            heightView2.constant = 112
            viewQus2.isHidden = false
            viewAns2.isHidden = false
            heightQuesAns.constant = (viewHeight * 2) + 8
            
            let btn = self.view.viewWithTag(2) as! UIButton
            btn.isEnabled = false
        }
        
        heightView3.constant = 0
        if txtQuestion3.text?.count > 0 {
            question = 3
            heightView3.constant = 112
            viewQus3.isHidden = false
            viewAns3.isHidden = false
            heightQuesAns.constant = (viewHeight * 3) + 16
            
            let btn = self.view.viewWithTag(3) as! UIButton
            btn.isEnabled = false
        }
        
        heightView4.constant = 0
        if txtQuestion4.text?.count > 0 {
            question = 4
            heightView4.constant = 112
            viewQus4.isHidden = false
            viewAns4.isHidden = false
            heightQuesAns.constant = (viewHeight * 4) + 24
            let btn = self.view.viewWithTag(4) as! UIButton
            btn.isEnabled = false
        }
        
        heightView5.constant = 0
        if txtQuestion5.text?.count > 0 {
            question = 5
            heightView5.constant = 112
            viewQus5.isHidden = false
            viewAns5.isHidden = false
            heightQuesAns.constant = (viewHeight * 5) + 32
            let btn = self.view.viewWithTag(5) as! UIButton
            btn.isEnabled = false
        }
        
        totalHeightQA = heightQuesAns.constant
    }
    
    func clearDelTypeButtonColors() {
        UIView.animate(withDuration: 0.3) {
            self.heightDelType.constant = 86
            self.view.layoutIfNeeded()
        }
        lblDelTypDesc.text = "*Contact me directly"
        strDelType = DELIVERY_TYPE.CONTACTME.rawValue
        viewPopStation.isHidden = true
        viewCourier.isHidden = true
        btnContactMe.backgroundColor = BLUECOLOR
        btnPopStation.backgroundColor = UIColor.clear
        btnCourier.backgroundColor = UIColor.clear
        btnContactMe.setTitleColor(UIColor.white, for: .normal)
        btnPopStation.setTitleColor(BLUECOLOR, for: .normal)
        btnCourier.setTitleColor(BLUECOLOR, for: .normal)
    }
}

extension AddFoundItemVC {
    func GetSpecificTimeDate(time: String, date: Date) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = String(format: "%@ %@",dateFormatter.string(from: date),time)
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mma"
        return dateFormatter.date(from: strDate)!
    }
}
