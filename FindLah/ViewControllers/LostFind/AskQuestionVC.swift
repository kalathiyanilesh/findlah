//
//  AskQuestionVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 19/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class AskQuestionVC: UIViewController {

    @IBOutlet var heightView: NSLayoutConstraint!
    @IBOutlet var viewQuestion: UIView!
    @IBOutlet var lblQuestion: UILabel!
    @IBOutlet var txtAnswer: IQTextView!
    
    var Height:CGFloat = 150
    var strQuestion: String = ""
    var strAnswer: String = ""
    var page: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if page == 0 {
            AppUtilities.sharedInstance.txtAns1 = txtAnswer
        } else if page == 1 {
            AppUtilities.sharedInstance.txtAns2 = txtAnswer
        } else if page == 2 {
            AppUtilities.sharedInstance.txtAns3 = txtAnswer
        } else if page == 3 {
            AppUtilities.sharedInstance.txtAns4 = txtAnswer
        } else if page == 4 {
            AppUtilities.sharedInstance.txtAns5 = txtAnswer
        }
        txtAnswer.placeholder = "Ans"
        heightView.constant = Height
        
        lblQuestion.text = strQuestion
        txtAnswer.text = strAnswer
        
        NotificationCenter.default.addObserver(self, selector: #selector(AskQuestionVC.setAnswer), name: Notification.Name("setAnswer"), object: nil)
    }

    //MARK:- NotificationCenter Method
    @objc func setAnswer(withNotification notification : NSNotification) {
        if AppUtilities.sharedInstance.isPostAns {
            AppUtilities.sharedInstance.isPostAns = false
            let index = notification.object as! Int
  
            var strTmpAnswer = ""
            if index == 0 {
                AppUtilities.sharedInstance.txtAns1.text = TRIM(string: AppUtilities.sharedInstance.txtAns1.text)
                strTmpAnswer = AppUtilities.sharedInstance.txtAns1.text
            } else if index == 1 {
                AppUtilities.sharedInstance.txtAns2.text = TRIM(string: AppUtilities.sharedInstance.txtAns2.text)
                strTmpAnswer = AppUtilities.sharedInstance.txtAns2.text
            } else if index == 2 {
                AppUtilities.sharedInstance.txtAns3.text = TRIM(string: AppUtilities.sharedInstance.txtAns3.text)
                strTmpAnswer = AppUtilities.sharedInstance.txtAns3.text
            } else if index == 3 {
                AppUtilities.sharedInstance.txtAns4.text = TRIM(string: AppUtilities.sharedInstance.txtAns4.text)
                strTmpAnswer = AppUtilities.sharedInstance.txtAns4.text
            } else if index == 4 {
                AppUtilities.sharedInstance.txtAns5.text = TRIM(string: AppUtilities.sharedInstance.txtAns5.text)
                strTmpAnswer = AppUtilities.sharedInstance.txtAns5.text
            }

            let dict = AppUtilities.sharedInstance.arrQuesAns.object(at: index) as! NSMutableDictionary
            dict.setObject(TRIM(string: strTmpAnswer), forKey: "answer" as NSCopying)
            AppUtilities.sharedInstance.arrQuesAns.replaceObject(at: index, with: dict)
        }
        
        print(AppUtilities.sharedInstance.arrQuesAns)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("setAnswer"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
