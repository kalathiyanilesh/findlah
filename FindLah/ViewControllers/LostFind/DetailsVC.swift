//
//  DetailsVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 21/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

protocol delegateUSD {
    func updateStatusFromDetail(dictStatus: NSDictionary, index: Int)
}

protocol delegateUpdateSubmiteStatus {
    func updateStatusForSubmit(dict: NSDictionary, index: Int)
}

class DetailsVC: UIViewController, delegateDetail, delegateUpdateStatus {
    func updateStatus(dictStatus: NSDictionary, index: Int) {
        self.delegateUSD?.updateStatusFromDetail(dictStatus: dictStatus, index: index)
    }
    
    func navigateToBack() {
        if isFromBranch {
            APP_DELEGATE.gotoHomeScreen()
        } else {
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    var isFromNotification:Bool = false
    var delegateUSD: delegateUSD?
    var delegateUpdateSubmiteStatus: delegateUpdateSubmiteStatus?
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblReward: UILabel!
    @IBOutlet var imgItem: UIImageView!
    
    @IBOutlet var lblDes: UILabel!
    @IBOutlet var lblMonth: UILabel!
    @IBOutlet var lblCategory: UILabel!
    @IBOutlet var lblLocation1: UILabel!
    @IBOutlet var lblLocation2: UILabel!
    @IBOutlet var lblLocation3: UILabel!
    @IBOutlet var lblRegion: UILabel!
    
    @IBOutlet var viewLoc1: UIView!
    @IBOutlet var viewLoc2: UIView!
    @IBOutlet var viewLoc3: UIView!
    @IBOutlet var lblRewardPrice: UILabel!
    @IBOutlet var btnReqOwnship: UIButton!
    
    @IBOutlet var viewPin: UIView!
    @IBOutlet var lblPinNo: UILabel!
    
    @IBOutlet var yRegion: NSLayoutConstraint!
    @IBOutlet var heightLocTitle: NSLayoutConstraint!
    @IBOutlet var heightLoc1: NSLayoutConstraint!
    @IBOutlet var heightLoc2: NSLayoutConstraint!
    @IBOutlet var heightLoc3: NSLayoutConstraint!
    
    @IBOutlet var yDelType: NSLayoutConstraint!
    @IBOutlet var heightDelType: NSLayoutConstraint!
    @IBOutlet var viewDelType: UIView!
    @IBOutlet var lblDelTypeTitle: UILabel!
    @IBOutlet var viewPopStation: UIView!
    @IBOutlet var lblPopStationName: UILabel!
    @IBOutlet var viewCourier: UIView!
    @IBOutlet var lblCourierAdd: UILabel!
    @IBOutlet var lblCourierDate: UILabel!
    
    var index: Int = 0
    var isFromPush: Bool = false
    var isAnswered: Bool = false
    var isFromFinder: Bool = false
    var dictReport: NSDictionary = NSDictionary()
    
    var isFromBranch: Bool = false
    
    @IBOutlet var heightPinDetail: NSLayoutConstraint!
    @IBOutlet var heightPSDetail: NSLayoutConstraint!
    @IBOutlet var heightCourDetail: NSLayoutConstraint!
    
    @IBOutlet var heightCourAdd: NSLayoutConstraint!
    @IBOutlet var heightCourDate: NSLayoutConstraint!
    //MARK:- VIEW  LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

        if APP_DELEGATE.isCallAPIs {
            APP_DELEGATE.isCallAPIs = false
            AppUtilities.sharedInstance.isCallFinderList = true
            AppUtilities.sharedInstance.isCallSeekerList = true
        }
        print(dictReport)
        setReportData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDetailData), name: Notification.Name("updateDetailData"), object: nil)
        
//        runAfterTime(time: 10) {
//            let dict: NSMutableDictionary = self.dictReport.mutableCopy() as! NSMutableDictionary
//            let dictPopStation = dict.object(forKey: "popstation_data") as! NSDictionary
//            dictPopStation.setValue("jhgsfkj BdlkhkJDHKA JBFKAJFKJBNAKBFKAN kwbkdbgkn rknkneklnjsknejg.", forKey: "popstation_name")
//            dictPopStation.setValue("jhfvh bjaebhfjbh kjfkqekfjkejfk kwekj engfnwlgknmlksm lsmelglkmeglml smkgl;ms;rglm;lr;lgklwrgkbk efkjekfwkehfkwekqjekjkejr kjeke.", forKey: "popstation_address")
//
//            dict.setObject(dictPopStation, forKey: "popstation_data" as NSCopying)
//            dict.setObject("jhfvh bjaebhfjbh kjfkqekfjkejfk kwekj engfnwlgknmlksm lsmelglkmeglml smkgl;ms;rglm;lr;lgklwrgkbk efkjekfwkehfkwekqjekjkejr kjeke. popstation_address jhfvh bjaebhfjbh kjfkqekfjkejfk kwekj engfnwlgknmlksm lsmelglkmeglml smkgl;ms;rglm;lr;lgklwrgkbk efkjekfwkehfkwekqjekjkejr kjeke.popstation_address jhfvh bjaebhfjbh kjfkqekfjkejfk kwekj engfnwlgknmlksm lsmelglkmeglml smkgl;ms;rglm;lr;lgklwrgkbk efkjekfwkehfkwekqjekjkejr kjeke.", forKey: "code" as NSCopying)
//
//            self.dictReport = dict.copy() as! NSDictionary
//            self.setReportData()
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isFromLaunch = false
        APP_DELEGATE.isInDetailScreen = true
        APP_DELEGATE.strDetailReportID = dictReport.object(forKey: "report_id") as! String
        
        /*
        imgBack.image = AppUtilities.sharedInstance.userType == SEEKER ? UIImage(named:"img_bg") : UIImage(named:"login_bg")
        if isFromNotification {
            imgBack.image = !isFromFinder ? UIImage(named:"img_bg") : UIImage(named:"login_bg")
        }*/
        
        imgBack.image = isFromFinder ? UIImage(named:"login_bg") : UIImage(named:"img_bg")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        APP_DELEGATE.isInDetailScreen = false
        APP_DELEGATE.strDetailReportID = "0"
    }
    
    @objc func updateDetailData(withNotification notification : NSNotification) {
        if APP_DELEGATE.isMethodCallOneTime {
            APP_DELEGATE.isMethodCallOneTime = false
            AppUtilities.sharedInstance.isCallFinderList = true
            AppUtilities.sharedInstance.isCallSeekerList = true
            dictReport = notification.userInfo! as NSDictionary
            setReportData()
        }
    }
    
    //MARK:- BUTTON ACTION
    @IBAction func btnBack(_ sender: Any) {
        gotoBack()
    }
    
    func gotoBack() {
        if isFromBranch {
            APP_DELEGATE.gotoHomeScreen()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnReqOwnship(_ sender: Any) {
        if dictReport.object(forKey: "user_id") as! String == AppUtilities.sharedInstance.getUserID() {
            let strStatus = dictReport.object(forKey: "status") as? String ?? ""
            if strStatus.count > 0 {
                let strDelType = dictReport.object(forKey: "delivery_type") as! String
                if strDelType == DELIVERY_TYPE.CONTACTME.rawValue {
                    let strReportType = dictReport.object(forKey: "report_type") as? String ?? ""
                    if strReportType == SEEKER {
                        //Seeker - who's item is lost
                        if strStatus == CONTACTME_TYPE.PENDING.rawValue || strStatus == CONTACTME_TYPE.REJECT.rawValue {
                            self.gotoBack()
                        } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue {
                            openPaymentAlert()
                        } else if strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                            if strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                                gotoFounderInfo(userID: dictReport.object(forKey: "user_id") as! String)
                            } else {
                                let arrStatus = dictReport.object(forKey: "report_que_ans") as! NSArray
                                let predict = NSPredicate(format: "request_status = %@", CONTACTME_TYPE.COMPLETE.rawValue)
                                let arrTmp = arrStatus.filtered(using: predict) as NSArray
                                if arrTmp.count > 0 {
                                    let dictData = arrTmp.object(at: 0) as! NSDictionary
                                    gotoFounderInfo(userID: dictData.object(forKey: "user_id") as! String)
                                }
                            }
                        }
                    }
                    else
                    {
                        //Founder - who's item is found
                        if strStatus == CONTACTME_TYPE.PENDING.rawValue || strStatus == CONTACTME_TYPE.REJECT.rawValue {
                            self.gotoBack()
                        } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue {
                            tellStatusItem(description: "Your item verify by admin successfully. We will update you once we found item owner.", short: false)
                        } else if strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                            print("complete")
                        } else {
                            self.gotoBack()
                        }
                    }
                    
                } else if strDelType == DELIVERY_TYPE.POPSTATION.rawValue {
                    if strStatus == POPSTATION_TYPE.PROCESSING.rawValue {
                        let strMsg = String(format:"Hello %@, Your pop station arrangement is currently being processed.. We will get back to you once details have been confirmed.",UserDefaults.standard.object(forKey: UD_UserName) as! String)
                        tellStatusItem(description: strMsg, short: false)
                    } else if strStatus == POPSTATION_TYPE.WAIT_FOR_ITEM_SUBMISSION.rawValue {
                        askForCorrectItem(description: "Have you submitted the right item?", type: "popstationsubmit")
                    } else if strStatus == POPSTATION_TYPE.SUBMITED.rawValue {
                        tellStatusItem(description: "Thanks for the item submission. We will verify and get back to you when a match is made.", short: false)
                    } else if strStatus == POPSTATION_TYPE.VERIFY_PENDING.rawValue {
                        tellStatusItem(description: "Your item verify by admin successfully. We will update you once we found item owner.", short: false)
                    } else if strStatus == POPSTATION_TYPE.REJECTED.rawValue {
                        tellStatusItem(description: "Sorry, Your item reject by admin.", short: true)
                    } else if strStatus == POPSTATION_TYPE.COMPLETE.rawValue {
                        
                    }
                } else if strDelType == DELIVERY_TYPE.COURIER.rawValue {
                    if strStatus == COURIER_TYPE.PROCESSING.rawValue {
                        //let strMsg = String(format:"Hello %@, Your courier arrangement is currently being processed. We will get back to you once details have been confirmed.",UserDefaults.standard.object(forKey: UD_UserName) as! String)
                        tellStatusItem(description: "Your Courier has been arranged. Please be available during the pick up timing. REMINDER: Place the item in the mailing bag that has been sent to your PO BOX.", short: false)
                    } else if strStatus == COURIER_TYPE.VERIFYED_DETAILS.rawValue {
                        tellStatusItem(description: "Your courier details have been successfully verified. IMPORTANT: A mailing bag has been sent to your POST BOX. Pls retrieve the mailing bag and place item inside before the courier pickup", short: false)
                    } else if strStatus == COURIER_TYPE.PROCESSING_COLCTN.rawValue {
                        askForCorrectItem(description: "Have you submitted the right item?", type: "couriersubmit")
                    } else if strStatus == COURIER_TYPE.SUBMITED.rawValue {
                        tellStatusItem(description: "Thanks for the item submission. We will verify and get back to you when a match is made.", short: false)
                    } else if strStatus == COURIER_TYPE.REJECT.rawValue {
                        tellStatusItem(description: "Sorry, Your item reject by admin.", short: true)
                    } else if strStatus == COURIER_TYPE.CONFIRM.rawValue {
                        tellStatusItem(description: "Your item verify by admin successfully. We will update you once we found item owner.", short: false)
                    } else if strStatus == COURIER_TYPE.COMPLETE.rawValue {
                        
                    }
                }
            } else {
                self.gotoBack()
            }
        } else {
            
            let strReportType = dictReport.object(forKey: "report_type") as? String ?? ""
            let strStatus = getReportStatus(dict: dictReport, needOnlyStatus: true)
            
            if strReportType == SEEKER {
                
                //Founder - who's item is found

                if strStatus.count > 0 {
                    if strStatus == CONTACTME_TYPE.PENDING.rawValue {
                        tellStatusItem(description: "We have received your item verification request. Our admin will verify your answers and update you shortly. Thank You.", short: false)
                    } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue {
                        tellStatusItem(description: "Your item verify by admin successfully. We will update you once we found item owner.", short: false)
                    } else if strStatus == CONTACTME_TYPE.REJECT.rawValue {
                        tellStatusItem(description: "Sorry, your item request has been rejected due to incorrect answers, insufficient info or inaccurate details provided.", short: false)
                    } else if strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                    }
                } else {
                    openQuesAns(dictReport: dictReport)
                }
            }
            else
            {
                //Seeker - who's item is lost

                if strStatus.count > 0 {
                    if strStatus == CONTACTME_TYPE.PENDING.rawValue {
                        tellStatusItem(description: "We have received your item verification request. Our admin will verify your answers and update you shortly. Thank You.", short: false)
                    } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue {
                        openPaymentAlert()
                    } else if strStatus == CONTACTME_TYPE.REJECT.rawValue {
                        tellStatusItem(description: "Sorry, your item request has been rejected due to incorrect answers, insufficient info or inaccurate details provided.", short: false)
                    } else if strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                        let strDelType = dictReport.object(forKey: "delivery_type") as! String
                        if strDelType == DELIVERY_TYPE.CONTACTME.rawValue {
                            gotoFounderInfo(userID: dictReport.object(forKey: "user_id") as! String)
                        } else {
                            //put here
                            tellStatusItem(description: "Our team will be contacting you shortly as we currently arrange for your item to be sent. Thank you.", short: false)
                        }
                        
                    }
                } else {
                    openQuesAns(dictReport: dictReport)
                }
            }
        }
    }
    
    func gotoFounderInfo(userID: String) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idEditProfile) as! EditProfile
        vc.isOtherProfile = true
        vc.strOtherUserID = userID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openQuesAns(dictReport: NSDictionary) {
        let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idQuestionVC) as! QuestionVC
        vc.dictReport = dictReport
        vc.isFromDetail = true
        vc.index = index
        vc.delegateDetail = self
        vc.delegateUpdateStatus = self
        vc.isFromFinder = isFromFinder
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension DetailsVC: displayAlertDelegate, AlertDelegate {
    func tellStatusItem(description: String, short: Bool) {
        if short {
            let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
            vc.AlertDelegate = self
            vc.strTitle = APPNAME
            vc.strDescription = description
            vc.type = "itemstatus"
            vc.strUserType = isFromFinder ? FINDER : SEEKER
            self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
        } else {
            let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
            vc.displayAlertDelegate = self
            vc.strTitle = APPNAME
            vc.strDescription = description
            vc.type = "itemstatus"
            vc.strUserType = isFromFinder ? FINDER : SEEKER
            self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
        }
    }
    
    func askForCorrectItem(description: String, type: String) {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = description
        vc.type = type
        vc.isHideOk = true
        vc.strUserType = isFromFinder ? FINDER : SEEKER
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func openPaymentAlert() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = "Please settle the reward before the info is released."
        vc.type = "payreward"
        vc.strUserType = isFromFinder ? FINDER : SEEKER
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            let dictParam: NSMutableDictionary = ["report_id":self.dictReport.object(forKey: "report_id") as! String, "user_id":AppUtilities.sharedInstance.getUserID()]
            if type == "popstationsubmit" {
                dictParam.setObject(POPSTATION_TYPE.SUBMITED.rawValue, forKey: "status" as NSCopying)
            } else if type == "couriersubmit" {
                dictParam.setObject(COURIER_TYPE.SUBMITED.rawValue, forKey: "status" as NSCopying)
            }
            
            sendUpdateStatus(dictParam: dictParam, responseData: {(error,responseDict) -> Void in
                hideMessage()
                if error != nil {
                    return
                } else {
                    print(responseDict ?? "Not Found")
                    if responseDict!["success"] as? String == "1"
                    {
                        self.dictReport.setValue(dictParam.object(forKey: "status") as! String, forKey: "status")
                        if type == "popstationsubmit" {
                            self.btnReqOwnship.setTitle("Item Submitted pop station", for: .normal)
                        } else if type == "couriersubmit" {
                            self.btnReqOwnship.setTitle("Submitted to courier", for: .normal)
                        }
                        self.delegateUpdateSubmiteStatus?.updateStatusForSubmit(dict: self.dictReport, index: self.index)
                    }
                    else {
                        showMessage(responseDict?.object(forKey: kMessage) as! String)
                    }
                }
            })
        }
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "payreward" {
                
                var strFounderUserID = "0"
                if self.dictReport.object(forKey: "user_id") as! String == AppUtilities.sharedInstance.getUserID() {
                    let strReportType = self.dictReport.object(forKey: "report_type") as? String ?? ""
                    if strReportType == SEEKER {
                        
                        let arrStatus = self.dictReport.object(forKey: "report_que_ans") as! NSArray
                        let predict = NSPredicate(format: "request_status = %@", CONTACTME_TYPE.VERIFIED.rawValue)
                        let arrTmp = arrStatus.filtered(using: predict) as NSArray
                        if arrTmp.count > 0 {
                            let dictData = arrTmp.object(at: 0) as! NSDictionary
                            strFounderUserID = dictData.object(forKey: "user_id") as! String
                        }
                        
                    } else {
                        strFounderUserID = self.dictReport.object(forKey: "user_id") as! String
                    }
                } else {
                    strFounderUserID = self.dictReport.object(forKey: "user_id") as! String
                }
                let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idPaymentVC) as! PaymentVC
                vc.dictReport = self.dictReport
                vc.strFounderUserID = strFounderUserID
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    /*
    func ChangeStatusInReportDict(status: String) {
        let arrStatus: NSMutableArray = (self.dictReport.object(forKey: "report_que_ans") as! NSArray).mutableCopy() as! NSMutableArray
        for i in 0..<arrStatus.count {
            let dict = arrStatus.object(at: i) as! NSDictionary
            let strUserID = dict.object(forKey: "user_id") as! String
            if strUserID == AppUtilities.sharedInstance.getUserID() {
                let dictNew: NSDictionary = ["status":status,
                                             "user_id":AppUtilities.sharedInstance.getUserID()]
                arrStatus.replaceObject(at: i, with: dictNew)
                
                let dictTmp: NSMutableDictionary = self.dictReport.mutableCopy() as! NSMutableDictionary
                dictTmp.setObject(arrStatus.copy() as! NSArray, forKey: "report_que_ans" as NSCopying)
                self.dictReport = dictTmp.copy() as! NSDictionary
                break
            }
        }
    }*/
}

extension DetailsVC {
    
    func setReportData() {
        print(dictReport)
        viewPin.isHidden = true
        self.lblPinNo.text = ""
 
        if dictReport.object(forKey: "user_id") as! String == AppUtilities.sharedInstance.getUserID() {
            if isFromFinder {
                let strDelType = dictReport.object(forKey: "delivery_type") as! String
                if strDelType == DELIVERY_TYPE.CONTACTME.rawValue {
                    hideDeliveryView()
                } else if strDelType == DELIVERY_TYPE.POPSTATION.rawValue {
                    lblDelTypeTitle.text = "POPStation"
                    let dictPopStation = dictReport.object(forKey: "popstation_data") as! NSDictionary
                    let strName = dictPopStation.object(forKey: "popstation_name") as? String
                    let strAddress = dictPopStation.object(forKey: "popstation_address") as? String
                    lblPopStationName.text = String(format: "%@\n%@",strName!,strAddress!)
                    
                    let PSDHeight = calculatedHeight(string: self.lblPopStationName.text!, withConstrainedWidth: SCREENWIDTH()-92, font: UIFont(name: "Montserrat-Medium", size: 12)!) + 12
                    heightPSDetail.constant = PSDHeight
                    
                    let strStatus = dictReport.object(forKey: "status") as? String ?? ""
                    if strStatus == POPSTATION_TYPE.WAIT_FOR_ITEM_SUBMISSION.rawValue {
                        let strPinNo = String(format: "Your POPStation Details: %@", dictReport.object(forKey: "code") as! String)
                        let pinHeight = calculatedHeight(string: strPinNo, withConstrainedWidth: SCREENWIDTH()-92, font: UIFont(name: "Montserrat-Bold", size: 12)!)
                        heightDelType.constant = pinHeight + PSDHeight + 36
                    } else {
                        heightDelType.constant = PSDHeight + 36 + 28
                    }
                    viewCourier.isHidden = true
                } else if strDelType == DELIVERY_TYPE.COURIER.rawValue {
                    lblDelTypeTitle.text = "Courier"
                    let dictCourier = dictReport.object(forKey: "courier_data") as! NSDictionary
                    let strCourierUnitNo = dictCourier.object(forKey: "courier_unitno") as? String ?? ""
                    let strCourierAddress = dictCourier.object(forKey: "courier_address") as? String ?? ""
                    if strCourierUnitNo.count > 0 {
                        lblCourierAdd.text = "\(strCourierUnitNo), \(strCourierAddress)"
                    } else {
                        lblCourierAdd.text = strCourierAddress
                    }
                    lblCourierDate.text = String(format:"%@  %@",dictCourier.object(forKey: "courier_date") as? String ?? "",dictCourier.object(forKey: "courier_pickup_time") as? String ?? "")
                    
                    let CourierAddHeight = calculatedHeight(string: self.lblCourierAdd.text!, withConstrainedWidth: SCREENWIDTH()-92, font: UIFont(name: "Montserrat-Medium", size: 12)!)
                    let CourierDateHeight = calculatedHeight(string: self.lblCourierDate.text!, withConstrainedWidth: SCREENWIDTH()-92, font: UIFont(name: "Montserrat-Medium", size: 12)!)
                    
                    heightCourAdd.constant = CourierAddHeight
                    heightCourDate.constant = CourierDateHeight
                        
                    heightCourDetail.constant = CourierAddHeight + CourierDateHeight + 32
                    heightDelType.constant =  heightCourDetail.constant + 28
                    viewPopStation.isHidden = true
                }
            } else {
                hideDeliveryView()
            }
            
            setUpForContactMe()
            
            let strDelType = dictReport.object(forKey: "delivery_type") as! String
            if isFromFinder {
                let strStatus = dictReport.object(forKey: "status") as? String ?? ""
                if strStatus.count > 0 {
                    if strDelType == DELIVERY_TYPE.POPSTATION.rawValue {
                        if strStatus == POPSTATION_TYPE.PROCESSING.rawValue {
                            btnReqOwnship.setTitle("Processing pop station", for: .normal)
                        } else if strStatus == POPSTATION_TYPE.WAIT_FOR_ITEM_SUBMISSION.rawValue {
                            viewPin.isHidden = false
                            self.lblPinNo.text = String(format: "Your POPStation Details: %@", dictReport.object(forKey: "code") as! String)
                            let pinHeight = calculatedHeight(string: self.lblPinNo.text!, withConstrainedWidth: SCREENWIDTH()-92, font: UIFont(name: "Montserrat-Bold", size: 12)!)
                            heightPinDetail.constant = pinHeight
                            heightDelType.constant = heightPSDetail.constant + pinHeight + 36
                            btnReqOwnship.setTitle("Item Submitted", for: .normal)
                        } else if strStatus == POPSTATION_TYPE.SUBMITED.rawValue {
                            btnReqOwnship.setTitle("Item Submitted to pop station", for: .normal)
                        } else if strStatus == POPSTATION_TYPE.VERIFY_PENDING.rawValue {
                            btnReqOwnship.setTitle("Item verified by admin", for: .normal)
                        } else if strStatus == POPSTATION_TYPE.REJECTED.rawValue {
                            btnReqOwnship.setTitle("Item rejected by admin", for: .normal)
                        } else if strStatus == POPSTATION_TYPE.COMPLETE.rawValue {
                            btnReqOwnship.setTitle("Complete", for: .normal)
                        }
                    } else if strDelType == DELIVERY_TYPE.COURIER.rawValue {
                        if strStatus == COURIER_TYPE.PROCESSING.rawValue {
                            btnReqOwnship.setTitle("Processing for courier", for: .normal)
                        } else if strStatus == COURIER_TYPE.VERIFYED_DETAILS.rawValue {
                            btnReqOwnship.setTitle("Courier details verified", for: .normal)
                        } else if strStatus == COURIER_TYPE.PROCESSING_COLCTN.rawValue {
                            btnReqOwnship.setTitle("Item Submitted", for: .normal)
                        } else if strStatus == COURIER_TYPE.SUBMITED.rawValue {
                            btnReqOwnship.setTitle("Submitted to courier", for: .normal)
                        } else if strStatus == COURIER_TYPE.REJECT.rawValue {
                            btnReqOwnship.setTitle("Item rejected by admin", for: .normal)
                        } else if strStatus == COURIER_TYPE.CONFIRM.rawValue {
                            btnReqOwnship.setTitle("Item verified by admin", for: .normal)
                        } else if strStatus == POPSTATION_TYPE.COMPLETE.rawValue {
                            btnReqOwnship.setTitle("Complete", for: .normal)
                        }
                    }
                }
            }
        } else {
            hideDeliveryView()
            setUpForContactMe()
        }
        
        /*
        if AppUtilities.sharedInstance.userType == SEEKER {
            setThemeColor(Color: REDCOLOR)
        }
        if isFromNotification {
            if isFromFinder {
                setThemeColor(Color: BLUECOLOR)
            } else {
                setThemeColor(Color: REDCOLOR)
            }
        }*/
        
        if isFromFinder {
            setThemeColor(Color: BLUECOLOR)
        } else {
            setThemeColor(Color: REDCOLOR)
        }
        
        imgItem.setImageWith(URL.init(string:dictReport.object(forKey: "report_image_1") as! String), placeholderImage: nil, options: .continueInBackground, completed: { (img, error, cachType, url) in
            if(img == nil)
            {
                self.imgItem.image = nil
            }
            else
            {
                self.imgItem.image = img
            }
        }, usingActivityIndicatorStyle: .gray)
        lblTitle.text = dictReport.object(forKey: "title") as? String
        if isAnswered {
            if isFromFinder {
                lblTitle.text = String(format: "Lost %@",dictReport.object(forKey: "category_name") as! String)
            } else {
                lblTitle.text = String(format: "Found %@",dictReport.object(forKey: "category_name") as! String)
            }
        }
        if let region = dictReport.object(forKey: "region") as? String {
            lblRegion.text = region
        }
        if lblRegion.text!.count <= 0 {
            lblRegion.text = "Region"
        }
        lblReward.text = "$\(dictReport.object(forKey: "amount") as? String ?? "0")"
        
        lblDes.text = dictReport.object(forKey: "description") as? String
        
        let strFromDate = dictReport.object(forKey: "from_date") as! String
        let strToDate = dictReport.object(forKey: "to_date") as! String
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone = TimeZone.current
        let dateFrom = dateFormatter.date(from: strFromDate)
        let dateTo = dateFormatter.date(from: strToDate)
        
        lblMonth.text = ""
        let strFromMonth = (dateFrom?.getMonthName())!
        let strToMonth = (dateTo?.getMonthName())!
        if strFromMonth == strToMonth {
            lblMonth.text = strFromMonth
        } else {
            lblMonth.text = String(format:"%@ - %@",strFromMonth,strToMonth)
        }
        
        let strUnitOne = dictReport.object(forKey: "unitno_1") as? String ?? ""
        let strUnitTwo = dictReport.object(forKey: "unitno_2") as? String ?? ""
        let strUnitThree = dictReport.object(forKey: "unitno_3") as? String ?? ""
        
        lblLocation1.text = dictReport.object(forKey: "address_1") as? String
        lblLocation2.text = dictReport.object(forKey: "address_2") as? String
        lblLocation3.text = dictReport.object(forKey: "address_3") as? String
        
        if strUnitOne.count > 0 {
            lblLocation1.text = String(format:"%@, %@",strUnitOne,lblLocation1.text!)
        }
        if strUnitTwo.count > 0 {
            lblLocation2.text = String(format:"%@, %@",strUnitTwo,lblLocation2.text!)
        }
        if strUnitThree.count > 0 {
            lblLocation3.text = String(format:"%@, %@",strUnitThree,lblLocation3.text!)
        }
        
        if lblLocation1.text!.count <= 0 {
            viewLoc1.isHidden = true
            heightLoc1.constant = 0
        }
        if lblLocation2.text!.count <= 0 {
            viewLoc2.isHidden = true
            heightLoc2.constant = 0
        }
        if lblLocation3.text!.count <= 0 {
            viewLoc3.isHidden = true
            heightLoc3.constant = 0
        }
        
        lblRewardPrice.text = "$\(dictReport.object(forKey: "amount") as? String ?? "0")"
        
        let cat1 = dictReport.object(forKey: "category_name") as! String
        lblCategory.text = cat1
        /*
        let cat2 = dictReport.object(forKey: "category_name_1") as! String
        let cat3 = dictReport.object(forKey: "category_name_2") as! String
        let arrCat = NSMutableArray()
        if cat1.count > 0 { arrCat.add(cat1) }
        if cat2.count > 0 { arrCat.add(cat2) }
        if cat3.count > 0 { arrCat.add(cat3) }
        lblCategory.text = (arrCat as NSArray).componentsJoined(by: ", ")*/
    }
    
    func setUpForContactMe() {

        let strReportType = dictReport.object(forKey: "report_type") as? String ?? ""
        if strReportType == SEEKER {
            if dictReport.object(forKey: "user_id") as! String == AppUtilities.sharedInstance.getUserID() {
                
                //Seeker - who's item is lost
                
                let strStatus = dictReport.object(forKey: "status") as? String ?? ""
                if strStatus == CONTACTME_TYPE.PENDING.rawValue || strStatus == CONTACTME_TYPE.REJECT.rawValue {
                    btnReqOwnship.setTitle("Done", for: .normal)
                } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue || strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                    btnReqOwnship.setTitle("Get finder info", for: .normal)
                }
            } else {
                
                //Founder - who's item is found
                
                let strStatus = getReportStatus(dict: dictReport, needOnlyStatus: true)
                if strStatus.count > 0 {
                    if strStatus == CONTACTME_TYPE.PENDING.rawValue {
                        setLocHide()
                        btnReqOwnship.setTitle("Request - In Progress", for: .normal)
                    } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue {
                        setLocHide()
                        btnReqOwnship.setTitle("Request - Verified", for: .normal)
                    } else if strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                        btnReqOwnship.setTitle("Request - Complete", for: .normal)
                    } else if strStatus == CONTACTME_TYPE.REJECT.rawValue {
                        setLocHide()
                        btnReqOwnship.setTitle("Request - Rejected", for: .normal)
                    }
                } else {
                    setLocHide()
                    btnReqOwnship.setTitle("I Find It", for: .normal)
                }
            }
            
        } else {
            if dictReport.object(forKey: "user_id") as! String == AppUtilities.sharedInstance.getUserID() {
                
                //Founder - who's item is found
                
                let strStatus = dictReport.object(forKey: "status") as? String ?? ""
                if strStatus == CONTACTME_TYPE.PENDING.rawValue || strStatus == CONTACTME_TYPE.REJECT.rawValue {
                    btnReqOwnship.setTitle("Done", for: .normal)
                } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue {
                    btnReqOwnship.setTitle("Request - Verified", for: .normal)
                } else if strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                    btnReqOwnship.setTitle("Request - Complete", for: .normal)
                } else  {
                    btnReqOwnship.setTitle("Done", for: .normal)
                }
                
            } else {
                
                //Seeker - who's item is lost
                
                let strStatus = getReportStatus(dict: dictReport, needOnlyStatus: true)
                if strStatus.count > 0 {
                    if strStatus == CONTACTME_TYPE.PENDING.rawValue {
                        setLocHide()
                        btnReqOwnship.setTitle("Request - In Progress", for: .normal)
                    } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue {
                        setLocHide()
                        btnReqOwnship.setTitle("Get finder info", for: .normal)
                    } else if strStatus == CONTACTME_TYPE.REJECT.rawValue {
                        setLocHide()
                        btnReqOwnship.setTitle("Request - Rejected", for: .normal)
                    } else if strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                        let strDelType = dictReport.object(forKey: "delivery_type") as! String
                        if strDelType == DELIVERY_TYPE.CONTACTME.rawValue {
                            btnReqOwnship.setTitle("Get finder info", for: .normal)
                        } else {
                            btnReqOwnship.setTitle("Request - Complete", for: .normal)
                        }
                    }
                } else {
                    setLocHide()
                    btnReqOwnship.setTitle("Request for ownership", for: .normal)
                }
                
            }
        }
    }
    
    func setThemeColor(Color:UIColor) {
        btnReqOwnship.backgroundColor = Color
        lblDes.textColor = Color
        lblMonth.textColor = Color
        lblCategory.textColor = Color
        lblLocation1.textColor = Color
        lblLocation2.textColor = Color
        lblLocation3.textColor = Color
        lblRewardPrice.textColor = Color
        lblRegion.textColor = Color
        lblPopStationName.textColor = Color
        lblCourierAdd.textColor = Color
        lblCourierDate.textColor = Color
    }
    
    func hideDeliveryView() {
        yDelType.constant = 0
        heightDelType.constant = 0
        viewDelType.isHidden = true
    }
    
    func setLocHide() {
        yRegion.constant = -12
        heightLocTitle.constant = 0
        viewLoc1.isHidden = true
        heightLoc1.constant = 0
        viewLoc2.isHidden = true
        heightLoc2.constant = 0
        viewLoc3.isHidden = true
        heightLoc3.constant = 0
    }
}
