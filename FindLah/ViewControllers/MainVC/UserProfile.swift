//
//  UserProfile.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 18/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit


class UserProfile: UIViewController, delegateReward {
    
    func updateRewardAmount(amount: String) {
        let finalAmnt = Double(self.total_Balance)! - Double(amount)!
        total_Balance = String(format: "%.2f",finalAmnt)
        lblReward.text = String(format: "$%.2f",finalAmnt)
    }

    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var imgEmail: UIImageView!
    @IBOutlet var imgPhone: UIImageView!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPhoneNo: UITextField!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnLogout: UIButton!
    @IBOutlet var lblFinder: UILabel!
    @IBOutlet var lblSeeker: UILabel!
    @IBOutlet var lblReward: UILabel!
    
    var lat_address:Double = 0.0
    var long_address:Double = 0.0
    var total_Balance: String = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sendGetCountReportReq()
        
        let dictUserInfo = UserDefaults.standard.object(forKey: UD_UserData) as! NSDictionary
        print(dictUserInfo)
        let countryCode = dictUserInfo.object(forKey: "country_code") as! String
        imgProfile.sd_setImage(with: URL(string:UserDefaults.standard.object(forKey: UD_UserProfileURL) as! String), placeholderImage:#imageLiteral(resourceName: "male_p"))
         
        txtEmail.text = UserDefaults.standard.object(forKey: UD_UserEmail) as? String
        txtPhoneNo.text = String(format:"+%@%@",countryCode,UserDefaults.standard.object(forKey: UD_UserMobileNo) as! String)
        if TRIM(string: txtPhoneNo.text!) == "+" {
            txtPhoneNo.text = ""
        }
        lblAddress.text = UserDefaults.standard.object(forKey: UD_UserAddresss) as? String
        lblName.text = UserDefaults.standard.object(forKey: UD_UserName) as? String
        
        setThemeColor()
    }

    //MARK:- UIBUTTON ACTION
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        openLogoutAlert()
    }
    
    @IBAction func btnEditProfile(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idEditProfile)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnFounders(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idFoundedItemsVC) as! FoundedItemsVC
        vc.type = FINDER
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLosters(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idFoundedItemsVC) as! FoundedItemsVC
        vc.type = SEEKER
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnRewards(_ sender: Any) {
        if Double(self.total_Balance)! <= 0 { return }
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idRewardsVC) as! RewardsVC
        vc.total_Balance = total_Balance
        vc.delegateReward = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension UserProfile {
    func sendGetCountReportReq() {
        let dictParam : NSDictionary = ["user_id":AppUtilities.sharedInstance.getUserID()]
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIGetCountReport,
            parameters: dictParam,
            showLoader:false)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    let dictCountInfo = dictionaryOfFilteredBy(dict: responseDict?["data"] as! NSDictionary)
                    self.lblFinder.text = dictCountInfo.object(forKey: "total_finder") as? String
                    self.lblSeeker.text = dictCountInfo.object(forKey: "total_seeker") as? String
                    self.lblReward.text = "$\(String(format: "%.2f",Double(dictCountInfo.object(forKey: "total_reward") as? String ?? "0")!))"
                    self.total_Balance = dictCountInfo.object(forKey: "total_reward") as! String
                }
            }
        }
    }
}

extension UserProfile: AlertDelegate {
    
    func openLogoutAlert() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = "Logout"
        vc.strDescription = "Are you sure\nYou want to logout?"
        vc.isHideOk = true
        vc.type = "logoutalert"
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
 
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            AppUtilities.sharedInstance.UserLogout()
        }
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnOk(type: String) {
        
    }
}

extension UserProfile {
    func setThemeColor() {
        if AppUtilities.sharedInstance.userType == SEEKER {
            btnLogout.backgroundColor = REDCOLOR
            imgBack.image = UIImage(named:"img_bg")
            imgEmail.image = UIImage(named:"ic_emailred")
            imgPhone.image = UIImage(named:"ic_phonered")
        } else {
            btnLogout.backgroundColor = BLUECOLOR
            imgBack.image = UIImage(named:"ic_background")
            imgEmail.image = UIImage(named:"ic_email_blue")
            imgPhone.image = UIImage(named:"ic_phone")
        }
    }
}


