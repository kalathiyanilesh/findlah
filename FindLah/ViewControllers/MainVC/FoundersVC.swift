//
//  FoundersVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 17/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit
import GoogleMaps

class FoundersVC: UIViewController,UISearchBarDelegate, delegateAddEditReport, delegateUpdateStatus, delegateUSD, GMSMapViewDelegate, delegateUpdateSubmiteStatus {
    
    func updateStatusForSubmit(dict: NSDictionary, index: Int) {
        arrReport.replaceObject(at: index, with: dict)
        tblFounders.reloadData()
    }
    
    func updateStatusFromDetail(dictStatus: NSDictionary, index: Int) {
        updateStatus(dictStatus: dictStatus, index: index)
    }
    
    func updateStatus(dictStatus: NSDictionary, index: Int) {
        let dict: NSMutableDictionary = (arrReport.object(at: index) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        var arrStatus: NSMutableArray = NSMutableArray()
        let arrTmp: NSArray = dict.object(forKey: "report_que_ans") as! NSArray
        if arrTmp.count > 0 {
            arrStatus = arrTmp.mutableCopy() as! NSMutableArray
        }
        arrStatus.add(dictStatus)
        dict.setObject(arrStatus, forKey: "report_que_ans" as NSCopying)
        arrReport.replaceObject(at: index, with: dict.copy() as! NSDictionary)
    }
    
    func updateRecord(dictData: NSDictionary, index: Int, isEdit: Bool) {
        if isEdit {
           arrReport.replaceObject(at: index, with: dictData)
        } else {
            arrReport.insert(dictData, at: 0)
        }
        tblFounders.reloadData()
    }
    
    @IBOutlet var tblFounders: UITableView!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var searchItem: UISearchBar!
    @IBOutlet var btnMap: UIButton!
    @IBOutlet var viewMap: UIView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var imgItem: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblReward: UILabel!
    @IBOutlet var viewMarkInfo: UIView!
    var markers = [GMSMarker]()
    var indexDelete: Int = 0
    var SelectedIndex: Int = 0
    var pointNow = CGPoint.zero
    var isAnswered: Bool = false
    var offset = 1
    var isSend = false
    var refreshControl = UIRefreshControl()
    var arrReport: NSMutableArray = NSMutableArray()
    var dictReport: NSDictionary = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewMap.alpha = 0
        if isAnswered { btnAdd.isHidden = true }
        tblFounders.register(UINib.init(nibName: "LostFindCell", bundle: nil), forCellReuseIdentifier: "LostFindCell")
        
        tblFounders.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(self.pulltorefresh), for: .valueChanged)
        showLoaderHUD(strMessage: "")
        pulltorefresh()
        
        tblFounders.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.offset += 1
                self.sendGetFinderReportReq()
            }else {
                self.tblFounders.infiniteScrollingView.stopAnimating()
            }
        }))

        //NotificationCenter.default.addObserver(self, selector: #selector(self.getFinderList), name: Notification.Name("getFinderList"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setFilter), name: Notification.Name("filterfinderdata"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoPushToDetail), name: Notification.Name("gotoDetail"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshControl.endRefreshing()
        setThemeColor()
        if AppUtilities.sharedInstance.isCallFinderList {
            AppUtilities.sharedInstance.isCallFinderList = false
            pulltorefresh()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        refreshControl.endRefreshing()
    }
    
    //MARK:- NotificationCenter Method
    
    /*
    @objc func getFinderList(withNotification notification : NSNotification) {
        if AppUtilities.sharedInstance.isGetFinderList {
            AppUtilities.sharedInstance.isGetFinderList = false
            isSend = false
            offset = 1
            sendGetFinderReportReq()
        }
    }*/
    
    @objc func setFilter(withNotification notification : NSNotification) {
        if AppUtilities.sharedInstance.isCallFilterFinder {
            AppUtilities.sharedInstance.isCallFilterFinder = false
            searchItem.text = ""
            arrReport.removeAllObjects()
            tblFounders.reloadData()
            showLoaderHUD(strMessage: "")
            pulltorefresh()
        }
    }
    
    @objc func gotoPushToDetail(withNotification notification : NSNotification) {
        if APP_DELEGATE.isGoToDetail {
            APP_DELEGATE.isGoToDetail = false
            let dictData = notification.userInfo! as NSDictionary
            let strReportType = dictData.object(forKey: "report_type") as? String ?? ""
            let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idDetailsVC) as! DetailsVC
            vc.dictReport = dictData
            vc.isAnswered = isAnswered
            vc.isFromFinder = false
            if strReportType == FINDER {
                vc.isFromFinder = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- Refresh Control Method
    @objc func pulltorefresh() {
        if self.isSend == false {
            self.offset = 1
            self.sendGetFinderReportReq()
        }
    }

    //MARK:- BUTTON ACTION
    @IBAction func btnAdd(_ sender: Any) {
        if AppUtilities.sharedInstance.isLogin() == "0" {
            AppUtilities.sharedInstance.logout(userType: FINDER)
        } else {
            whoBuddy()
        }
    }
    
    @IBAction func btnMap(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.viewMap.alpha = 1
        }
        setUpMap()
    }
    
    @IBAction func btnCurrentLoc(_ sender: Any) {
        let camera = GMSCameraPosition.camera(withLatitude: lat_currnt, longitude: long_currnt, zoom: 16)
        mapView.camera = camera
        mapView.animate(to: camera)
    }
    
    @IBAction func btnCloseMap(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.viewMap.alpha = 0
        }
    }
    
    deinit {
        //NotificationCenter.default.removeObserver(self, name: Notification.Name("getFinderList"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("filterfinderdata"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension FoundersVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return AppUtilities.sharedInstance.setNoDataLabel(tableView: tableView, array: arrReport, text: "Report Not Found.")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReport.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LostFindCell")! as! LostFindCell
        let dictData = arrReport.object(at: indexPath.row) as! NSDictionary
        cell.setDataToRow(dictReport: dictData)
        cell.btnDetail.addTarget(self, action: #selector(btnDetail(sender:)), for: .touchUpInside)
        cell.imgDetail.image = UIImage(named:"ic_detail")
        cell.btnMore.addTarget(self, action: #selector(btnMore(sender:)), for: .touchUpInside)
        
        cell.btnMore.isHidden = false
        cell.lblStatus.isHidden = true
        if isAnswered {
            cell.btnMore.isHidden = true
            cell.lblTitle.text = String(format: "Lost %@",dictData.object(forKey: "category_name") as! String)
            cell.lblStatus.isHidden = false
            //cell.lblStatus.text = getReportStatus(dict: dictData, needOnlyStatus: false)
            cell.lblStatus.text = getStatusForAnsweredReport(dictReport: dictData)
            cell.lblStatus.textColor = BLUECOLOR
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tapOnReport(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tapOnReport(indexPath: IndexPath) {
        if AppUtilities.sharedInstance.isLogin() == "0" {
            AppUtilities.sharedInstance.logout(userType: FINDER)
        } else {
            SelectedIndex = indexPath.row
            dictReport = arrReport.object(at: indexPath.row) as! NSDictionary
            if dictReport.object(forKey: "user_id") as! String != AppUtilities.sharedInstance.getUserID() {
                let strStatus = getReportStatus(dict: dictReport, needOnlyStatus: true)
                if strStatus.count > 0 {
                    if strStatus == CONTACTME_TYPE.PENDING.rawValue {
                        tellStatusItem(description: "We have received your item verification request. Our admin will verify your answers and update you shortly. Thank You.", short: false)
                    } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue || strStatus == CONTACTME_TYPE.REJECT.rawValue || strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                        gotoDetail(dict: dictReport, index: indexPath.row)
                    } else {
                        askForLoseItem()
                    }
                } else {
                    askForLoseItem()
                }
            } else {
                let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idDetailsVC) as! DetailsVC
                vc.dictReport = self.dictReport
                vc.isFromFinder = true
                vc.isAnswered = isAnswered
                vc.delegateUpdateSubmiteStatus = self
                vc.index = indexPath.row
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @objc func btnDetail(sender:UIButton) {
        if AppUtilities.sharedInstance.isLogin() == "0" {
            AppUtilities.sharedInstance.logout(userType: FINDER)
        } else {
            let buttonPosition = sender.convert(CGPoint.zero, to: tblFounders)
            let indexPath = tblFounders.indexPathForRow(at: buttonPosition)
            gotoDetail(dict: arrReport.object(at: indexPath!.row) as! NSDictionary, index: (indexPath?.row)!)
        }
    }
    
    func gotoDetail(dict: NSDictionary, index: Int) {
        let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idDetailsVC) as! DetailsVC
        vc.dictReport = dict
        vc.isFromFinder = true
        vc.delegateUSD = self
        vc.index = index
        vc.isAnswered = isAnswered
        vc.delegateUpdateSubmiteStatus = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnMore(sender:UIButton) {
        if AppUtilities.sharedInstance.isLogin() == "0" {
            AppUtilities.sharedInstance.logout(userType: FINDER)
        } else {
            let buttonPosition = sender.convert(CGPoint.zero, to: tblFounders)
            let indexPath = tblFounders.indexPathForRow(at: buttonPosition)
            
            let dictReport = arrReport.object(at: indexPath!.row) as! NSDictionary
            
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                print("Cancel")
            }
            actionSheetController.addAction(cancelActionButton)
            
            let saveActionButton = UIAlertAction(title: "Edit", style: .default)
            { _ in
                let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idAddFoundItemVC) as! AddFoundItemVC
                vc.isEdit = true
                vc.index = (indexPath?.row)!
                vc.dictReport = dictReport
                vc.delegateAddEditReport = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            let deleteActionButton = UIAlertAction(title: "Delete", style: .default)
            { _ in
                self.indexDelete = indexPath!.row
                self.openDeleteAlert()
            }
            
            let shareActionButton = UIAlertAction(title: "Share", style: .default)
            { _ in
                let objShare = shareContent_DeepLinking(dicMess: dictReport)
                objShare.getShortUrl(with: get_BranchLinkProperties()) { (url, error) in
                    if error == nil {
                        print(url ?? "not found")
                        
                        let shareItems = [url!]
                        let activityViewController = UIActivityViewController(activityItems: shareItems , applicationActivities: nil)
                        activityViewController.popoverPresentationController?.sourceView = self.view
                        self.present(activityViewController, animated: true, completion: nil)
                    }
                    else {
                        print("got my Branch link : Error : %@", error?.localizedDescription as Any)
                        showMessage(error?.localizedDescription ?? "Something wrong")
                    }
                }
            }
            
            if dictReport.object(forKey: "user_id") as! String == AppUtilities.sharedInstance.getUserID() {
                actionSheetController.addAction(saveActionButton)
                actionSheetController.addAction(deleteActionButton)
                actionSheetController.addAction(shareActionButton)
            } else {
                actionSheetController.addAction(shareActionButton)
            }
            
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
}

extension FoundersVC {
    func sendGetFinderReportReq() {
        isSend = true
        let strIsAnswered = isAnswered == true ? "1" : "0"
        let strUserId = isAnswered == true ? AppUtilities.sharedInstance.getUserID() : ""
        let strSearchText: String = searchItem != nil ? searchItem.text ?? "" : ""
        
        var strDistance = ""
        var strLattitude = ""
        var strLongitude = ""
        var strRegion = ""
        var strCategoryIDs = ""
        
        var isFilter = false
        if !isAnswered {
            if let dictData = UserDefaults.standard.object(forKey: "filterdata") {
                isFilter = true
                let dictFilterData = dictData as! NSDictionary
                if dictFilterData.object(forKey: "reset") as? String ?? "0" == "0" {
                    if (dictFilterData.object(forKey: "distance") != nil) {
                        strDistance = dictFilterData.object(forKey: "distance") as! String
                        strLattitude = "\(lat_currnt)"
                        strLongitude = "\(long_currnt)"
                        
                        if let arrSelectedCategory = dictFilterData.object(forKey: "categorys") as? NSArray {
                            let arrCatIDs = arrSelectedCategory.value(forKeyPath: "category_id") as! NSArray
                            strCategoryIDs = arrCatIDs.componentsJoined(by: ",")
                        }
                        
                        if let arrSelectedRegion = dictFilterData.object(forKey: "region") as? NSArray {
                            strRegion = arrSelectedRegion.componentsJoined(by: ",")
                        }
                    }
                }
            }
        }

        let dictParam: NSDictionary = ["user_id":strUserId,
                                       "report_type":FINDER,
                                       "is_answered":strIsAnswered,
                                       "search_text":strSearchText,
                                       "page":"\(offset)",
                                       "length":"\(LimitItem)",
                                       "distance" : strDistance,
                                       "latitude" : strLattitude,
                                       "longitude" : strLongitude,
                                       "region" : strRegion,
                                       "category_id" : strCategoryIDs]
        
        print("finder: ",dictParam)
        
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIGetReport,
            parameters: dictParam,
            showLoader:false)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                self.refreshControl.endRefreshing()
                self.isSend = false
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    if self.offset == 1 {
                        self.arrReport = NSMutableArray.init()
                        self.arrReport = arrayOfFilteredBy(arr: responseDict?.object(forKey: kData) as! NSArray).mutableCopy() as! NSMutableArray
                        if self.arrReport.count > 0{
                            self.tblFounders.showsInfiniteScrolling = true
                        }
                    } else {
                        let arrOtherData = arrayOfFilteredBy(arr: responseDict?.object(forKey: kData) as! NSArray).mutableCopy() as! NSMutableArray
                        if arrOtherData.count < LimitItem {
                            self.tblFounders.showsInfiniteScrolling = false
                        }
                        self.arrReport.addObjects(from: arrOtherData as! [Any])
                        self.tblFounders.infiniteScrollingView.stopAnimating()
                    }
                    DispatchQueue.main.async {
                        self.tblFounders.reloadData()
                    }
                } else {
                    if self.offset != 1 {
                        self.tblFounders.infiniteScrollingView.stopAnimating()
                    }
                }
                
                if self.arrReport.count <= 0 { self.tblFounders.showsInfiniteScrolling = false }
                self.refreshControl.endRefreshing()
                self.isSend = false
                
                if isFilter && self.viewMap.alpha == 1{
                    self.setUpMap()
                }
            }
        }
    }
}

extension FoundersVC {
    func searchBarSearchButtonClicked( _ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (TRIM(string: searchText).count) > 0 {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(search), object: nil)
            self.perform(#selector(search), with: nil, afterDelay: 0.5)
        } else {
        }
    }
    
    @objc func search() {
        self.arrReport = NSMutableArray.init()
        self.tblFounders.reloadData()
        pulltorefresh()
    }
}

extension FoundersVC: whoBuddyDelegate {
    func whoBuddy() {
        let vc : WhoBuddyVC = WhoBuddyVC(nibName: idWhoBuddyVC, bundle: nil)
        vc.whoBuddyDelegate = self
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnDone() {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if AppUtilities.sharedInstance.userType == SEEKER {
                let dictUserInfo = UserDefaults.standard.object(forKey: UD_UserData) as! NSDictionary
                if dictUserInfo.object(forKey: "is_id_submited") as! String == "0" {
                    self.askForLoseID()
                } else if dictUserInfo.object(forKey: "is_id_submited") as! String == "2" {
                    AppUtilities.sharedInstance.tellProofPending()
                } else if dictUserInfo.object(forKey: "is_id_submited") as! String == "3" {
                    self.tellProofRejected()
                } else {
                    self.setUser()
                }
            } else {
                self.setUser()
            }
        }
    }
    
    func btnCancelSelection() {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func setUser() {
        self.tblFounders.reloadData()
        self.setThemeColor()
        NotificationCenter.default.post(name: Notification.Name("changebackground"), object: nil)
        let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idAddFoundItemVC) as! AddFoundItemVC
        vc.delegateAddEditReport = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension FoundersVC: AlertDelegate, displayAlertDelegate {
    
    func tellStatusItem(description: String, short: Bool) {
        if short {
            let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
            vc.AlertDelegate = self
            vc.strTitle = APPNAME
            vc.strDescription = description
            vc.type = "itemstatus"
            vc.strUserType = FINDER
            self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
        } else {
            let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
            vc.displayAlertDelegate = self
            vc.strTitle = APPNAME
            vc.strDescription = description
            vc.type = "itemstatus"
            vc.strUserType = FINDER
            self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
        }
    }
    
    func openDeleteAlert() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = "Delete"
        vc.strDescription = "Are you sure you want to delete this report?"
        vc.type = "askdelete"
        vc.strUserType = FINDER
        vc.isHideOk = true
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }

    func askForLoseItem() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = "Did you lose this item?"
        vc.type = "loseitem"
        vc.strUserType = FINDER
        vc.isHideOk = true
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func askForLoseID() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = "You’re almost ready."
        vc.strDescription = "Did you lose your ID or NRIC?"
        vc.isHideOk = true
        vc.type = "askforlostid"
        vc.strUserType = FINDER
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func tellProofRejected() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = "Your ID Proof is Rejected by the Management. Re-submit a clear and accurate Proof now?"
        vc.type = "proofreject"
        vc.isHideOk = true
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "askdelete" {
                let dictReport = self.arrReport.object(at: self.indexDelete) as! NSDictionary
                AppUtilities.sharedInstance.sendDeleteReportReq(reportID: dictReport.object(forKey: "report_id") as! String)
                self.arrReport.removeObject(at: self.indexDelete)
                self.tblFounders.reloadData()
            } else if type == "loseitem" {
                let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idQuestionVC) as! QuestionVC
                vc.dictReport = self.dictReport
                vc.index = self.SelectedIndex
                vc.delegateUpdateStatus = self
                vc.isFromFinder = true
                self.navigationController?.present(vc, animated: true, completion: nil)
            } else if type == "askforlostid" {
                let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
                vc.displayAlertDelegate = self
                vc.strTitle = kIMPNOTICE
                vc.strDescription = kLATESTPHONEBILL
                vc.type = kBILLINGBILL
                vc.strUserType = FINDER
                self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
            } else if type == "proofreject" {
                self.askForLoseID()
            }
        }
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "askforlostid" {
                let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
                vc.displayAlertDelegate = self
                vc.strTitle = kIMPNOTICE
                vc.strDescription = kPHOTOIDNRIC
                vc.type = kHIMID
                vc.strUserType = FINDER
                self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
            }
        }
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == kBILLINGBILL || type == kHIMID {
                self.openImagePicker(type: type)
            }
        }
    }
}

extension FoundersVC: ImagePickerDelegate{
    
    func openImagePicker(type: String) {
        self.view.endEditing(true)
        ImagePicker.sharedInstance.delegate = self
        ImagePicker.sharedInstance.strUserType = FINDER
        ImagePicker.sharedInstance.selectImage(sender: type)
    }
    
    func pickImageComplete(_ imageData: UIImage, sender: String)
    {
        if sender == kBILLINGBILL || sender == kHIMID {
            let strIDProof = sender == kHIMID ? "1" : "0"
            sendUploadIdProof(image: imageData, strIsIDProof: strIDProof, responseData: {(error,responseDict) -> Void in
                hideMessage()
                if error != nil {
                    return
                } else {
                    print(responseDict ?? "Not Found")
                    if responseDict!["success"] as? String == "1"
                    {
                        self.setUser()
                    }
                    else {
                        showMessage(responseDict?.object(forKey: kMessage) as! String)
                    }
                }
            })
        }
    }
}

extension FoundersVC {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        pointNow = scrollView.contentOffset
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if arrReport.count <= 0  || isAnswered  { return }
        if scrollView.contentOffset.y > pointNow.y {
            UIView.animate(withDuration: 0.25, animations: {
                self.btnAdd.frame.origin.y = self.view.frame.size.height
                self.btnMap.frame.origin.y = self.view.frame.size.height
                self.view.layoutIfNeeded()
            })
        } else if scrollView.contentOffset.y < pointNow.y {
            UIView.animate(withDuration: 0.25, animations: {
                self.btnAdd.frame.origin.y = self.view.frame.size.height - (self.btnAdd.frame.size.height + 14)
                self.btnMap.frame.origin.y = self.view.frame.size.height - (self.btnMap.frame.size.height + 17)
                self.view.layoutIfNeeded()
            })
        }
        self.view.endEditing(true)
    }
}

extension FoundersVC {
    func setThemeColor() {
        /*
        if (btnAdd != nil) {
            if AppUtilities.sharedInstance.userType == SEEKER {
                btnAdd.setImage(UIImage(named:"ic_addred"), for: .normal)
            } else {
                btnAdd.setImage(UIImage(named:"ic_add"), for: .normal)
            }
        }*/
        if (btnAdd != nil) {
            btnAdd.setImage(UIImage(named:"ic_add"), for: .normal)
        }
        
    }
}

extension FoundersVC {
    func setUpMap() {
        
        let camera = GMSCameraPosition.camera(withLatitude: lat_currnt, longitude: long_currnt, zoom: 16)
        mapView.camera = camera
        mapView.animate(to: camera)
        
        mapView.clear()
        markers.removeAll()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        //var bounds = GMSCoordinateBounds()
        for i in 0..<arrReport.count {
            var coords: CLLocationCoordinate2D = CLLocationCoordinate2D()
            let dict: NSDictionary = arrReport.object(at: i) as! NSDictionary
            coords.latitude = Double(dict.object(forKey: "latitude_1") as? String ?? "0") ?? 0
            coords.longitude = Double(dict.object(forKey: "longitude_1") as? String ?? "0") ?? 0

            let marker = GMSMarker()
            marker.icon = UIImage(named:"ic_pin")
            marker.position = coords
            marker.title = dict.object(forKey: "title") as? String
            marker.snippet = dict.object(forKey: "description") as? String
            
            marker.map = mapView
            markers.append(marker)
//            bounds = bounds.includingCoordinate(marker.position)
        }
        
//        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
//        mapView.animate(with: update)
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if let index = markers.index(of: marker) {
            tapOnReport(indexPath: IndexPath(row: index, section: 0))
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        if let index = markers.index(of: marker) {
           let dictData = arrReport.object(at: index) as! NSDictionary
            imgItem.sd_setImage(with:URL(string:dictData.object(forKey: "report_image_1") as! String), placeholderImage:nil, options:.progressiveDownload)
            lblTitle.text = dictData.object(forKey: "title") as? String
            lblDesc.text = dictData.object(forKey: "description") as? String
            lblReward.text = "$0"
            if dictData.object(forKey: "is_reward") as! String == "1" {
                lblReward.text = "$\(dictData.object(forKey: "amount") as? String ?? "0")"
            }
        }
        return viewMarkInfo
    }
}


