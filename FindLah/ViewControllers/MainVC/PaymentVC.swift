//
//  PaymentVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 18/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit
import Stripe
//import BraintreeDropIn
import GooglePlaces

class PaymentVC: UIViewController, STPPaymentCardTextFieldDelegate {

    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var imgCard: UIImageView!
    @IBOutlet var btnCreditCard: UIButton!
    @IBOutlet var btnPaypal: UIButton!
    @IBOutlet var btnDone: UIButton!
    
    @IBOutlet var txtFname: UITextField!
    @IBOutlet var txtLname: UITextField!
    @IBOutlet var paymentTextField: STPPaymentCardTextField!
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtAddress: UITextField!
    @IBOutlet var viewPaypal: CardView!
    @IBOutlet var txtUnitNo: UITextField!
    
    var chargableAmount:Float = 0
    var dictReport: NSDictionary = NSDictionary()
    var paymentNonce: String = ""
    var strFounderUserID: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPaypal.isHidden = true
        if let amount = (dictReport.object(forKey: "amount") as! String).float(){
            chargableAmount = amount
        }else{
            chargableAmount = 0
        }
        
        if AppUtilities.sharedInstance.userType == SEEKER {
            imgCard.image = UIImage(named:"ic_creditcardred")
            btnCreditCard.setImage(UIImage(named:"ic_selectedred"), for: .selected)
            btnPaypal.setImage(UIImage(named:"ic_selectedred"), for: .selected)
            btnDone.backgroundColor = REDCOLOR
        }
        
        sendGenerateNonceReq()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imgBack.image = AppUtilities.sharedInstance.userType == SEEKER ? UIImage(named:"img_bg") : UIImage(named:"login_bg")
    }

    //MARK:- BUTTON ACTION
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnCreditCard(_ sender: Any) {
        btnCreditCard.isSelected = true
        btnPaypal.isSelected = false
    }
    
    @IBAction func btnAddress(_ sender: Any) {
        openGoogleAutoComplete()
    }
    
    @IBAction func btnPaypal(_ sender: Any) {
        if self.paymentNonce.count > 0 {
            btnCreditCard.isSelected = false
            btnPaypal.isSelected = true
            showDropIn(clientTokenOrTokenizationKey: self.paymentNonce)
        }
    }
    
    @IBAction func btnDone(_ sender: Any) {
        if btnCreditCard.isSelected && validate() {
            makeStripePayment()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension PaymentVC {
    func makeStripePayment() {
        print(self.paymentTextField!.cardParams.number!)

        let address: STPAddress = STPAddress()
        address.city = txtCity.text!
        address.country = txtCountry.text!
        address.line1 = getAddressWithHouseNo()
        
        let card: STPCardParams = STPCardParams()
        card.name = txtFname.text! + txtLname.text!
        card.number = paymentTextField.cardNumber
        card.expMonth = paymentTextField.expirationMonth
        card.expYear = paymentTextField.expirationYear
        card.cvc = paymentTextField.cvc
        card.address = address
        
        if chargableAmount >= 1 {
            showLoaderHUD(strMessage: "")
            self.view.endEditing(true)
            paymentTextField?.resignFirstResponder()
            self.view.endEditing(true)
            STPAPIClient.shared().createToken(withCard: (paymentTextField?.cardParams)!, completion: {(_ token: STPToken?, _ error: Error?) -> Void in
                if error != nil {
                    print(error?.localizedDescription ?? "Error is nil")
                    hideLoaderHUD()
                    showMessage((error?.localizedDescription)!)
                } else {
                    print("TokenID ",token?.tokenId ?? "Not Found")
                    self.sendPaymentReq(token: (token?.tokenId)!, paymentType: "stripe")
                }

            })
        } else {
            showMessage("Need Valid Amount")
        }
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        //btnDone?.isUserInteractionEnabled = textField.isValid
    }
    
    
    //BRAINTREE INTIGRATION
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print(error?.localizedDescription ?? "Error")
                showMessage(error?.localizedDescription ?? "Error")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let result = result {
                // Use the BTDropInResult properties to update your UI
                // result.paymentOptionType
                // result.paymentMethod
                // result.paymentIcon
                // result.paymentDescription
                let nonce: String = result.paymentMethod?.nonce ?? ""
                print(nonce)
                showLoaderHUD(strMessage: "")
                self.sendPaymentReq(token: nonce, paymentType: "braintree")
            }
            controller.dismiss(animated: true, completion: nil)
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
}

extension PaymentVC {
    func sendPaymentReq(token: String, paymentType: String) {
        let dictParam: NSMutableDictionary = ["user_id":AppUtilities.sharedInstance.getUserID(),
                                       "token":token,
                                       "report_id":dictReport.object(forKey: "report_id") as! String,
                                       "amount":"\(chargableAmount)",
                                        "first_name":"",
                                        "last_name":"",
                                        "card_number":"",
                                        "expire_date":"",
                                        "cvv":"",
                                        "country":"",
                                        "city":"",
                                        "address":"",
                                        "payment_type":paymentType]
        
        if paymentType == "stripe" {
            let expDate = String(format:"%d/%d",paymentTextField.expirationMonth,paymentTextField.expirationYear)
            dictParam.setObject(txtFname.text!, forKey: "first_name" as NSCopying)
            dictParam.setObject(txtLname.text!, forKey: "last_name" as NSCopying)
            dictParam.setObject(paymentTextField.cardNumber!, forKey: "card_number" as NSCopying)
            dictParam.setObject(expDate, forKey: "expire_date" as NSCopying)
            dictParam.setObject(paymentTextField.cvc!, forKey: "cvv" as NSCopying)
            dictParam.setObject(txtCountry.text!, forKey: "country" as NSCopying)
            dictParam.setObject(txtCity.text!, forKey: "city" as NSCopying)
            dictParam.setObject(getAddressWithHouseNo(), forKey: "address" as NSCopying)
        }
        
        print("payment param ",dictParam)
        
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIPayment,
            parameters: dictParam.copy() as! NSDictionary,
            showLoader:false)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    self.openPaymentSuccessAlert()
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
    
    func sendGenerateNonceReq() {
        let dictParam: NSDictionary = NSDictionary()
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIGenerateNonce,
            parameters: dictParam,
            showLoader:false)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    self.paymentNonce = responseDict!["nonce"] as! String
                } else {
                    
                }
            }
        }
    }
}

extension PaymentVC: displayAlertDelegate, AlertDelegate {
    func openPaymentSuccessAlert() {
        let strDelType = dictReport.object(forKey: "delivery_type") as! String
        var strDescription = "Thank You for your payment. Your items will be mailed or couriered to your address. Please ensure your contact details are updated and that someone is available for the collection in the next 3-7 days."
        if strDelType == DELIVERY_TYPE.POPSTATION.rawValue || strDelType == DELIVERY_TYPE.COURIER.rawValue {
            strDescription = "Our team will be contacting you shortly as we currently arrange for your item to be sent. Thank you."
        }
        
        let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
        vc.displayAlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = strDescription
        vc.type = "paymentsuccess"
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "paymentsuccess" {
                let strDelType = self.dictReport.object(forKey: "delivery_type") as! String
                if strDelType == DELIVERY_TYPE.POPSTATION.rawValue || strDelType == DELIVERY_TYPE.COURIER.rawValue {
                    let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idSWRevealViewController)
                    UIView.transition(with: self.navigationController!.view, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: { (finished) in
                        self.navigationController?.pushViewController(vc, animated: false)
                    })
                } else {
                    let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idEditProfile) as! EditProfile
                    vc.isOtherProfile = true
                    vc.isFromPaymentVC = true
                    vc.strOtherUserID = self.strFounderUserID
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}

extension PaymentVC {
    func validate() -> Bool {
        if !validateTxtFieldLength(txtFname, withMessage: EnterFname) {
            return false
        }
        if !validateTxtFieldLength(txtLname, withMessage: EnterLname) {
            return false
        }
        if !paymentTextField.isValid {
            showMessage(EnterCardDetail)
            return false
        }
        if !validateTxtFieldLength(txtCountry, withMessage: EnterCountry) {
            return false
        }
        if !validateTxtFieldLength(txtCity, withMessage: EnterCity) {
            return false
        }
        if !validateTxtFieldLength(txtAddress, withMessage: EnterAddress) {
            return false
        }
        return true
    }
    
    func getAddressWithHouseNo() -> String {
        var strAddress = txtAddress.text!
        if TRIM(string: txtUnitNo.text ?? "").count > 0
        {
            strAddress = String(format:"%@, %@",txtUnitNo.text ?? "0",txtAddress.text!)
        }
        return strAddress
    }
}

extension PaymentVC: GMSAutocompleteViewControllerDelegate {
    
    func openGoogleAutoComplete() {
        self.view.endEditing(true)
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place address: \(place.coordinate.latitude)")
        print("Place attributions: \(place.coordinate.longitude)")
        
        txtAddress.text = place.formattedAddress ?? ""
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
