//
//  FilterVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 26/09/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class FilterVC: UIViewController {
    
    @IBOutlet var sliderDistance: UISlider!
    @IBOutlet var lblSelectedKm: UILabel!
    @IBOutlet var txtCategory: UITextField!
    @IBOutlet var txtRegion: UITextField!
    @IBOutlet var btnApply: UIButton!
    
    var strUserType = ""
    var arrSelectedCategory: NSMutableArray = NSMutableArray()
    var arrSelectedRegion: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if strUserType == SEEKER {
            sliderDistance.minimumTrackTintColor = REDCOLOR
            sliderDistance.thumbTintColor = REDCOLOR
            btnApply.backgroundColor = REDCOLOR
        }
        
        if let dictSetData = UserDefaults.standard.object(forKey: "filterdata") {
            let dict = dictSetData as! NSDictionary
            
            sliderDistance.value = Float(dict.object(forKey: "distance") as? String ?? "\(sliderDistance.value)")!
            lblSelectedKm.text = String(format: "%d km",Int(sliderDistance.value))
            
            if (dict.object(forKey: "region") != nil) {
                if let arrRegion = dict.object(forKey: "region") as? NSArray {
                    arrSelectedRegion = arrRegion.mutableCopy() as! NSMutableArray
                    self.txtRegion.text = arrSelectedRegion.componentsJoined(by: ",")
                }
            }
            
            if (dict.object(forKey: "categorys") != nil) {
                if let arrCategory = dict.object(forKey: "categorys") as? NSArray {
                    arrSelectedCategory = arrCategory.mutableCopy() as! NSMutableArray
                    let arrCats = arrSelectedCategory.value(forKeyPath: "category_name") as! NSArray
                    self.txtCategory.text = arrCats.componentsJoined(by: ",")
                }
            }
        }
    }
    
    //MARK:- UIBUTTON ACTION
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sliderDistance(_ sender: UISlider) {
        lblSelectedKm.text = String(format: "%d km",Int(sliderDistance.value))
    }
    
    @IBAction func btnSelectCategory(_ sender: Any) {
        openList(array: AppUtilities.sharedInstance.arrCatHint, arrSelected: arrSelectedCategory, type: "categoryforfilter")
    }
    
    @IBAction func btnSelectRegion(_ sender: Any) {
        let arrAddress: NSMutableArray = ["North", "South", "East", "West", "Central", "Northeast", "Northwest", "Southeast", "Southwest"]
        openList(array: arrAddress, arrSelected: arrSelectedRegion, type: "regionforfilter")
    }
    
    @IBAction func btnReset(_ sender: Any) {
        openResetAlert()
    }
    
    @IBAction func btnApply(_ sender: Any) {
        applyFilter(isReset: "0")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension FilterVC: pickerVCDelegate {
    
    func openList(array: NSMutableArray, arrSelected: NSMutableArray, type: String) {
        self.view.endEditing(true)
        let vc : PickerVC = PickerVC(nibName: "PickerVC", bundle: nil)
        vc.pickerVCDelegate = self
        vc.arrData = array
        vc.arrSelectedData = arrSelected
        vc.listType = type
        vc.strUserType = strUserType
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnSave(arrDetail: NSMutableArray, type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        
        print(arrDetail)
        
        if arrDetail.count <= 0 {
            if type == "categoryforfilter" {
                self.arrSelectedCategory.removeAllObjects()
                self.txtCategory.text = ""
            } else if type == "regionforfilter" {
                self.arrSelectedRegion.removeAllObjects()
                self.txtRegion.text = ""
            }
            return
        }
        
        if type == "categoryforfilter" {
            arrSelectedCategory = arrDetail
            let arrCats = arrSelectedCategory.value(forKeyPath: "category_name") as! NSArray
            self.txtCategory.text = arrCats.componentsJoined(by: ",")
        } else if type == "regionforfilter" {
            arrSelectedRegion = arrDetail
            self.txtRegion.text = arrSelectedRegion.componentsJoined(by: ",")
        }
    }
    
    func btnCancel(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
}

extension FilterVC: AlertDelegate {
    
    func openResetAlert() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = "Are you sure you want to reset filter?"
        vc.type = "simplealert"
        vc.isHideOk = true
        vc.strUserType = strUserType
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "simplealert" {
                self.clearFilter()
            }
        }
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        
    }
    
    func clearFilter() {
        arrSelectedCategory.removeAllObjects()
        txtCategory.text = ""
        arrSelectedRegion.removeAllObjects()
        txtRegion.text = ""
        sliderDistance.value = 100
        lblSelectedKm.text = String(format: "%d km",Int(sliderDistance.value))
        
        applyFilter(isReset: "1")
    }
    
    func applyFilter(isReset:String) {
        let dict: NSMutableDictionary = NSMutableDictionary()
        dict.setObject("\(sliderDistance.value)", forKey: "distance" as NSCopying)
        if arrSelectedRegion.count > 0 {
            dict.setObject(arrSelectedRegion, forKey: "region" as NSCopying)
        }
        if arrSelectedCategory.count > 0 {
            dict.setObject(arrSelectedCategory, forKey: "categorys" as NSCopying)
        }
        dict.setObject(isReset, forKey: "reset" as NSCopying)
        UserDefaults.standard.set(dict.copy() as! NSDictionary, forKey: "filterdata")
        UserDefaults.standard.synchronize()
        AppUtilities.sharedInstance.isCallFilterFinder = true
        AppUtilities.sharedInstance.isCallFilterSeeker = true
        NotificationCenter.default.post(name: Notification.Name("filterfinderdata"), object: nil, userInfo: dict.copy() as! NSDictionary as? [AnyHashable : Any])
        NotificationCenter.default.post(name: Notification.Name("filterseekerdata"), object: nil, userInfo: dict.copy() as! NSDictionary as? [AnyHashable : Any])
        self.dismiss(animated: true, completion: nil)
    }
}

