//
//  HomeVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 17/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    var pageMenu : CAPSPageMenu?
    
    var isAnswered: Bool = false
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var btnList: UIButton!
    @IBOutlet var btnNotification: UIButton!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet var lblUnreadCount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isAnswered {
            imgUser.isHidden = true
            btnList.isHidden = true
            btnFilter.isHidden = true
            btnNotification.isHidden = true
            btnProfile.setImage(UIImage(named:"ic_back"), for: .normal)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.changebackground), name: Notification.Name("changebackground"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUnreadCount), name: Notification.Name("updateUnreadCount"), object: nil)
        
        if AppUtilities.sharedInstance.isSubmitForm {
            AppUtilities.sharedInstance.isSubmitForm = false
            let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idAddFoundItemVC)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        setPageMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUnreadCount()
        if AppUtilities.sharedInstance.isLogin() == "1" {
            imgUser.sd_setImage(with: URL(string:UserDefaults.standard.object(forKey: UD_UserProfileURL) as! String), placeholderImage:#imageLiteral(resourceName: "ic_profile"))
            sendGetUnreadNoti()
        } else {
            imgUser.isHidden = true
            btnList.isHidden = true
            btnFilter.isHidden = true
            btnProfile.isHidden = true
            btnNotification.isHidden = true
        }
        
        imgBack.image = AppUtilities.sharedInstance.userType == SEEKER ? UIImage(named:"img_bg") : UIImage(named:"login_bg")
        if pageMenu?.currentPageIndex == 1 {
            imgBack.image = UIImage(named:"img_bg")
        } else {
            imgBack.image = UIImage(named:"login_bg")
        }
    }
    
    @IBAction func btnFilter(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idFilterVC) as! FilterVC
        vc.strUserType = pageMenu?.currentPageIndex == 0 ? FINDER : SEEKER
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- NotificationCenter Method
    @objc func changebackground(withNotification notification : NSNotification) {
        imgBack.image = AppUtilities.sharedInstance.userType == SEEKER ? UIImage(named:"img_bg") : UIImage(named:"login_bg")
    }
    
    @objc func updateUnreadCount(withNotification notification : NSNotification) {
        setUnreadCount()
    }

    func setUnreadCount() {
        lblUnreadCount.isHidden = true
        if noti_unread_count > 0 {
            lblUnreadCount.isHidden = false
            lblUnreadCount.text = "\(noti_unread_count)"
        }
    }
    
    //MARK:- UIBUTTON ACTION
    @IBAction func btnNotification(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idNotificationVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnList(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idHomeVC) as! HomeVC
        vc.isAnswered = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        if isAnswered {
            self.navigationController?.popViewController(animated: true)
        } else {
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idUserProfile)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("changebackground"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("updateUnreadCount"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension HomeVC: CAPSPageMenuDelegate {
    func setPageMenu() {
        var arrController : [UIViewController] = []
        
        var strFinderTitle = "FINDERS"
        if isAnswered {
            strFinderTitle = "Seeking Item Request"
        }
        let founders = loadVC(strStoryboardId: SB_MAIN, strVCId: idFoundersVC) as! FoundersVC
        founders.isAnswered = isAnswered
        founders.title = strFinderTitle
        arrController.append(founders)
        
        var strSeekerTitle = "SEEKERS"
        if isAnswered {
            strSeekerTitle = "Finding Item Request"
        }
        let losters = loadVC(strStoryboardId: SB_MAIN, strVCId: idLostersVC) as! LostersVC
        losters.isAnswered = isAnswered
        losters.title = strSeekerTitle
        arrController.append(losters)
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.clear),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(UIColor.white),
            .bottomMenuHairlineColor(UIColor.clear),
            .selectedMenuItemLabelColor(UIColor.white),
            .unselectedMenuItemLabelColor(UIColor.white),
            .selectionIndicatorHeight(3),
            .menuHeight(62),
            .menuMargin(0),
            .menuItemFont(UIFont(name: "Montserrat-Bold", size: 14)!),
            .menuItemWidth(self.view.frame.size.width/CGFloat(arrController.count)),
            .centerMenuItems(true)
        ]
        
        let frame = CGRect(x: 0, y: 62, width: self.view.frame.size.width, height: self.view.frame.size.height-62)
        
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu!.delegate = self
        pageMenu?.controllerScrollView.isScrollEnabled = false
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int){
        if index == 1 {
            imgBack.image = UIImage(named:"img_bg")
        } else {
            imgBack.image = UIImage(named:"login_bg")
        }
    }
}


extension HomeVC: whoBuddyDelegate {
    
    
    func whoBuddy() {
        let vc : WhoBuddyVC = WhoBuddyVC(nibName: idWhoBuddyVC, bundle: nil)
        vc.whoBuddyDelegate = self
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnDone() {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        if AppUtilities.sharedInstance.userType == SEEKER {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idAddFoundItemVC)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func btnCancelSelection() {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
}
