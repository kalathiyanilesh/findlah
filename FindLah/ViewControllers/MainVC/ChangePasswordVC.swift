//
//  ChangePasswordVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 31/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet var txtOldPwd: UITextField!
    @IBOutlet var txtNewPwd: UITextField!
    @IBOutlet var txtReTypePwd: UITextField!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var viewOldPass: UIView!
    @IBOutlet var heightOldPass: NSLayoutConstraint!
    var isSocialUser: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let dictUserInfo = UserDefaults.standard.object(forKey: UD_UserData) as! NSDictionary
        let socialID = dictUserInfo.object(forKey: "social_id") as! String
        let password = dictUserInfo.object(forKey: "password") as! String
        if socialID.count > 0 && password.count <= 0 {
            isSocialUser = true
            heightOldPass.constant = 0
            viewOldPass.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setThemeColor()
    }
    
    //MARK:- UIBUTTON ACTION
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit(_ sender: UIButton) {
        if isSocialUser {
            if (validateTxtFieldLength(txtNewPwd, withMessage: EnterNEWPassword)) &&
                (validateMinTxtFieldLength(txtNewPwd, withMessage: EnterMinLengthPassword)) &&
                (passwordMismatch(txtNewPwd, txtReTypePwd, withMessage: PasswordMismatch)) {
                sendChangePaswordReq()
            }
        } else {
            if (validateTxtFieldLength(txtOldPwd, withMessage: EnterOLDPassword)) &&
                (validateTxtFieldLength(txtNewPwd, withMessage: EnterNEWPassword)) &&
                (validateMinTxtFieldLength(txtNewPwd, withMessage: EnterMinLengthPassword)) &&
                (passwordMismatch(txtNewPwd, txtReTypePwd, withMessage: PasswordMismatch)) {
                sendChangePaswordReq()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ChangePasswordVC {
    func sendChangePaswordReq() {
        self.view.endEditing(true)
        let dictParam : NSDictionary = ["id":AppUtilities.sharedInstance.getUserID(),
                                        "old_password":txtOldPwd.text!,
                                        "new_password": txtNewPwd.text!,
                                        "confirm_password": txtReTypePwd.text!]
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIChangePassword,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                if responseDict?.object(forKey: kSuccess) as! String == "1" {
                    let dictUserInfo : NSDictionary = responseDict!["data"] as! NSDictionary
                    AppUtilities.sharedInstance.saveUserData(dictUserInfo: dictUserInfo, isUpdateUser: false)
                    //self.openAlert(message: responseDict?.object(forKey: kMessage) as! String)
                    self.openAlert(message: "Your Password has been changed successfully!")
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
}

extension ChangePasswordVC: AlertDelegate {
    
    func openAlert(message: String) {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = message
        vc.type = "simplealert"
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
}

extension ChangePasswordVC {
    func setThemeColor() {
        if AppUtilities.sharedInstance.userType == SEEKER {
            imgBack.image = UIImage(named:"img_bg")
            btnSubmit.setTitleColor(REDCOLOR, for: .normal)
        } else {
            imgBack.image = UIImage(named:"login_bg")
            btnSubmit.setTitleColor(BLUECOLOR, for: .normal)
        }
    }
}
