//
//  NotificationVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 18/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet var imgType: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
}

class NotificationVC: UIViewController {

    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var tblNotiication: UITableView!
    var arrNotificaction: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendGetNotificationReq()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imgBack.image = AppUtilities.sharedInstance.userType == SEEKER ? UIImage(named:"img_bg") : UIImage(named:"login_bg")
    }

    //MARK:- BUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension NotificationVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return setNoDataLabel(tableView: tblNotiication, array: arrNotificaction, text: "No Data Found.")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotificaction.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NotificationCell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.lblDescription.textColor = BLUECOLOR
        if AppUtilities.sharedInstance.userType == SEEKER {
            cell.lblDescription.textColor = REDCOLOR
        }
        
        let dictNoti = arrNotificaction.object(at: indexPath.row) as! NSDictionary
        cell.imgType.image = UIImage(named: "ic_rewardsn")
        cell.lblDescription.text = dictNoti.object(forKey: "message") as? String
        
        /*
        if indexPath.row == 0 {
            cell.imgType.image = UIImage(named: "ic_foundedn")
            cell.lblDescription.text = "Now you are able to view finders post"
        } else if indexPath.row == 1 {
            cell.imgType.image = UIImage(named: "ic_rewardsn")
            cell.lblDescription.text = "You get reward of $80."
        } else if indexPath.row == 2 {
            cell.imgType.image = UIImage(named: "ic_losters")
            cell.lblDescription.text = "Admin accept your request to post lost item. We you will get it soon."
        }*/
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictNotification: NSDictionary = arrNotificaction.object(at: indexPath.row) as! NSDictionary
        print(dictNotification)
        let strNotificationID = dictNotification.object(forKey: "notification_id") as? String ?? "0"
        sendReadNoti(strID: strNotificationID)
        
        let strReportID: String = dictNotification.object(forKey: "report_id") as? String ?? "0"
        if strReportID != "0" {
            sendGetReportReq(reportID: strReportID)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension NotificationVC {
    func sendGetNotificationReq() {
        let dictParam: NSDictionary = ["user_id":AppUtilities.sharedInstance.getUserID()]
        print("payment param ",dictParam)
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIGetNotification,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1" {
                    self.arrNotificaction = (responseDict?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                    self.tblNotiication.reloadData()
                } else {
                    //showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
    
    func sendGetReportReq(reportID: String) {
        let dictParam: NSDictionary = ["report_id":reportID]
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIGetReportByID,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error == nil
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    let dictReport: NSDictionary = dictionaryOfFilteredBy(dict: responseDict?.object(forKey: "data") as! NSDictionary)
                    let strReportType = dictReport.object(forKey: "report_type") as? String
                    let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idDetailsVC) as! DetailsVC
                    vc.dictReport = dictReport
                    if strReportType == FINDER {
                        vc.isFromFinder = true
                    }
                    vc.isFromNotification = true
                    vc.isAnswered = false
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
}
