//
//  RewardsVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 14/09/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

protocol delegateReward {
    func updateRewardAmount(amount: String)
}

class RewardsVC: UIViewController {

    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var txtAccountNo: UITextField!
    @IBOutlet var txtBankName: UITextField!
    @IBOutlet var btnReward: UIButton!
    @IBOutlet var imgBack: UIImageView!
    
    var delegateReward: delegateReward?
    var dictSelectedBank: NSDictionary = NSDictionary()
    var total_Balance: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppUtilities.sharedInstance.userType == SEEKER {
            imgBack.image = UIImage(named:"img_bg")
            btnReward.backgroundColor = REDCOLOR
        }
        txtAmount.text = total_Balance
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBankList(_ sender: Any) {
        let arrSelected: NSMutableArray = NSMutableArray()
        arrSelected.add(dictSelectedBank)
        openBankList(array: AppUtilities.sharedInstance.arrBankList, arrSelected: arrSelected, type: "banklist")
    }
    
    @IBAction func btnReward(_ sender: Any) {
        let strMaxReward = "Your maximum reward amount is \(total_Balance)"
        if validateTxtFieldLength(txtAmount, withMessage: EnterAmount) &&
            validateAmount(txtAmount, withMessage: EnterValidAmount) &&
            compareAmount(txtAmount, amount: total_Balance, withMessage: strMaxReward) &&
            validateTxtFieldLength(txtBankName, withMessage: EnterBankName) &&
            validateTxtFieldLength(txtAccountNo, withMessage: EnterAccountNo) {
            sendRewardReq()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension RewardsVC: pickerVCDelegate {
    func openBankList(array: NSMutableArray, arrSelected: NSMutableArray, type: String) {
        self.view.endEditing(true)
        let vc : PickerVC = PickerVC(nibName: "PickerVC", bundle: nil)
        vc.pickerVCDelegate = self
        vc.arrData = array
        vc.arrSelectedData = arrSelected
        vc.listType = type
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnSave(arrDetail: NSMutableArray, type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        print(arrDetail)
        dictSelectedBank = NSDictionary()
        if arrDetail.count > 0 {
            dictSelectedBank = arrDetail.object(at: 0) as! NSDictionary
            txtBankName.text = dictSelectedBank.object(forKey: "bank_name") as? String
        }
    }
    
    func btnCancel(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
}

extension RewardsVC {
    func sendRewardReq() {

        let dictParam: NSDictionary = ["user_id":AppUtilities.sharedInstance.getUserID(),
                                       "amount":txtAmount.text!,
                                       "account_no":txtAccountNo.text!,
                                       "bank_name":txtBankName.text!]
        
        print("payment param ",dictParam)
        
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIAddRewardReq,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1" {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                    self.delegateReward?.updateRewardAmount(amount: self.txtAmount.text!)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
}
