//
//  FoundedItemsVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 18/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class FoundedItemCell: UITableViewCell {
    @IBOutlet var lblItem: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblReward: UILabel!
    @IBOutlet var imgItem: UIImageView!
    @IBOutlet var btnMore: UIButton!
}

class FoundedItemsVC: UIViewController, delegateAddEditReport, delegateUpdateSubmiteStatus {
    
    func updateStatusForSubmit(dict: NSDictionary, index: Int) {
        AppUtilities.sharedInstance.isCallFinderList = true
        AppUtilities.sharedInstance.isCallSeekerList = true
        arrReport.replaceObject(at: index, with: dict)
        tblItems.reloadData()
    }
    
    func updateRecord(dictData: NSDictionary, index: Int, isEdit: Bool) {
        AppUtilities.sharedInstance.isCallFinderList = true
        AppUtilities.sharedInstance.isCallSeekerList = true
        if isEdit {
            arrReport.replaceObject(at: index, with: dictData)
        } else {
            arrReport.insert(dictData, at: 0)
        }
        tblItems.reloadData()
    }
    
    
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var tblItems: UITableView!
    @IBOutlet var lblTitle: UILabel!

    var indexDelete: Int = 0
    var type = ""
    var offset = 1
    var isSend = false
    var refreshControl = UIRefreshControl()
    var arrReport: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblItems.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(self.pulltorefresh), for: .valueChanged)
        showLoaderHUD(strMessage: "")
        pulltorefresh()
        
        tblItems.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.offset += 1
                self.sendGetReportReq()
            }else {
                self.tblItems.infiniteScrollingView.stopAnimating()
            }
        }))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imgBack.image = AppUtilities.sharedInstance.userType == SEEKER ? UIImage(named:"img_bg") : UIImage(named:"login_bg")
        lblTitle.text = type == SEEKER ? "LOST ITEMS" : "FOUND ITEMS"
    }
    
    //MARK:- Refresh Control Method
    @objc func pulltorefresh() {
        if self.isSend == false {
            self.offset = 1
            self.sendGetReportReq()
        }
    }

    //MARK:- BUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension FoundedItemsVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return AppUtilities.sharedInstance.setNoDataLabel(tableView: tableView, array: arrReport, text: "Report Not Found.")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReport.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : FoundedItemCell = tableView.dequeueReusableCell(withIdentifier: "FoundedItemCell", for: indexPath) as! FoundedItemCell
        cell.lblStatus.textColor = BLUECOLOR
        if AppUtilities.sharedInstance.userType == SEEKER {
            cell.lblStatus.textColor = REDCOLOR
        }
        let dictReport = arrReport.object(at: indexPath.row) as! NSDictionary
//        cell.imgItem.sd_setImage(with:URL(string:dictReport.object(forKey: "report_image_1") as! String), placeholderImage:nil, options:.continueInBackground)
//
        cell.imgItem.setImageWith(URL.init(string:dictReport.object(forKey: "report_image_1") as! String), placeholderImage: nil, options: .continueInBackground, completed: { (img, error, cachType, url) in
            if(img == nil)
            {
                cell.imgItem.image = nil
            }
            else
            {
                cell.imgItem.image = img
            }
        }, usingActivityIndicatorStyle: .gray)
        
        cell.lblItem.text = String(format: "Item : %@",dictReport.object(forKey: "title") as! String)
        let address1 = dictReport.object(forKey: "address_1") as! String
        let address2 = dictReport.object(forKey: "address_2") as! String
        let address3 = dictReport.object(forKey: "address_3") as! String
        let arrAdd = NSMutableArray()
        if address1.count > 0 { arrAdd.add(address1) }
        if address2.count > 0 { arrAdd.add(address2) }
        if address3.count > 0 { arrAdd.add(address3) }
        
        let strAddress = (arrAdd as NSArray).componentsJoined(by: " / ")
        cell.lblLocation.text = String(format: "Location : %@",strAddress)
        
        cell.btnMore.addTarget(self, action: #selector(btnMore(sender:)), for: .touchUpInside)
        
        /*
        cell.btnMore.isHidden = true
        cell.lblStatus.text = getOriginalUserReportStatus(dict: dictReport, needOnlyStatus: false)
        if cell.lblStatus.text?.lowercased() == "in progress" {
            cell.btnMore.isHidden = false
            cell.btnMore.addTarget(self, action: #selector(btnMore(sender:)), for: .touchUpInside)
        }*/
    
        cell.lblReward.text = "Reward : $0"
        if dictReport.object(forKey: "is_reward") as! String == "1" {
            cell.lblReward.text = String(format: "Reward : $%@",dictReport.object(forKey: "amount") as! String)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrReport.object(at: indexPath.row) as! NSDictionary
        let strReportType = dict.object(forKey: "report_type") as? String
        let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idDetailsVC) as! DetailsVC
        vc.dictReport = dict
        if strReportType == FINDER {
            vc.isFromFinder = true
            vc.delegateUpdateSubmiteStatus = self
            vc.index = indexPath.row
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func btnMore(sender:UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblItems)
        let indexPath = tblItems.indexPathForRow(at: buttonPosition)
        
        let dictReport = arrReport.object(at: indexPath!.row) as! NSDictionary
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Edit", style: .default)
        { _ in
            let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idAddFoundItemVC) as! AddFoundItemVC
            vc.isEdit = true
            vc.index = (indexPath?.row)!
            vc.dictReport = dictReport
            vc.delegateAddEditReport = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let deleteActionButton = UIAlertAction(title: "Delete", style: .default)
        { _ in
            self.indexDelete = indexPath!.row
            self.openDeleteAlert()
        }
        
        let shareActionButton = UIAlertAction(title: "Share", style: .default)
        { _ in
            let objShare = shareContent_DeepLinking(dicMess: dictReport)
            objShare.getShortUrl(with: get_BranchLinkProperties()) { (url, error) in
                if error == nil {
                    print(url ?? "not found")
            
                    let shareItems = [url!]
                    let activityViewController = UIActivityViewController(activityItems: shareItems , applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                }
                else {
                    print("got my Branch link : Error : %@", error?.localizedDescription as Any)
                    showMessage(error?.localizedDescription ?? "Something wrong")
                }
            }
        }
        
        let strStatus = getOriginalUserReportStatus(dict: dictReport, needOnlyStatus: false)
        if strStatus.lowercased() == "in progress" {
            actionSheetController.addAction(saveActionButton)
            actionSheetController.addAction(deleteActionButton)
            actionSheetController.addAction(shareActionButton)
        } else {
            actionSheetController.addAction(shareActionButton)
        }
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
}

extension FoundedItemsVC: AlertDelegate {
    func openDeleteAlert() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = "Delete"
        vc.strDescription = "Are you sure you want to delete this report?"
        vc.type = "askdelete"
        vc.isHideOk = true
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "askdelete" {
                AppUtilities.sharedInstance.isCallFinderList = true
                AppUtilities.sharedInstance.isCallSeekerList = true
                let dictReport = self.arrReport.object(at: self.indexDelete) as! NSDictionary
                AppUtilities.sharedInstance.sendDeleteReportReq(reportID: dictReport.object(forKey: "report_id") as! String)
                self.arrReport.removeObject(at: self.indexDelete)
                self.tblItems.reloadData()
            }
        }
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
}

extension FoundedItemsVC {
    func sendGetReportReq() {
        isSend = true
        let dictParam: NSDictionary = ["user_id":AppUtilities.sharedInstance.getUserID(),
                                       "report_type":type,
                                       "is_answered":"0",
                                       "search_text":"",
                                       "page":"\(offset)",
                                        "length":"\(LimitItem)"]
        
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIGetReport,
            parameters: dictParam,
            showLoader:false)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    if self.offset == 1 {
                        self.arrReport = arrayOfFilteredBy(arr: responseDict?.object(forKey: kData) as! NSArray).mutableCopy() as! NSMutableArray
                        if self.arrReport.count > 0{
                            self.tblItems.showsInfiniteScrolling = true
                        }
                    } else {
                        let arrOtherData = arrayOfFilteredBy(arr: responseDict?.object(forKey: kData) as! NSArray).mutableCopy() as! NSMutableArray
                        if arrOtherData.count < LimitItem {
                            self.tblItems.showsInfiniteScrolling = false
                        }
                        self.arrReport.addObjects(from: arrOtherData as! [Any])
                        self.tblItems.infiniteScrollingView.stopAnimating()
                    }
                    self.tblItems.reloadData()
                } else {
                    if self.offset != 1 {
                        self.tblItems.infiniteScrollingView.stopAnimating()
                    }
                }
                
                if self.arrReport.count <= 0 { self.tblItems.showsInfiniteScrolling = false }
                if self.offset == 1 { self.refreshControl.endRefreshing() }
                self.isSend = false
            }
        }
    }
}
