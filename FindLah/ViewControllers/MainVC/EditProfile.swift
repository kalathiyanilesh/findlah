//
//  EditProfile.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 05/08/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit
import GooglePlaces

class EditProfile: UIViewController, delegateEditMobileNo {
    func updateMobileNo() {
        sendEditProfileReq()
    }
    
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPhoneNo: UITextField!
    @IBOutlet var txtAddress: UITextField!
    @IBOutlet var txtUnitNo: UITextField!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var btnChangePass: UIButton!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var imgEmail: UIImageView!
    @IBOutlet var imgPhone: UIImageView!
    @IBOutlet var imgAddress: UIImageView!
    @IBOutlet var imgUnitNo: UIImageView!
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnCountry: UIButton!
    @IBOutlet var lblAddress: UILabel!
    
    var lat_address:Double = 0.0
    var long_address:Double = 0.0
    
    var isFromPaymentVC: Bool = false
    var isOtherProfile: Bool = false
    var strOtherUserID: String = ""
    var isImage: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isOtherProfile {
            txtAddress.isHidden = true
            btnChangePass.isHidden = true
            btnDone.isHidden = true
            lblTitle.text = APPNAME
            sendGetOtherUserReq()
        } else {
            lblAddress.isHidden = true
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            imgProfile.addGestureRecognizer(tapGestureRecognizer)
            
            let dictUserInfo = UserDefaults.standard.object(forKey: UD_UserData) as! NSDictionary
            let countryCode = TRIM(string: dictUserInfo.object(forKey: "country_code") as! String)
            if countryCode.count <= 0 {
                self.btnCountry.setTitle(setDefaultCountryCode(), for: .normal)
            } else {
                self.btnCountry.setTitle("+\(countryCode)", for: .normal)
            }
            
            lat_address = Double("\(dictUserInfo.object(forKey: "latitude") as! String)") ?? 0
            long_address = Double("\(dictUserInfo.object(forKey: "longitude") as! String)") ?? 0
            
            let strProfileImage = dictUserInfo.object(forKey: "profile_img") as! String
            if strProfileImage.count > 0 {
                isImage = true
            }
            imgProfile.sd_setImage(with: URL(string:UserDefaults.standard.object(forKey: UD_UserProfileURL) as! String), placeholderImage:#imageLiteral(resourceName: "male_p"))
            txtEmail.text = UserDefaults.standard.object(forKey: UD_UserEmail) as? String
            txtAddress.text = UserDefaults.standard.object(forKey: UD_UserAddresss) as? String
            txtPhoneNo.text = UserDefaults.standard.object(forKey: UD_UserMobileNo) as? String
            txtUserName.text = UserDefaults.standard.object(forKey: UD_UserName) as? String
            txtUnitNo.text = UserDefaults.standard.object(forKey: UD_UserUnitNo) as? String
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setThemeColor()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if isOtherProfile { return }
        openImagePicker(type: "")
    }

    //MARK:- BUTTON ACTION
    @IBAction func btnBack(_ sender: Any) {
        if isOtherProfile && isFromPaymentVC  {
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idSWRevealViewController)
            UIView.transition(with: self.navigationController!.view, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: { (finished) in
                self.navigationController?.pushViewController(vc, animated: false)
            })
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnCountry(_ sender: Any) {
        self.view.endEditing(true)
        if isOtherProfile { return }
        let alert = UIAlertController(style: .actionSheet, title: "Phone Codes")
        alert.addLocalePicker(type: .phoneCode) { info in
            self.btnCountry.setTitle(info?.phoneCode, for: .normal)
        }
        alert.addAction(title: "Cancel", style: .cancel)
        alert.show()
    }
    
    @IBAction func btnAddress(_ sender: Any) {
        if isOtherProfile { return }
        openGoogleAutoComplete()
    }
    
    @IBAction func btnChangePass(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idChangePasswordVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnDone(_ sender: Any) {
        if (validateTxtFieldLength(txtUserName, withMessage: EnterUserName) &&
            validateTxtFieldLength(txtAddress, withMessage: EnterAddress) &&
            validateTxtFieldLength(txtPhoneNo, withMessage: EnterPhone) &&
            validatePhoneNo(txtPhoneNo, withMessage: EnterValidPhone)) {
            
            let strMobileNo = UserDefaults.standard.object(forKey: UD_UserMobileNo) as? String
            let dictUserInfo = UserDefaults.standard.object(forKey: UD_UserData) as! NSDictionary
            let countryCode = TRIM(string: dictUserInfo.object(forKey: "country_code") as! String)
            
            if self.btnCountry.titleLabel?.text != "+\(countryCode)" || strMobileNo != txtPhoneNo.text {
                self.sendOTPReq()
            } else {
                sendEditProfileReq()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension EditProfile {
    func sendEditProfileReq() {
        self.view.endEditing(true)
        let dictParam : NSDictionary = ["id":AppUtilities.sharedInstance.getUserID(),
                                        "name":txtUserName.text!,
                                        "address":txtAddress.text!,
                                        "latitude":"\(lat_address)",
                                        "longitude":"\(long_address)",
                                        "country_code":btnCountry.titleLabel!.text!,
                                        "phone":txtPhoneNo.text!,
                                        "unitno":txtUnitNo.text!]
        
        let imageParam = isImage ? "profile_img" : ""
        
        HttpRequestManager.sharedInstance.requestWithPostMultipartParam(
            endpointurl: Server_URL,
            service:APIEditProfile,
            parameters: dictParam,
            image: imgProfile.image!,
            imageParam: imageParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict?.object(forKey: kSuccess) as! String == "1" {
                    let dictUserInfo : NSDictionary = responseDict!["user_info"] as! NSDictionary
                    AppUtilities.sharedInstance.saveUserData(dictUserInfo: dictUserInfo, isUpdateUser: false)
                    self.navigationController?.popViewController(animated: true)
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
    
    func sendGetOtherUserReq() {
        let dictParam: NSDictionary = ["user_id":strOtherUserID]
        
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIGetOtherUserProfile,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    let dictUserInfo = responseDict?["data"] as! NSDictionary
                    let countryCode = TRIM(string: dictUserInfo.object(forKey: "country_code") as! String)
                    if countryCode.count <= 0 {
                        self.btnCountry.setTitle(setDefaultCountryCode(), for: .normal)
                    } else {
                        self.btnCountry.setTitle("+\(countryCode)", for: .normal)
                    }
   
                    self.imgProfile.sd_setImage(with: URL(string:dictUserInfo.object(forKey: "profile_img") as! String), placeholderImage:#imageLiteral(resourceName: "male_p"))
                    self.txtEmail.text = dictUserInfo.object(forKey: "email") as? String
                    self.lblAddress.text = dictUserInfo.object(forKey: "address") as? String
                    self.txtPhoneNo.text = dictUserInfo.object(forKey: "phone") as? String
                    self.txtUserName.text = dictUserInfo.object(forKey: "name") as? String
                    self.txtUnitNo.text = dictUserInfo.object(forKey: "unitno") as? String
                    
                    if TRIM(string: self.lblAddress.text!).count <= 0 {
                        self.lblAddress.text = "Address"
                    }

                    self.txtEmail.isEnabled = false
                    self.txtAddress.isEnabled = false
                    self.txtPhoneNo.isEnabled = false
                    self.txtUserName.isEnabled = false
                    self.txtUnitNo.isEnabled = false
                    
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
    
    func sendOTPReq(){
        self.view.endEditing(true)
        let countryCode = btnCountry.titleLabel!.text!
        let dictParam: NSDictionary = ["id": AppUtilities.sharedInstance.getUserID(),
                                       "email": "",
                                       "phone": txtPhoneNo.text!,
                                       "country_code": countryCode]
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APISendOTP,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                
                if responseDict?.object(forKey: kSuccess) as! String == "1" {
                    let vc = loadVC(strStoryboardId: SB_SISU, strVCId: idAddOTPVC) as! AddOTPVC
                    vc.isMobileVerify = true
                    vc.isForEditPhoneNo = true
                    vc.strPhoneNo = self.txtPhoneNo.text!
                    vc.strCountryCode = (self.btnCountry.titleLabel?.text!)!
                    vc.isAutoFill = true
                    vc.delegateEditMobileNo = self
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
}

extension EditProfile: GMSAutocompleteViewControllerDelegate {
    
    func openGoogleAutoComplete() {
        self.view.endEditing(true)
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place address: \(place.coordinate.latitude)")
        print("Place attributions: \(place.coordinate.longitude)")
        
        lat_address = place.coordinate.latitude
        long_address = place.coordinate.longitude
        txtAddress.text = place.formattedAddress ?? ""
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension EditProfile: ImagePickerDelegate{
    
    func openImagePicker(type: String) {
        self.view.endEditing(true)
        ImagePicker.sharedInstance.delegate = self
        ImagePicker.sharedInstance.selectImage(sender: type)
    }
    
    func pickImageComplete(_ imageData: UIImage, sender: String)
    {
        isImage = true
        imgProfile.image = imageData
    }
}

extension EditProfile {
    func setThemeColor() {
        if AppUtilities.sharedInstance.userType == SEEKER {
            btnDone.backgroundColor = REDCOLOR
            btnChangePass.backgroundColor = REDCOLOR
            imgBack.image = UIImage(named:"img_bg")
            imgUser.image = UIImage(named:"ic_usernamered_4x")
            imgEmail.image = UIImage(named:"ic_emailred")
            imgPhone.image = UIImage(named:"ic_phonered")
            imgAddress.image = UIImage(named:"ic_locationred")
            imgUnitNo.image = UIImage(named:"ic_locationred")
        } else {
            btnDone.backgroundColor = BLUECOLOR
            btnChangePass.backgroundColor = BLUECOLOR
            imgBack.image = UIImage(named:"ic_background")
            imgUser.image = UIImage(named:"ic_usernameblue_4x")
            imgEmail.image = UIImage(named:"ic_email_blue")
            imgPhone.image = UIImage(named:"ic_phone")
            imgAddress.image = UIImage(named:"ic_location")
            imgUnitNo.image = UIImage(named:"ic_location")
        }
    }
}

