//
//  SignUpVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 15/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit
import GooglePlaces

class SignUpVC: UIViewController {
    
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPhoneNo: UITextField!
    @IBOutlet var txtAddress: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtConfirmPass: UITextField!
    @IBOutlet var imgUserPic: UIImageView!
    @IBOutlet var btnCountryCode: UIButton!
    @IBOutlet var btnSeeker: UIButton!
    @IBOutlet var btnFinder: UIButton!
    @IBOutlet var txtUnitNo: UITextField!
    
    var isImage: Bool = false
    var lat_address:Double = 0.0
    var long_address:Double = 0.0
    var user:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgUserPic.addGestureRecognizer(tapGestureRecognizer)
        
        if AppUtilities.sharedInstance.userType == SEEKER {
            user = "1"
            btnSeeker.isSelected = true
        } else {
            user = "2"
            btnFinder.isSelected = true
        }
        
        btnCountryCode.setTitle(setDefaultCountryCode(), for: .normal)
    }

    override func viewWillAppear(_ animated: Bool) {
        setThemeColor()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        openImagePicker(type: "")
    }

    //MARK:- Button Action
    @IBAction func btnSignIn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSeeker(_ sender: Any) {
        user = "1"
        btnSeeker.isSelected = true
        btnFinder.isSelected = false
       // btnJustBrows.isSelected = false
        AppUtilities.sharedInstance.userType = SEEKER
        AppUtilities.sharedInstance.saveData(data: AppUtilities.sharedInstance.userType as AnyObject, key: kUserType)
        setThemeColor()
    }
    
    @IBAction func btnFinder(_ sender: Any) {
        user = "2"
        btnFinder.isSelected = true
        btnSeeker.isSelected = false
        AppUtilities.sharedInstance.userType = FINDER
        AppUtilities.sharedInstance.saveData(data: AppUtilities.sharedInstance.userType as AnyObject, key: kUserType)
        setThemeColor()
    }

    @IBAction func btnCountryCode(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(style: .actionSheet, title: "Phone Codes")
        alert.addLocalePicker(type: .phoneCode) { info in
            self.btnCountryCode.setTitle(info?.phoneCode, for: .normal)
        }
        alert.addAction(title: "Cancel", style: .cancel)
        alert.show()
    }
    
    @IBAction func btnAddress(_ sender: Any) {
        openGoogleAutoComplete()
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        if (validateTxtFieldLength(txtName, withMessage: EnterUserName) &&
            validateTxtFieldLength(txtEmail, withMessage: EnterEmail) &&
            validateEmailAddress(txtEmail, withMessage: EnterValidEmail) &&
            validateTxtFieldLength(txtPhoneNo, withMessage: EnterPhone) &&
            validatePhoneNo(txtPhoneNo, withMessage: EnterValidPhone) &&
            validateTxtFieldLength(txtAddress, withMessage: EnterAddress) &&
            validateTxtFieldLength(txtPassword, withMessage: EnterPassword) &&
            validateMinTxtFieldLength(txtPassword, withMessage: EnterMinLengthPassword) &&
            passwordMismatch(txtPassword, txtConfirmPass, withMessage: PasswordMismatch)) {
            sendSignUpReq()
        }
    }
    
    func gotoVerifyScreen(isMobileNo: Bool) {
        let vc = loadVC(strStoryboardId: SB_SISU, strVCId: idVarifyEmailPhoneVC) as! VarifyEmailPhoneVC
        vc.isMobileVarify = isMobileNo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension SignUpVC: ImagePickerDelegate{
    
    func openImagePicker(type: String) {
        self.view.endEditing(true)
        ImagePicker.sharedInstance.delegate = self
        ImagePicker.sharedInstance.selectImage(sender: type)
    }
    
    func pickImageComplete(_ imageData: UIImage, sender: String)
    {
        isImage = true
        imgUserPic.image = imageData
    }
}

extension SignUpVC {
    func sendSignUpReq() {
        self.view.endEditing(true)
        let dictParam : NSDictionary = ["login_type":"0",
                                       "user_type":user,
                                       "name":txtName.text!,
                                       "email":txtEmail.text!,
                                       "country_code":btnCountryCode.titleLabel!.text!,
                                       "phone":txtPhoneNo.text!,
                                       "address":txtAddress.text!,
                                       "latitude":"\(lat_address)",
                                       "longitude":"\(long_address)",
                                       "unitno":txtUnitNo.text!,
                                       "password":txtPassword.text!,
                                       "confirm_password":txtPassword.text!,
                                       "social_id":"",
                                       "device_type": PlatformName,
                                       "device_token": AppUtilities.sharedInstance.getDeviceToken()]
        
        let imageParam = isImage ? "profile_img" : ""
        
        print(dictParam)
        
        HttpRequestManager.sharedInstance.requestWithPostMultipartParam(
            endpointurl: Server_URL,
            service:APISignUp,
            parameters: dictParam,
            image: imgUserPic.image!,
            imageParam: imageParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    let dictUserInfo : NSDictionary = responseDict!["user_info"] as! NSDictionary
                    AppUtilities.sharedInstance.saveUserData(dictUserInfo: dictUserInfo, isUpdateUser: true)
                    self.setThemeColor()
                    self.askForVerify()
                }
                else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
    
    func askForVerify() {
        if AppUtilities.sharedInstance.userType == SEEKER {
            let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
            vc.AlertDelegate = self
            vc.strTitle = APPNAME
            vc.strDescription = "Did you lose your phone?"
            vc.isHideOk = true
            vc.type = "askforlostphone"
            self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
        } else {
            self.gotoVerifyScreen(isMobileNo: true)
        }
    }
}

extension SignUpVC: displayAlertDelegate, AlertDelegate {
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.gotoVerifyScreen(isMobileNo: false)
        }
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.gotoVerifyScreen(isMobileNo: true)
        }
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
}

extension SignUpVC {
    
    func setThemeColor() {
        setCursorColor()
        if AppUtilities.sharedInstance.userType == SEEKER {
            imgBack.image = UIImage(named:"img_bg")
            btnRegister.setTitleColor(REDCOLOR, for: .normal)
        } else {
            imgBack.image = UIImage(named:"login_bg")
            btnRegister.setTitleColor(BLUECOLOR, for: .normal)
        }
    }
}

extension SignUpVC: GMSAutocompleteViewControllerDelegate {
    
    func openGoogleAutoComplete() {
        self.view.endEditing(true)
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place address: \(place.coordinate.latitude)")
        print("Place attributions: \(place.coordinate.longitude)")
        
        lat_address = place.coordinate.latitude
        long_address = place.coordinate.longitude
        txtAddress.text = place.formattedAddress ?? ""
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
