//
//  VarifyEmailPhoneVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 16/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class VarifyEmailPhoneVC: UIViewController {

    @IBOutlet var viewEmailVarify: UIView!
    @IBOutlet var viewMobileVarify: UIView!
    @IBOutlet var btnVarify: UIButton!
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var txtPhoneNo: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var btnCountryCode: UIButton!
    
    var isMobileVarify:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewEmailVarify.isHidden = true
        viewMobileVarify.isHidden = true
        
        let dictUserInfo = UserDefaults.standard.object(forKey: UD_UserData) as! NSDictionary
        
        if isMobileVarify {
            let countryCode = dictUserInfo.object(forKey: "country_code") as! String
            if countryCode.count > 0 {
                btnCountryCode.setTitle("+" + countryCode, for: .normal)
            } else {
                btnCountryCode.setTitle(setDefaultCountryCode(), for: .normal)
            }
            txtPhoneNo.text = UserDefaults.standard.object(forKey: UD_UserMobileNo) as? String
            viewMobileVarify.isHidden = false
            btnVarify.setTitle("VERIFY MOBILE NUMBER", for: .normal)
        } else {
            txtEmail.text = UserDefaults.standard.object(forKey: UD_UserEmail) as? String
            viewEmailVarify.isHidden = false
            btnVarify.setTitle("VERIFY EMAIL ADDRESS", for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setThemeColor()
    }

    //MARK:- UIBUTTON ACTION
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCountryCode(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(style: .actionSheet, title: "Phone Codes")
        alert.addLocalePicker(type: .phoneCode) { info in
            self.btnCountryCode.setTitle(info?.phoneCode, for: .normal)
        }
        alert.addAction(title: "Cancel", style: .cancel)
        alert.show()
    }
    
    @IBAction func btnVarify(_ sender: Any) {
        self.view.endEditing(true)
        if isMobileVarify {
            let countryCode = btnCountryCode.titleLabel!.text!
            if countryCode == "+65" {
                if (validateTxtFieldLength(txtPhoneNo, withMessage: EnterPhone) &&
                    validatePhoneNo(txtPhoneNo, withMessage: EnterValidPhone)) {
                    self.sendOTPReq()
                }
            } else {
                let vc = loadVC(strStoryboardId: SB_SISU, strVCId: idAddOTPVC) as! AddOTPVC
                vc.isMobileVerify = self.isMobileVarify
                vc.strPhoneNo = self.txtPhoneNo.text!
                vc.strCountryCode = self.btnCountryCode.titleLabel!.text!
                vc.isAutoFill = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if (validateTxtFieldLength(txtEmail, withMessage: EnterEmail) &&
                validateEmailAddress(txtEmail, withMessage: EnterValidEmail)) {
                self.sendOTPReq()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension VarifyEmailPhoneVC {
    
    func sendOTPReq(){
        self.view.endEditing(true)
        var countryCode = ""
        if isMobileVarify {
            countryCode = btnCountryCode.titleLabel!.text!
        }
        let dictParam: NSDictionary = ["id": AppUtilities.sharedInstance.getUserID(),
                                       "email": txtEmail.text!,
                                       "phone": txtPhoneNo.text!,
                                       "country_code": countryCode]
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APISendOTP,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                
                if responseDict?.object(forKey: kSuccess) as! String == "1" {
                    self.openAlert(message: responseDict?.object(forKey: kMessage) as! String)
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
}

extension VarifyEmailPhoneVC: displayAlertDelegate {
    
    func openAlert(message: String) {
        let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
        vc.displayAlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = message
        vc.type = "simplealert"
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let vc = loadVC(strStoryboardId: SB_SISU, strVCId: idAddOTPVC) as! AddOTPVC
            vc.isMobileVerify = self.isMobileVarify
            if self.isMobileVarify {
                vc.strPhoneNo = self.txtPhoneNo.text!
                vc.strCountryCode = self.btnCountryCode.titleLabel!.text!
            } else {
                vc.strEmail = self.txtEmail.text!
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func btnYes(type: String) {
        
    }
    
    func btnNo(type: String) {
        
    }
}

extension VarifyEmailPhoneVC {
    func setThemeColor() {
        if AppUtilities.sharedInstance.userType == SEEKER {
            imgBack.image = UIImage(named:"img_bg")
            btnVarify.setTitleColor(REDCOLOR, for: .normal)
        } else {
            imgBack.image = UIImage(named:"login_bg")
            btnVarify.setTitleColor(BLUECOLOR, for: .normal)
        }
    }
}
