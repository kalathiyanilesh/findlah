//
//  SignInVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 15/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class SignInVC: UIViewController, whoBuddyDelegate {
    
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnFB: UIButton!
    @IBOutlet var btnGoogle: UIButton!
    @IBOutlet var imgGooglePlus: UIImageView!
    @IBOutlet var imgFB: UIImageView!
    @IBOutlet var btnTerms: UIButton!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!

    var dictUserInfo: NSDictionary = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //txtEmail.text = "ankitkathiriya007@gmail.com"
        //txtPassword.text = "123456"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setThemeColor()
    }
    
    //MARK:- whoBuddyDelegate METHOD
    func btnDone() {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.setThemeColor()
        }
    }
    
    func btnCancelSelection() {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }

    //MARK:- Button Action
    @IBAction func btnSignUp(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_SISU, strVCId: idSignUpVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnJustBrow(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idSWRevealViewController)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnForgotPass(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_SISU, strVCId: idForgotPassVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnTerms(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnOpenTC(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_SISU, strVCId: idTermsConditionVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        if(validateTxtFieldLength(txtEmail, withMessage: EnterEmail) &&
            validateTxtFieldLength(txtPassword, withMessage: EnterPassword) && validateMinTxtFieldLength(txtPassword, withMessage: EnterMinLengthPassword) &&
            validateTerms(btnTerms.isSelected, withMessage: EnterTerms)) {
            sendLoginReq()
        }
    }
    
    func login(fromLogin:Bool)
    {
        self.setThemeColor()
        if AppUtilities.sharedInstance.userType == SEEKER
        {
            if dictUserInfo.object(forKey: "is_email_verified") as! String == "0" && dictUserInfo.object(forKey: "is_phone_verified") as! String == "0"
            {
                let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
                vc.AlertDelegate = self
                vc.strTitle = APPNAME
                vc.strDescription = "Did you lose your phone?"
                vc.isHideOk = true
                vc.type = "askforlostphone"
                self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
            }
            else if dictUserInfo.object(forKey: "is_id_submited") as! String == "0"
            {
                askForLoseID()
            }
            else
            {
                gotoHome()
            }
        }
        else
        {
            if dictUserInfo.object(forKey: "is_email_verified") as! String == "0" && dictUserInfo.object(forKey: "is_phone_verified") as! String == "0" {
                self.gotoVerifyScreen(isMobileNo: true)
            } else {
                gotoHome()
            }
        }
    }
    
    func askForLoseID() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = "You’re almost ready."
        vc.strDescription = "Did you lose your ID or NRIC?"
        vc.isHideOk = true
        vc.type = "askforlostid"
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func gotoHome() {
        AppUtilities.sharedInstance.saveData(data: "1" as AnyObject, key: UD_IsLoggedIn)
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idSWRevealViewController)
        self.navigationController?.pushViewController(vc, animated: true)
        
        /*
        if(AppUtilities.sharedInstance.isSubmitForm == true)
        {
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
            self.navigationController?.pushViewController(vc, animated: true)
        }*/
    }
    
    @IBAction func btnGoogle(_ sender: Any) {
        googleLogin()
    }

    @IBAction func btnFacebook(_ sender: Any) {
        facebookLogin()
    }
    
    //MARK:- FACEBOOK LOGIN
    func facebookLogin() {
        if(FBSDKAccessToken.current() == nil){
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
                if (error == nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    
                    if (result?.isCancelled)!{
                        hideLoaderHUD()
                        return
                    }
                    if error != nil{
                        hideLoaderHUD()
                        print(error?.localizedDescription ?? "")
                        return
                    }
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                    }
                }
            }
        }else{
            showLoaderHUD(strMessage: "Loading..")
            self.getFBUserData()
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                hideLoaderHUD()
                if (error == nil){
                    print(result ?? "Not Found")
                    let dictResult = result as! NSDictionary
                    
                    let dictData = NSMutableDictionary()
                    if let email = dictResult.object(forKey: "email") as? String {
                        dictData.setObject(email, forKey: "email" as NSCopying)
                    } else {
                        dictData.setObject("", forKey: "email" as NSCopying)
                    }
                    if let name = dictResult.object(forKey: "name") as? String {
                        dictData.setObject(name, forKey: "name" as NSCopying)
                    } else {
                        dictData.setObject("", forKey: "name" as NSCopying)
                    }
                    if let fbid = dictResult.object(forKey: "id") as? String {
                        dictData.setObject(fbid, forKey: "social_id" as NSCopying)
                    } else {
                        dictData.setObject("", forKey: "social_id" as NSCopying)
                    }
                    
                    dictData.setObject("1", forKey: "login_type" as NSCopying)
         
                    if let dicPic = dictResult.object(forKey: "picture") as? NSDictionary {
                        let dict = dicPic.object(forKey: "data") as! NSDictionary
                        let userImageURL = dict.object(forKey: "url") as! String
                        self.sendSocialLoginReq(dictData: dictData, image:userImageURL)
                        /*
                        if let url = NSURL(string: userImageURL) {
                            self.sendSocialLoginReq(dictData: dictData, image:userImageURL)
                        }*/
                    }
                    else {
                        self.sendSocialLoginReq(dictData: dictData, image:"")
                    }
                    
                }
            })
        } else {
            hideLoaderHUD()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SignInVC: displayAlertDelegate, AlertDelegate {
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "askforlostid" {
                let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
                vc.displayAlertDelegate = self
                vc.strTitle = kIMPNOTICE
                vc.strDescription = kLATESTPHONEBILL
                vc.type = kBILLINGBILL
                self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
            } else if type == "askforlostphone" {
                self.gotoVerifyScreen(isMobileNo: false)
            }
        }
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "askforlostid" {
                let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
                vc.displayAlertDelegate = self
                vc.strTitle = kIMPNOTICE
                vc.strDescription = kPHOTOIDNRIC
                vc.type = kHIMID
                self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
            } else if type == "askforlostphone" {
                self.gotoVerifyScreen(isMobileNo: true)
            }
        }
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == kBILLINGBILL || type == kHIMID {
                self.openImagePicker(type: type)
            } else {
                self.openImagePicker(type: "uploadidproof")
            }
        }
    }
    
    func gotoVerifyScreen(isMobileNo: Bool) {
        let vc = loadVC(strStoryboardId: SB_SISU, strVCId: idVarifyEmailPhoneVC) as! VarifyEmailPhoneVC
        vc.isMobileVarify = isMobileNo
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SignInVC: ImagePickerDelegate{
    
    func openImagePicker(type: String) {
        self.view.endEditing(true)
        ImagePicker.sharedInstance.delegate = self
        ImagePicker.sharedInstance.selectImage(sender: type)
    }
    
    func pickImageComplete(_ imageData: UIImage, sender: String)
    {
        if sender == kBILLINGBILL || sender == kHIMID || sender == "uploadidproof" {
            let strIDProof = sender == kHIMID ? "1" : "0"
            sendUploadIdProof(image: imageData, strIsIDProof: strIDProof, responseData: {(error,responseDict) -> Void in
                hideMessage()
                if error != nil {
                    return
                } else {
                    print(responseDict ?? "Not Found")
                    if responseDict!["success"] as? String == "1"
                    {
                        //AppUtilities.sharedInstance.isSubmitForm = true
                        self.gotoHome()
                    }
                    else {
                        showMessage(responseDict?.object(forKey: kMessage) as! String)
                    }
                }
            })
        }
    }
}

extension SignInVC {
    
    func sendLoginReq() {
        self.view.endEditing(true)
        let dictParam: NSDictionary = ["email":txtEmail.text!,
                                      "password":txtPassword.text!,
                                      "device_type":PlatformName,
                                      "device_token":AppUtilities.sharedInstance.getDeviceToken()]
        
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APILogin,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    self.dictUserInfo = responseDict!["user_info"] as! NSDictionary
                    AppUtilities.sharedInstance.saveUserData(dictUserInfo: self.dictUserInfo, isUpdateUser: true)
                    self.login(fromLogin: true)
                }
                else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
    
    func sendSocialLoginReq(dictData: NSMutableDictionary, image: String){
        self.view.endEditing(true)
        
        print(image)
        var userType = "0"
        
        if AppUtilities.sharedInstance.userType == SEEKER {
            userType = "1"
        } else if AppUtilities.sharedInstance.userType == FINDER {
            userType = "2"
        }
        dictData.setObject("", forKey: "phone" as NSCopying)
        dictData.setObject("", forKey: "country_code" as NSCopying)
        dictData.setObject("", forKey: "password" as NSCopying)
        dictData.setObject("", forKey: "address" as NSCopying)
        dictData.setObject("", forKey: "unitno" as NSCopying)
        dictData.setObject("", forKey: "latitude" as NSCopying)
        dictData.setObject("", forKey: "longitude" as NSCopying)
        dictData.setObject("", forKey: "profile_img" as NSCopying)
        dictData.setObject("", forKey: "password" as NSCopying)
        dictData.setObject("", forKey: "confirm_password" as NSCopying)
        dictData.setObject(userType, forKey: "user_type" as NSCopying)
        dictData.setObject(PlatformName, forKey: "device_type" as NSCopying)
        dictData.setObject(AppUtilities.sharedInstance.getDeviceToken(), forKey: "device_token" as NSCopying)
        
        dictData.setObject(image, forKey: "profile_img" as NSCopying)
        
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APISignUp,
            parameters: dictData,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    if let dict = responseDict!["user_info"] as? NSDictionary {
                        self.dictUserInfo = dict
                    } else {
                        self.dictUserInfo = responseDict!["data"] as! NSDictionary
                    }
                    
                    AppUtilities.sharedInstance.saveUserData(dictUserInfo: self.dictUserInfo, isUpdateUser: true)
                    self.login(fromLogin: false)
                }
                else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
}

extension SignInVC: GIDSignInDelegate, GIDSignInUIDelegate {
    
    func googleLogin() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
    }
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        hideLoaderHUD()
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            let dictData = NSMutableDictionary()
            dictData.setObject(user.profile.email, forKey: "email" as NSCopying)
            dictData.setObject(user.profile.name, forKey: "name" as NSCopying)
            dictData.setObject(user.userID, forKey: "social_id" as NSCopying)
            dictData.setObject("2", forKey: "login_type" as NSCopying)
            
            var imageurl: NSURL = NSURL()
            if user.profile.hasImage {
                imageurl = user.profile.imageURL(withDimension: 200)! as NSURL
                /*
                if let data = NSData(contentsOf: (imageURL as? URL)!){
                    googleimage = UIImage(data: data as Data)!
                }*/
            }
            
            self.sendSocialLoginReq(dictData: dictData, image: imageurl.absoluteString!)
        }
    }
}

extension SignInVC {
    func setThemeColor() {
        if AppUtilities.sharedInstance.userType == SEEKER {
            imgBack.image = UIImage(named:"img_bg")
            btnLogin.setTitleColor(REDCOLOR, for: .normal)
            btnFB.setTitleColor(REDCOLOR, for: .normal)
            btnGoogle.setTitleColor(REDCOLOR, for: .normal)
            imgGooglePlus.image = UIImage(named:"ic_googleplusred")
            imgFB.image = UIImage(named:"ic_facebookred")
        } else {
            imgBack.image = UIImage(named:"login_bg")
            btnLogin.setTitleColor(BLUECOLOR, for: .normal)
            btnFB.setTitleColor(BLUECOLOR, for: .normal)
            btnGoogle.setTitleColor(BLUECOLOR, for: .normal)
            imgGooglePlus.image = UIImage(named:"ic_googleplus")
            imgFB.image = UIImage(named:"ic_facebook")
        }
    }
}
