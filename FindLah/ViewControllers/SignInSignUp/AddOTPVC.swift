//
//  AddOTPVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 18/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}

class MyTextField: UITextField {
    var myDelegate: MyTextFieldDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
}

protocol delegateEditMobileNo {
    func updateMobileNo()
}

class AddOTPVC: UIViewController, MyTextFieldDelegate {

    @IBOutlet var first: MyTextField!
    @IBOutlet var second: MyTextField!
    @IBOutlet var third: MyTextField!
    @IBOutlet var fourth: MyTextField!
    var currentTextFiled:MyTextField! = nil
    
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var lblCodeExpire: UILabel!
    @IBOutlet var btnResend: UIButton!
    @IBOutlet var btnResendBottom: UIButton!
    
    var delegateEditMobileNo: delegateEditMobileNo?
    
    var timer = Timer()
    var seconds = 0
    
    var isMobileVerify: Bool = false
    var strPhoneNo: String = ""
    var strCountryCode: String = ""
    var strEmail: String = ""
    var strOTP: String = ""
    var isAutoFill: Bool = false
    var isForEditPhoneNo: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTextField()
        if isAutoFill {
            if let otp = UserDefaults.standard.object(forKey: kUserOTP) as? String {
                strOTP = otp
                if strOTP.count == 4 {
                    first.text = String(describing: strOTP.character(at: 0)!)
                    second.text = String(describing: strOTP.character(at: 1)!)
                    third.text = String(describing: strOTP.character(at: 2)!)
                    fourth.text = String(describing: strOTP.character(at: 3)!)
                }
            }
        }
        
        startTimer()
    }

    override func viewWillAppear(_ animated: Bool) {
        setThemeColor()
    }

    //MARK:- BUTTON ACTIONS
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnResend(_ sender: Any) {
        sendOTPReq()
    }
    
    @IBAction func btnDone(_ sender: Any) {
        
        if TRIM(string: first.text!).count == 0 { return }
        if TRIM(string: second.text!).count == 0 { return }
        if TRIM(string: third.text!).count == 0 { return }
        if TRIM(string: fourth.text!).count == 0 { return }
        
        sendVerifyUserReq()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension AddOTPVC {
    func startTimer() {
        seconds = 0
        btnResend.isEnabled = false
        btnResendBottom.isEnabled = false
        timer = Timer()
        timer = Timer.scheduledTimer(timeInterval:1,  target: self, selector: #selector(self.calculateSeconds), userInfo: nil, repeats: true)
    }
    
    @objc func calculateSeconds() {
        seconds += 1
        if seconds > 60 {
            stopTimer()
            lblCodeExpire.text = "Code Expire in : 00:00"
        } else {
            btnDone.isEnabled = true
            lblCodeExpire.text = String(format:"Code Expire in : 00:%02d",60-seconds)
        }
    }
    
    func stopTimer() {
        self.timer.invalidate()
        self.seconds = 0
        self.btnResend.isEnabled = true
        self.btnResendBottom.isEnabled = true
        self.btnDone.isEnabled = false
    }
}

extension AddOTPVC: displayAlertDelegate, AlertDelegate {
    func btnYes(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "askforlostid" {
                let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
                vc.displayAlertDelegate = self
                vc.strTitle = kIMPNOTICE
                vc.strDescription = kLATESTPHONEBILL
                vc.type = kBILLINGBILL
                self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
            } else if type == "proofreject" {
                self.askForLoseID()
            }
        }
    }
    
    func btnNo(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "askforlostid" {
                let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
                vc.displayAlertDelegate = self
                vc.strTitle = kIMPNOTICE
                vc.strDescription = kPHOTOIDNRIC
                vc.type = kHIMID
                self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
            }
        }
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.openImagePicker(type: type)
        }
    }
}

extension AddOTPVC: ImagePickerDelegate{
    
    func openImagePicker(type: String) {
        self.view.endEditing(true)
        ImagePicker.sharedInstance.delegate = self
        ImagePicker.sharedInstance.selectImage(sender: type)
    }
    
    func pickImageComplete(_ imageData: UIImage, sender: String)
    {
        if sender == kBILLINGBILL || sender == kHIMID {
            let strIDProof = sender == kHIMID ? "1" : "0"
            sendUploadIdProof(image: imageData, strIsIDProof: strIDProof, responseData: {(error,responseDict) -> Void in
                hideMessage()
                if error != nil {
                    return
                } else {
                    print(responseDict ?? "Not Found")
                    if responseDict!["success"] as? String == "1"
                    {
                        //AppUtilities.sharedInstance.isSubmitForm = true
                        self.gotoHome()
                    }
                    else {
                        showMessage(responseDict?.object(forKey: kMessage) as! String)
                    }
                }
            })
        }
    }
}

extension AddOTPVC {
    func setUpTextField() {
        
        if isMobileVerify {
            lblDescription.text = "Enter the 4 digit code we sent you via SMS to Continue"
        } else {
            lblDescription.text = "Enter the 4 digit code we sent you via Email to Continue"
        }

        first.myDelegate = self
        second.myDelegate = self
        third.myDelegate = self
        fourth.myDelegate = self
        
        first.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        second.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        third.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        first.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        second.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        third.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fourth.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        
        currentTextFiled = first
    }
    
    func textFieldDidDelete() {
        if(currentTextFiled == first)
        {
            if(first.text!.count > 0 )
            {
                first.text = ""
                currentTextFiled = first
            }
            else
            {
                self.view.endEditing(true)
            }
        }
        else if(currentTextFiled == second)
        {
            if(second.text!.count > 0 )
            {
                second.text = ""
            }
            else
            {
                first.text = ""
                first.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == third)
        {
            if(third.text!.count > 0 )
            {
                third.text = ""
            }
            else
            {
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fourth)
        {
            if(fourth.text!.count > 0 )
            {
                fourth.text = ""
            }
            else
            {
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            }
        }
    }
    
    
    @objc func myTargetFunction(textField: UITextField) {
        if(textField == first)
        {
            currentTextFiled = first
            first.text = ""
        }
        else if(textField == second)
        {
            currentTextFiled = second
            second.text = ""
        }
        else if(textField == third)
        {
            currentTextFiled = third
            third.text = ""
        }
        else if(textField == fourth)
        {
            currentTextFiled = fourth
            fourth.text = ""
        }
    }
    
    /*
    @objc func textFieldDidChange(textField: UITextField){
        if(textField == first)
        {
            currentTextFiled = second
            second.becomeFirstResponder()
        }
        else if(textField == second)
        {
            currentTextFiled = third
            third.becomeFirstResponder()
        }
        else if(textField == third)
        {
            currentTextFiled = fourth
            fourth.becomeFirstResponder()
        }
        else if(textField == fourth)
        {
            self.view.endEditing(true)
        }
    }*/
    
    @objc func textFieldDidChange(textField: UITextField) {
        let text = textField.text
        if text?.count == 1 {
            switch textField{
            case first:
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            case second:
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            case third:
                currentTextFiled = fourth
                fourth.text = ""
                fourth.becomeFirstResponder()
            case fourth:
                self.view.endEditing(true)
            default:
                break
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    //MARK:- TextField Delegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldDidEndEditing(textField)
        return true
    }
}


extension AddOTPVC {
    
    func sendVerifyUserReq(){
        self.view.endEditing(true)
        let flag = isMobileVerify ? "is_phone_verified" : "is_email_verified"
        let code = "\(first.text!)" + "\(second.text!)" + "\(third.text!)" + "\(fourth.text!)"
        let dictParam: NSDictionary = ["id":AppUtilities.sharedInstance.getUserID(),
                                       "otp": code,
                                       "phone": strPhoneNo,
                                       "email": strEmail,
                                       "flag": flag,
                                       "country_code": strCountryCode]
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIVerifyUser,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                
                if responseDict?.object(forKey: kSuccess) as! String == "1" {
                    if self.isForEditPhoneNo {
                        self.delegateEditMobileNo?.updateMobileNo()
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        if AppUtilities.sharedInstance.userType == SEEKER {
                            self.stopTimer()
                            if let dictUserInfo: NSDictionary = UserDefaults.standard.object(forKey: UD_UserData) as? NSDictionary {
                                if dictUserInfo.object(forKey: "is_id_submited") as! String == "0" {
                                    self.askForLoseID()
                                } else if dictUserInfo.object(forKey: "is_id_submited") as! String == "2" {
                                    AppUtilities.sharedInstance.tellProofPending()
                                } else if dictUserInfo.object(forKey: "is_id_submited") as! String == "3" {
                                    self.tellProofRejected()
                                } else {
                                    self.gotoHome()
                                }
                            } else {
                                self.askForLoseID()
                            }
                        } else {
                            self.gotoHome()
                        }
                    }
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
    
    func askForLoseID() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = "You’re almost ready."
        vc.strDescription = "Did you lose your ID or NRIC?"
        vc.isHideOk = true
        vc.type = "askforlostid"
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func tellProofRejected() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = "Your ID Proof is Rejected by the Management. Re-submit a clear and accurate Proof now?"
        vc.type = "proofreject"
        vc.isHideOk = true
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func gotoHome() {
        AppUtilities.sharedInstance.saveData(data: "1" as AnyObject, key: UD_IsLoggedIn)
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idSWRevealViewController)
        self.navigationController?.pushViewController(vc, animated: true)
        /*
        if(AppUtilities.sharedInstance.isSubmitForm == true)
        {
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
            self.navigationController?.pushViewController(vc, animated: true)
        }*/
        
    }

    
    func sendOTPReq(){
        self.view.endEditing(true)
        let dictParam: NSDictionary = ["id": AppUtilities.sharedInstance.getUserID(),
                                       "email": strEmail,
                                       "phone": strPhoneNo,
                                       "country_code": strCountryCode]
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APISendOTP,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict?.object(forKey: kSuccess) as! String == "1" {
                    self.first.text = ""
                    self.second.text = ""
                    self.third.text = ""
                    self.fourth.text = ""
                    self.startTimer()
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
}

extension AddOTPVC {
    func setThemeColor() {
        if AppUtilities.sharedInstance.userType == SEEKER {
            imgBack.image = UIImage(named:"img_bg")
            btnResend.setTitleColor(REDCOLOR, for: .normal)
            btnDone.setTitleColor(REDCOLOR, for: .normal)
        } else {
            imgBack.image = UIImage(named:"login_bg")
            btnResend.setTitleColor(BLUECOLOR, for: .normal)
            btnDone.setTitleColor(BLUECOLOR, for: .normal)
        }
    }
}


extension StringProtocol where IndexDistance == Int {
    func index(at offset: Int, from start: Index? = nil) -> Index? {
        return index(start ?? startIndex, offsetBy: offset, limitedBy: endIndex)
    }
    func character(at offset: Int) -> Character? {
        precondition(offset >= 0, "offset can't be negative")
        guard let index = index(at: offset) else { return nil }
        return self[index]
    }
    subscript(_ range: CountableRange<Int>) -> SubSequence {
        precondition(range.lowerBound >= 0, "lowerBound can't be negative")
        let start = index(at: range.lowerBound) ?? endIndex
        let end = index(at: range.count, from: start) ?? endIndex
        return self[start..<end]
    }
    subscript(_ range: CountableClosedRange<Int>) -> SubSequence {
        precondition(range.lowerBound >= 0, "lowerBound can't be negative")
        let start = index(at: range.lowerBound) ?? endIndex
        let end = index(at: range.count, from: start) ?? endIndex
        return self[start..<end]
    }
    subscript(_ range: PartialRangeUpTo<Int>) -> SubSequence {
        return prefix(range.upperBound)
    }
    subscript(_ range: PartialRangeThrough<Int>) -> SubSequence {
        return prefix(range.upperBound+1)
    }
    subscript(_ range: PartialRangeFrom<Int>) -> SubSequence {
        return suffix(Swift.max(0,count-range.lowerBound))
    }
}
extension Substring {
    var string: String { return String(self) }
}
