//
//  ForgotPassVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 31/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController {
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var txtEmailPhone: UITextField!
    @IBOutlet var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setThemeColor()
    }

    //MARK:- UIBUTTON ACTION
    @IBAction func btnSubmit(_ sender: Any) {
        if (validateTxtFieldLength(txtEmailPhone, withMessage: EnterForgotPassMsg)) {
            self.sendForgotpassReq()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ForgotPassVC {
    func sendForgotpassReq(){
        self.view.endEditing(true)
        let dictParam: NSDictionary = ["email":txtEmailPhone.text!]
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIForgotPassword,
            parameters: dictParam,
            showLoader:true)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                return
            }
            else
            {
                print(responseDict ?? "Not Found")
                
                if responseDict?.object(forKey: kSuccess) as! String == "1" {
                    self.openAlert(message: responseDict?.object(forKey: kMessage) as! String)
                } else {
                    showMessage(responseDict?.object(forKey: kMessage) as! String)
                }
            }
        }
    }
}

extension ForgotPassVC: displayAlertDelegate {
    
    func openAlert(message: String) {
        let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
        vc.displayAlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = message
        vc.type = "simplealert"
        self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnOk(type: String) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func btnYes(type: String) {
        
    }
    
    func btnNo(type: String) {
        
    }
}

extension ForgotPassVC {
    func setThemeColor() {
        if AppUtilities.sharedInstance.userType == SEEKER {
            imgBack.image = UIImage(named:"img_bg")
            btnSubmit.setTitleColor(REDCOLOR, for: .normal)
        } else {
            imgBack.image = UIImage(named:"login_bg")
            btnSubmit.setTitleColor(BLUECOLOR, for: .normal)
        }
    }
}
