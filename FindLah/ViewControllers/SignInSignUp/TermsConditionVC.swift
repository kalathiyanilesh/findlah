//
//  TermsConditionVC.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 31/10/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit

class TermsConditionVC: UIViewController {
    
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var myWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppUtilities.sharedInstance.userType == SEEKER {
            imgBack.image = UIImage(named:"img_bg")
        } else {
            imgBack.image = UIImage(named:"login_bg")
        }
        myWebView.loadRequest(URLRequest(url: URL(string: "https://www.google.co.in")!))
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
