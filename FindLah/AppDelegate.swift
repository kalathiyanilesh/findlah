//
//  AppDelegate.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 15/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import FBSDKCoreKit
import GoogleSignIn
import UserNotifications
import Stripe
import Firebase
import Branch
//import Braintree

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate, CLLocationManagerDelegate, AlertDelegate, displayAlertDelegate {
    var window: UIWindow?
    var isCallAPIs: Bool = false
    var isGoToDetail: Bool = false
    var isFromLaunch : Bool = false
    var isInDetailScreen: Bool = false
    var isMethodCallOneTime: Bool = false
    var strBodyMsg: String = ""
    var strPushExtra: String = ""
    var strDetailReportID: String = "0"
    var totalUnreadCnt: Int = 0
    let locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        IQKeyboardManager.shared.enable = true
        GetCurrentLocation()
        
        STPPaymentConfiguration.shared().publishableKey = "pk_test_c0stlgAAksvaxndkLsI6u04t"
        //STPPaymentConfiguration.shared().createCardSources = false
        
        BTAppSwitch.setReturnURLScheme("\(Bundle.main.bundleIdentifier!).payments")
        
        GMSServices.provideAPIKey("AIzaSyAhP3_85Elw4JkKjzfRfIBjKrzFZnbntLE")
        GMSPlacesClient.provideAPIKey("AIzaSyAhP3_85Elw4JkKjzfRfIBjKrzFZnbntLE")

        GIDSignIn.sharedInstance().clientID = "649338891643-fkj6sn6rhs8ks592m6sma17pq3uu7h5a.apps.googleusercontent.com"
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        Branch.setUseTestBranchKey(true)
        Branch.getInstance().setDebug()
        
        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
            print(params as? [String: AnyObject] ?? {})
            if error == nil && AppUtilities.sharedInstance.isLogin() == "1" {
                let allKey : NSArray = (params! as NSDictionary).allKeys as NSArray
                if allKey.contains("data") == true {
                    let strDic : String = params?["data"] as! String
                    if let dictReport = convertStringToDictionary(str: strDic) as NSDictionary? {
                        print(dictReport)
                        
                        let strReportType = dictReport.object(forKey: "report_type") as? String ?? ""
                        let vc = loadVC(strStoryboardId: SB_LOFI, strVCId: idDetailsVC) as! DetailsVC
                        vc.dictReport = dictReport
                        vc.isAnswered = false
                        vc.isFromBranch = true
                        vc.isFromFinder = false
                        if strReportType == FINDER {
                            vc.isFromFinder = true
                        }
                        
                        let navVC = UINavigationController(rootViewController: vc)
                        navVC.isNavigationBarHidden = true
                        APP_DELEGATE.window?.rootViewController = navVC
                        APP_DELEGATE.window?.makeKeyAndVisible()
                    }
                    
                    
                    /*
                    self.gotoHomeScreen()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        self.isGoToDetail = true
                        NotificationCenter.default.post(name: Notification.Name("gotoDetail"), object: nil, userInfo: dictReport as? [AnyHashable : Any])
                    }*/
                }
            } else {
                print(error?.localizedDescription ?? "error")
            }
        })
        
        AppUtilities.sharedInstance.userType = FINDER
        if let strUsertype = UserDefaults.standard.object(forKey: kUserType) as? String {
            AppUtilities.sharedInstance.userType = strUsertype
        }
        setCursorColor()
        
        AppUtilities.sharedInstance.arrCatHint = AppUtilities.sharedInstance.getCatHintArray()
        AppUtilities.sharedInstance.getCategoryAndHintReq()
        
        AppUtilities.sharedInstance.arrBankList = AppUtilities.sharedInstance.getBankListArray()
        AppUtilities.sharedInstance.getBankListReq()

        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        let filePath = Bundle.main.path(forResource: "GoogleService-Info_FIREBASE", ofType: "plist")!
        let options = FirebaseOptions(contentsOfFile: filePath)
        FirebaseApp.configure(options: options!)
        Messaging.messaging().delegate = self
    
        if AppUtilities.sharedInstance.isLogin() == "1" {
            isFromLaunch = true
        }
        
        let isFirstLaunch = UserDefaults.isFirstLaunch()
        if !isFirstLaunch {
            AskForNotification()
        }
        
        /*
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (settings) in
            if(settings.authorizationStatus == .notDetermined)
            {
                self.askForPushAllow()
            }
        }*/
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let sourceApplication: String? = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        if url.scheme?.localizedCaseInsensitiveCompare("\(Bundle.main.bundleIdentifier!).payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }

        let googleDidHandle = GIDSignIn.sharedInstance().handle(url as URL, sourceApplication: sourceApplication, annotation: nil)
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(app,open: url as URL, sourceApplication: sourceApplication, annotation: nil)
        return googleDidHandle || facebookDidHandle
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // pass the url to the handle deep link call
        let branchHandled = Branch.getInstance().application(application,
                                                             open: url,
                                                             sourceApplication: sourceApplication,
                                                             annotation: annotation
        )
        if (!branchHandled) {
            // If not handled by Branch, do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        }
        
        // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // handler for Universal Links
        Branch.getInstance().continue(userActivity)
        return true
    }
    
    //MARK:- UINotification Methods
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        /*
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        
        print(token)*/
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        Branch.getInstance().handlePushNotification(userInfo)
        self.handlePushNotification(userInfo: userInfo as NSDictionary, application: application)
    }

    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //Branch.getInstance().handlePushNotification(userInfo)
        self.handlePushNotification(userInfo: userInfo as NSDictionary, application: application)
    }
    
    func handlePushNotification(userInfo:NSDictionary,application: UIApplication)
    {
        print(userInfo)
        UIApplication.shared.applicationIconBadgeNumber = 0
        if AppUtilities.sharedInstance.isLogin() == "0" { return }
        strBodyMsg = userInfo.object(forKey: "text") as? String ?? ""
        self.strPushExtra = userInfo.object(forKey: "extra") as? String ?? ""
        let pushType = userInfo.object(forKey: "notification_type") as? String ?? ""
        
        let strNotificationID = userInfo.object(forKey: "notification_id") as? String ?? "0"
        sendReadNoti(strID: strNotificationID)
        
        if !isFromLaunch {
            self.window?.rootViewController?.dismiss(animated: true, completion: nil)
        }
        
        if pushType == PROOF_VERIFICATION_REJECT {
            isFromLaunch = false
            let strMsg = String(format:"%@, You want to resubmit a proof?",strBodyMsg)
            self.openPushAlert(description: strMsg, type: pushType, hideOkbtn: true)
        } else if pushType == PROOF_VERIFICATION_SUCCESS {
            isFromLaunch = false
            showMessage(self.strBodyMsg)
            if let dict = UserDefaults.standard.object(forKey: UD_UserData) as? NSDictionary {
                let dictUserInfo: NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
                dictUserInfo.setObject("1", forKey: "is_id_submited" as NSCopying)
                AppUtilities.sharedInstance.saveData(data: dictUserInfo.copy() as! NSDictionary, key: UD_UserData)
            }
        } else if pushType == REPORT_STATUS_OFFLINE || pushType == WIN_REWARD_AMOUNT {
            self.openPushAlert(description: strBodyMsg, type: pushType, hideOkbtn: false)
        } else if self.strPushExtra.count > 0 {
            let dictData: NSDictionary = convertStringToDictionary(str: self.strPushExtra)! as NSDictionary
            let dictExtra: NSDictionary = dictData.object(forKey: "report_data") as! NSDictionary
            if isFromLaunch {
                isFromLaunch = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    let strID = dictExtra.object(forKey: "report_id") as? String ?? "0"
                    if self.isInDetailScreen && self.strDetailReportID == strID {
                        self.isMethodCallOneTime = true
                        showMessage(self.strBodyMsg)
                        NotificationCenter.default.post(name: Notification.Name("updateDetailData"), object: nil, userInfo: dictExtra as? [AnyHashable : Any])
                    } else {
                        self.isGoToDetail = true
                        self.isCallAPIs = true
                        showMessage(self.strBodyMsg)
                        NotificationCenter.default.post(name: Notification.Name("gotoDetail"), object: nil, userInfo: dictExtra as? [AnyHashable : Any])
                    }
                }
            } else {
                let strID = dictExtra.object(forKey: "report_id") as? String ?? "0"
                if self.isInDetailScreen && self.strDetailReportID == strID {
                    self.isFromLaunch = false
                    self.isMethodCallOneTime = true
                    showMessage(self.strBodyMsg)
                    NotificationCenter.default.post(name: Notification.Name("updateDetailData"), object: nil, userInfo: dictExtra as? [AnyHashable : Any])
                } else {
                    self.openPushAlert(description: strBodyMsg, type: pushType, hideOkbtn: false)
                }
            }
        }
    }
    
    //UIApplicationOpenURLOptionsSourceApplicationKey
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        FBSDKAppEvents.activateApp()
        if AppUtilities.sharedInstance.isLogin() == "1" {
            sendGetUnreadNoti()
        }
        UpdateUserData()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

extension AppDelegate {
    func AskForNotification() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                    (granted, error) in
                    UNUserNotificationCenter.current().delegate = self
                    //self.askForPushAllow()
                    
                    let alertController = UIAlertController(title: nil, message: "You need to set your push notification.", preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            })
                        }
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    alertController.addAction(settingsAction)
                    DispatchQueue.main.async {
                        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                        
                    }
                }
            }
        }
    }
}

extension AppDelegate {

    func openPushAlert(description: String, type: String, hideOkbtn: Bool) {
        /*
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.type = type
        vc.isHideOk = hideOkbtn
        vc.strTitle = APPNAME
        vc.strDescription = description
        self.window?.rootViewController?.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)*/
        
        
        let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
        vc.displayAlertDelegate = self
        vc.type = type
        vc.isHideOk = hideOkbtn
        vc.strTitle = APPNAME
        vc.strDescription = description
        self.window?.rootViewController?.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func askForLoseID() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = "You’re almost ready."
        vc.strDescription = "Did you lose your ID or NRIC?"
        vc.isHideOk = true
        vc.type = "askforlostid"
        self.window?.rootViewController?.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func askForPushAllow() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = "You need to set your push notification."
        vc.type = "forpush"
        self.window?.rootViewController?.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnYes(type: String) {
        self.window?.rootViewController?.dismissPopupViewControllerWithanimationType (MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == PROOF_VERIFICATION_REJECT {
                self.isFromLaunch = false
                self.askForLoseID()
            } else if type == "askforlostid" {
                self.isFromLaunch = false
                let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
                vc.displayAlertDelegate = self
                vc.strTitle = kIMPNOTICE
                vc.strDescription = kLATESTPHONEBILL
                vc.type = kBILLINGBILL
                self.window?.rootViewController?.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
            }
        }
    }
    
    func btnNo(type: String) {
        self.window?.rootViewController?.dismissPopupViewControllerWithanimationType (MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "askforlostid" {
                self.isFromLaunch = false
                let vc : DisplayAlertVC = DisplayAlertVC(nibName: idDisplayAlertVC, bundle: nil)
                vc.displayAlertDelegate = self
                vc.strTitle = kIMPNOTICE
                vc.strDescription = kPHOTOIDNRIC
                vc.type = kHIMID
                self.window?.rootViewController?.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
            }
        }
        
    }
    
    func btnOk(type: String) {
        self.window?.rootViewController?.dismissPopupViewControllerWithanimationType (MJPopupViewAnimationSlideTopBottom)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == kBILLINGBILL || type == kHIMID {
                self.openImagePicker(type: type)
            } else if type == PROOF_VERIFICATION_PENDING {
                self.askForLoseID()
            } else if type == REPORT_STATUS_OFFLINE || type == WIN_REWARD_AMOUNT {
                
            } else if type == "forpush" {
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    })
                }
            } else {
                if self.strPushExtra.count > 0 {
                    let dictData: NSDictionary = convertStringToDictionary(str: self.strPushExtra)! as NSDictionary
                    let dictExtra: NSDictionary = dictData.object(forKey: "report_data") as! NSDictionary
                    let strID = dictExtra.object(forKey: "report_id") as? String ?? "0"
                    if self.isInDetailScreen && self.strDetailReportID == strID {
                        self.isFromLaunch = false
                        self.isMethodCallOneTime = true
                        showMessage(self.strBodyMsg)
                        NotificationCenter.default.post(name: Notification.Name("updateDetailData"), object: nil, userInfo: dictExtra as? [AnyHashable : Any])
                    } else {
                        self.isFromLaunch = false
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.isGoToDetail = true
                            self.isCallAPIs = true
                            NotificationCenter.default.post(name: Notification.Name("gotoDetail"), object: nil, userInfo: dictExtra as? [AnyHashable : Any])
                        }
                    }
                }
            }
        }
    }
}

extension AppDelegate: ImagePickerDelegate{
    
    func openImagePicker(type: String) {
        ImagePicker.sharedInstance.delegate = self
        ImagePicker.sharedInstance.strUserType = FINDER
        ImagePicker.sharedInstance.selectImage(sender: type)
    }
    
    func pickImageComplete(_ imageData: UIImage, sender: String)
    {
        if sender == kBILLINGBILL || sender == kHIMID {
            let strIDProof = sender == kHIMID ? "1" : "0"
            sendUploadIdProof(image: imageData, strIsIDProof: strIDProof, responseData: {(error,responseDict) -> Void in
                hideMessage()
                if error != nil {
                    return
                } else {
                    print(responseDict ?? "Not Found")
                    if responseDict!["success"] as? String == "1"
                    {
                    }
                    else {
                        showMessage(responseDict?.object(forKey: kMessage) as! String)
                    }
                }
            })
        }
    }
}

extension AppDelegate {
    func gotoHomeScreen() {
        let storyboard = UIStoryboard(name: SB_MAIN, bundle: nil)
        let navigation = storyboard.instantiateViewController(withIdentifier: "mainnav") as! UINavigationController
        window?.rootViewController = navigation
    }
}

extension AppDelegate {
    func GetCurrentLocation()
    {
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        locationManager.stopUpdatingLocation()
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        lat_currnt = locValue.latitude
        long_currnt = locValue.longitude
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        locationManager.startUpdatingLocation()
    }
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
        }
    }
}

extension AppDelegate {
    func UpdateUserData() {
        if AppUtilities.sharedInstance.userType == SEEKER && AppUtilities.sharedInstance.isLogin() == "1" {
            let dictUserInfo = UserDefaults.standard.object(forKey: UD_UserData) as! NSDictionary
            if dictUserInfo.object(forKey: "is_id_submited") as! String != "1" {
                sendGetUserDetailReq()
            }
        }
    }
    
    func sendGetUserDetailReq() {
        let dictParam: NSDictionary = ["user_id":AppUtilities.sharedInstance.getUserID()]
        
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIGetOtherUserProfile,
            parameters: dictParam,
            showLoader:false)
        {(error,responseDict) -> Void in
            if error != nil
            {
                
            }
            else
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    let dictUserInfo = responseDict?["data"] as! NSDictionary
                    AppUtilities.sharedInstance.saveUserData(dictUserInfo: dictUserInfo, isUpdateUser: true)
                }
            }
        }
    }
}
