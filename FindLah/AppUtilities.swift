//
//  AppUtilities.swift
//  Lost&Found
//
//  Created by Nilesh Kalathiya on 21/07/18.
//  Copyright © 2018 Nilesh. All rights reserved.
//

import UIKit
import Firebase

class AppUtilities: NSObject, AlertDelegate {

    var userType: String = ""
    var isSubmitForm: Bool = false
    var isGetFinderList: Bool = false
    var isGetLosterList: Bool = false
    var isPostAns: Bool = false
    var isCallFinderList: Bool = false
    var isCallSeekerList: Bool = false
    var isCallFilterFinder: Bool = false
    var isCallFilterSeeker: Bool = false
    var arrCatHint: NSMutableArray = NSMutableArray()
    var arrBankList: NSMutableArray = NSMutableArray()
    var arrQuesAns: NSMutableArray = NSMutableArray()
    let arrMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
    var txtAns1: IQTextView = IQTextView()
    var txtAns2: IQTextView = IQTextView()
    var txtAns3: IQTextView = IQTextView()
    var txtAns4: IQTextView = IQTextView()
    var txtAns5: IQTextView = IQTextView()
    
    class var sharedInstance : AppUtilities {
        struct Singleton {
            static let instance = AppUtilities()
        }
        
        return Singleton.instance
    }
    
    func isLogin() -> String {
        if(UserDefaults.standard.string(forKey: UD_IsLoggedIn) == nil)
        {
            return "0"
        }
        else
        {
            return UserDefaults.standard.string(forKey: UD_IsLoggedIn)!
        }
    }
    
    /*
    func showAlert(title : NSString, msg : NSString)
    {
        let alert = UIAlertView(title: title as String, message: msg as String, delegate: nil, cancelButtonTitle: "OK")
        DispatchQueue.main.async {
            alert.show()
        }
    }*/
    
    func getDeviceToken() -> String {
        return Messaging.messaging().fcmToken != nil ? Messaging.messaging().fcmToken! : ""
    }
    
    func getUserID() -> String {
        if(UserDefaults.standard.string(forKey: UD_UserId) == nil)
        {
            return ""
        }
        else
        {
            return UserDefaults.standard.string(forKey: UD_UserId)!
        }
    }
    
    func getCatHintArray() -> NSMutableArray {
        if(UserDefaults.standard.string(forKey: CatHint) == nil)
        {
            return NSMutableArray.init()
        }
        else
        {
            return UserDefaults.standard.object(forKey: CatHint) as! NSMutableArray
        }
    }
    
    func getBankListArray() -> NSMutableArray {
        if(UserDefaults.standard.string(forKey: BankList) == nil)
        {
            return NSMutableArray.init()
        }
        else
        {
            return UserDefaults.standard.object(forKey: BankList) as! NSMutableArray
        }
    }
    
    func setNoDataLabel(tableView:UITableView, array:NSMutableArray, text:String) -> Int
    {
        var numOfSections = 0
        
        if array.count != 0 {
            tableView.backgroundView = nil
            numOfSections = 1
        } else {
            let noDataLabel = UILabel()
            noDataLabel.frame = CGRect(x: 10, y: 0, width: tableView.frame.size.width-20, height: tableView.frame.size.height)
            noDataLabel.text = text
            noDataLabel.font = UIFont(name: "Montserrat-Bold", size: 14)
            noDataLabel.numberOfLines = 0
            noDataLabel.textColor = UIColor.lightGray.withAlphaComponent(0.4)
            noDataLabel.textAlignment = NSTextAlignment.center
            tableView.backgroundView = noDataLabel;
            tableView.separatorStyle = .none
        }
        
        return numOfSections
    }
    
    func saveUserData(dictUserInfo: NSDictionary, isUpdateUser: Bool) {
        
        let userType = dictUserInfo.object(forKey: "user_type") as! String
        let userid = dictUserInfo.object(forKey: "id") as! String
        let username = dictUserInfo.object(forKey: "name") as! String
        let userimage = dictUserInfo.object(forKey: "profile_img") as! String
        let userphoneno = dictUserInfo.object(forKey: "phone") as! String
        let useremail = dictUserInfo.object(forKey: "email") as! String
        let useraddress = dictUserInfo.object(forKey: "address") as! String
        let userunitno = dictUserInfo.object(forKey: "unitno") as! String
        
        if isUpdateUser {
            if userType == "1" {
                AppUtilities.sharedInstance.userType = SEEKER
            } else {
                AppUtilities.sharedInstance.userType = FINDER
            }
            AppUtilities.sharedInstance.saveData(data: AppUtilities.sharedInstance.userType as AnyObject, key: kUserType)
            setCursorColor()
        }

        if let strOTP = dictUserInfo.object(forKey: "otp") as? String {
            AppUtilities.sharedInstance.saveData(data: strOTP as AnyObject, key: kUserOTP)
        }
        
        AppUtilities.sharedInstance.saveData(data: dictUserInfo, key: UD_UserData)
        AppUtilities.sharedInstance.saveData(data: userid as AnyObject, key: UD_UserId)
        AppUtilities.sharedInstance.saveData(data: username as AnyObject, key: UD_UserName)
        AppUtilities.sharedInstance.saveData(data: userimage as AnyObject, key: UD_UserProfileURL)
        AppUtilities.sharedInstance.saveData(data: userphoneno as AnyObject, key: UD_UserMobileNo)
        AppUtilities.sharedInstance.saveData(data: useremail as AnyObject, key: UD_UserEmail)
        AppUtilities.sharedInstance.saveData(data: useraddress as AnyObject, key: UD_UserAddresss)
        AppUtilities.sharedInstance.saveData(data: userunitno as AnyObject, key: UD_UserUnitNo)
    }
    
    func saveData(data: AnyObject, key : String)
    {
        UserDefaults.standard.set(data, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func getUserData() -> NSDictionary {
        return UserDefaults.standard.object(forKey: UD_UserData) as! NSDictionary
    }
    
    func getCategoryAndHintReq() {
        HttpRequestManager.sharedInstance.requestWithGetJsonParam(endpointurl: Server_URL, service: APICategory, showLoader: false) { (error,responseDict) -> Void in
            if error == nil
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    self.arrCatHint = arrayOfFilteredBy(arr: responseDict!.object(forKey: kData) as! NSArray) as! NSMutableArray
                    self.saveData(data: self.arrCatHint, key: CatHint)
                }
            }
        }
    }
    
    func getBankListReq() {
        HttpRequestManager.sharedInstance.requestWithGetJsonParam(endpointurl: Server_URL, service: APIGetBankList, showLoader: false) { (error,responseDict) -> Void in
            if error == nil
            {
                print(responseDict ?? "Not Found")
                if responseDict!["success"] as? String == "1"
                {
                    self.arrBankList = arrayOfFilteredBy(arr: responseDict!.object(forKey: kData) as! NSArray) as! NSMutableArray
                    self.saveData(data: self.arrBankList, key: BankList)
                }
            }
        }
    }
    
    func sendDeleteReportReq(reportID: String) {
        let dictParam: NSDictionary = ["report_id":reportID]
        HttpRequestManager.sharedInstance.requestWithPostJsonParam(
            endpointurl: Server_URL,
            service:APIDeleteReport,
            parameters: dictParam,
            showLoader:false)
        {(error,responseDict) -> Void in
            hideMessage()
            if error != nil
            {
                print(error?.localizedDescription ?? "error is nil")
            }
            else
            {
                print(responseDict ?? "Not Found")
            }
        }
    }
    
    func tellProofPending() {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = "Your User Profile Verification is currently in process. We will notify you within 24-hours."
        vc.type = "proofnotverify"
        APP_DELEGATE.window?.rootViewController?.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func logout(userType: String) {
        let vc : AlertVC = AlertVC(nibName: idAlertVC, bundle: nil)
        vc.AlertDelegate = self
        vc.strTitle = APPNAME
        vc.strDescription = "You need to login or register."
        vc.isHideOk = true
        vc.type = "needlogin"
        vc.strUserType = userType
        APP_DELEGATE.window?.rootViewController?.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
    }
    
    func btnYes(type: String) {
        APP_DELEGATE.window?.rootViewController?.dismissPopupViewControllerWithanimationType (MJPopupViewAnimationSlideTopBottom)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if type == "needlogin" {
                self.UserLogout()
            }
        }
    }
    
    func btnNo(type: String) {
        APP_DELEGATE.window?.rootViewController?.dismissPopupViewControllerWithanimationType (MJPopupViewAnimationSlideTopBottom)
    }
    
    func btnOk(type: String) {
        APP_DELEGATE.window?.rootViewController?.dismissPopupViewControllerWithanimationType (MJPopupViewAnimationSlideTopBottom)
    }
    
    func UserLogout() {
        cleanUserDefaultData()
        let vc = loadVC(strStoryboardId: SB_SISU, strVCId: idLoginNav) as! UINavigationController
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = vc
    }
}

extension UIImage {
    public func base64() -> String? {
        var imageData: Data?
        imageData = compressImage(image: self) as Data
        return imageData?.base64EncodedString()
    }
}
