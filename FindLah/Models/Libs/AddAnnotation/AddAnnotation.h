//
//  AddAnnotation.h


#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface AddAnnotation : NSObject <MKAnnotation>
{
    NSString *title;
    NSString *subtitle;
    CLLocationCoordinate2D coordinate;
}

@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *subtitle;
@property(nonatomic)CLLocationCoordinate2D coordinate;
@property (nonatomic, readwrite) int tag;

@end
