//
//  AddAnnotation.m


#import "AddAnnotation.h"

@implementation AddAnnotation


@synthesize title,subtitle,coordinate, tag;


- (NSString *)title
{
    return title;
}
-(NSString *)subtitle
{
    return subtitle;
}
- (id)initWithCoordinate:(CLLocationCoordinate2D) c
{
	coordinate = c;
	NSLog(@" %f %f",c.latitude,c.longitude);
	return self;
}

@end
