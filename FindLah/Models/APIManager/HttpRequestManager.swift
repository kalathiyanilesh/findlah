//
//  HttpRequestManager.swift
//  Copyright © 2016 PayalUmraliya. All rights reserved.

import UIKit
import SwiftyJSON

//Web Service Result

public enum RESPONSE_STATUS : NSInteger
{
    case INVALID
    case VALID
    case MESSAGE
}

class HttpRequestManager
{
    static let sharedInstance = HttpRequestManager()
    
    //MARK:- POST
    func requestWithPostMultipartParam(endpointurl:String,
                                       service:String,
                                       parameters:NSDictionary,
                                       image:UIImage,
                                       imageParam:String,
                                       showLoader:Bool,
                                       responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void)
    {
        if isConnectedToNetwork()
        {
            if showLoader { showLoaderHUD(strMessage: "") }
            let strURL = endpointurl + service
            DLog(message: "URL : \(strURL) Param :\(parameters)")
            let manager = AFHTTPRequestOperationManager()
            manager.post(strURL, parameters: parameters, constructingBodyWith: { (formData: AFMultipartFormData!) in
                if imageParam.count > 0 {
                    let imgData = compressImage(image: image) as Data
                    formData.appendPart(withFileData: imgData as Data!, name: imageParam, fileName: "image.png", mimeType: "image/jpeg")
                }
            }, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                responseData(nil,dictResponse)
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                hideLoaderHUD()
                responseData(error! as NSError,nil)
            }
        }
    }
    
    func requestWithPostJsonParam( endpointurl:String,
                                   service:String,
                                   parameters:NSDictionary,
                                   showLoader:Bool,
                                   responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void)
    {
        if isConnectedToNetwork()
        {
            if(showLoader) { showLoaderHUD(strMessage: "") }
            ShowNetworkIndicator(xx: true)
            let strURL = endpointurl + service
            DLog(message: "URL : \(strURL) Param :\(parameters)")
            let manager = AFHTTPRequestOperationManager()
            manager.post(strURL, parameters: parameters, success: { (operation, responseObject) in
                hideLoaderHUD()
                ShowNetworkIndicator(xx: false)
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                responseData(nil,dictResponse)
            }) { (operation, error) in
                hideLoaderHUD()
                ShowNetworkIndicator(xx: false)
                print(operation?.responseString ?? "responseString Not Found")
                responseData(error! as NSError,nil)
            }
        }
    }
    
    func requestWithGetJsonParam( endpointurl:String,
                                   service:String,
                                   showLoader:Bool,
                                   responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void)
    {
        if isConnectedToNetwork()
        {
            if(showLoader) { showLoaderHUD(strMessage: "") }
            ShowNetworkIndicator(xx: true)
            let strURL = endpointurl + service
            DLog(message: "URL : \(strURL)")
            let manager = AFHTTPRequestOperationManager()
            manager.get(strURL, parameters: nil, success: { (operation, responseObject) in
                hideLoaderHUD()
                ShowNetworkIndicator(xx: false)
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                responseData(nil,dictResponse)
            }) { (operation, error) in
                hideLoaderHUD()
                ShowNetworkIndicator(xx: false)
                print(operation?.responseString ?? "responseString Not Found")
                responseData(error! as NSError,nil)
            }
        }
    }

    //MARK: Convert InTo Json String
    func jsonToString(json: AnyObject) -> String{
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
            let convertedString = String(data: data, encoding: String.Encoding.utf8)
            return convertedString!
        } catch let myJSONError {
            print(myJSONError)
            return ""
        }
    }
}


