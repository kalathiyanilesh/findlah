//
//  constant.swift

import Foundation
import UIKit
import CoreLocation
import SystemConfiguration
import AVFoundation
import Branch

var spinner = RPLoadingAnimationView.init(frame: CGRect.zero)
var overlayView = UIView()
var lat_currnt: Double = 0
var long_currnt: Double = 0
var noti_unread_count: Int = 0

enum DELIVERY_TYPE: String{
    case CONTACTME = "0"
    case POPSTATION = "1"
    case COURIER = "2"
}

enum CONTACTME_TYPE: String {
    case PENDING = "0"
    case VERIFIED = "1"
    case REJECT = "2"
    case COMPLETE = "3"
    case COMPLETE_SECOND = "6"
}

enum POPSTATION_TYPE: String {
    case PROCESSING = "0"
    case WAIT_FOR_ITEM_SUBMISSION = "1"
    case SUBMITED = "2"
    case VERIFY_PENDING = "3"
    case REJECTED = "4"
    case COMPLETE = "6"
}

enum COURIER_TYPE: String {
    case PROCESSING = "0"
    case VERIFYED_DETAILS = "1"
    case PROCESSING_COLCTN = "2"
    case SUBMITED = "3"
    case REJECT = "4"
    case CONFIRM = "5"
    case COMPLETE = "6"
}

struct DIRECTORY_NAME
{
    public static let IMAGES = "Images"
    public static let VIDEOS = "Videos"
    public static let DOWNLOAD_VIDEOS = "Download_videos"
}

public func DegreesToRadians(degrees: Float) -> Float {
    return Float(Double(degrees) * .pi / 180)
}

public let isSimulator: Bool = {
    var isSim = false
    #if arch(i386) || arch(x86_64)
        isSim = true
    #endif
    return isSim
}()

//MARK:-  Get VC for navigation
//var banner = NotificationBanner(title: "", subtitle: "", style: .success)
public func getStoryboard(storyboardName: String) -> UIStoryboard {
    return UIStoryboard(name: storyboardName, bundle: nil)
}

public func loadVC(strStoryboardId: String, strVCId: String) -> UIViewController {
    
    let vc = getStoryboard(storyboardName: strStoryboardId).instantiateViewController(withIdentifier: strVCId)
    return vc
}

public var CurrentTimeStamp: String
{
    return "\(NSDate().timeIntervalSince1970 * 1000)"
}

func randomString(length: Int) -> String
{
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    var randomString = ""
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    return randomString
}

func getDate(date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
    let strdate = dateFormatter.string(from: date as Date)
    return strdate
}
func convertTimeToLocalZone(time:String) -> NSDate
{
    let dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
    let inputTimeZone = NSTimeZone(abbreviation: "UTC")
    let inputDateFormatter = DateFormatter()
    inputDateFormatter.timeZone = inputTimeZone as TimeZone!
    inputDateFormatter.dateFormat = dateFormat
    let date = inputDateFormatter.date(from: time)
    let outputTimeZone = NSTimeZone.local
    let outputDateFormatter = DateFormatter()
    outputDateFormatter.timeZone = outputTimeZone
    outputDateFormatter.dateFormat = dateFormat
    let outputString = outputDateFormatter.string(from: date!)
    return outputDateFormatter.date(from: outputString)! as NSDate
}

func GetAPIToken() -> String {
    if(UserDefaults.standard.string(forKey: kToken) == nil) {
        return ""
    } else {
        return UserDefaults.standard.string(forKey: kToken)!
    }
}
func GetSecretLogID() -> String {
    if(UserDefaults.standard.string(forKey: kSecret_log_id) == nil)
    {
        return ""
    }
    else
    {
        return UserDefaults.standard.string(forKey: kSecret_log_id)!
    }
}
func saveInUserDefault(value: String, key: String) {
    UserDefaults.standard.set(value, forKey: key)
    UserDefaults.standard.synchronize()
}
func compressImage(image:UIImage) -> NSData
{
    var compression:CGFloat!
    let maxCompression:CGFloat!
    compression = 0.9;
    maxCompression = 0.1;
    var imageData = UIImageJPEGRepresentation(image, compression)! as NSData
    while (imageData.length > 10 && compression > maxCompression)
    {
        compression = compression - 0.10;
        imageData = UIImageJPEGRepresentation(image, compression)! as NSData
    }
    return imageData
}

func getReportStatus(dict: NSDictionary, needOnlyStatus: Bool) -> String {
    let arrStatus = dict.object(forKey: "report_que_ans") as! NSArray
    let predict = NSPredicate(format: "user_id = %@", AppUtilities.sharedInstance.getUserID())
    let arrTmp = arrStatus.filtered(using: predict) as NSArray
    if arrTmp.count > 0 {
        let dictData = arrTmp.object(at: 0) as! NSDictionary
        let strStatus = dictData.object(forKey: "request_status") as! String
        if needOnlyStatus {
            return strStatus
        } else {
            let strDelType = dict.object(forKey: "delivery_type") as! String
            if strDelType == DELIVERY_TYPE.CONTACTME.rawValue {
                if strStatus == CONTACTME_TYPE.PENDING.rawValue {
                    return "In Progress"
                } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue {
                    return "Verified"
                } else if strStatus == CONTACTME_TYPE.REJECT.rawValue {
                    return "Rejected"
                } else if strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                    return "Complete"
                }
            } else if strDelType == DELIVERY_TYPE.POPSTATION.rawValue {
                if strStatus == POPSTATION_TYPE.PROCESSING.rawValue {
                    return "In Progress"
                } else if strStatus == POPSTATION_TYPE.WAIT_FOR_ITEM_SUBMISSION.rawValue {
                    return "Wait for item submission"
                } else if strStatus == POPSTATION_TYPE.SUBMITED.rawValue {
                    return "Submited"
                } else if strStatus == POPSTATION_TYPE.VERIFY_PENDING.rawValue {
                    return "Verified" //"Verification Pending"
                } else if strStatus == POPSTATION_TYPE.REJECTED.rawValue {
                    return "Rejected"
                } else if strStatus == POPSTATION_TYPE.COMPLETE.rawValue {
                    return "Complete"
                }
            } else if strDelType == DELIVERY_TYPE.COURIER.rawValue {
                if strStatus == COURIER_TYPE.PROCESSING.rawValue {
                    return "In Progress"
                } else if strStatus == COURIER_TYPE.VERIFYED_DETAILS.rawValue {
                    return "Verified Courier Details"
                } else if strStatus == COURIER_TYPE.PROCESSING_COLCTN.rawValue {
                    return "Processing for collection"
                } else if strStatus == COURIER_TYPE.SUBMITED.rawValue {
                    return "Submited"
                } else if strStatus == COURIER_TYPE.REJECT.rawValue {
                    return "Rejected"
                } else if strStatus == COURIER_TYPE.CONFIRM.rawValue {
                    return "Complete"
                }
            }
        }
    }
    if needOnlyStatus {
        return ""
    } else {
        return "In Progress"
    }
}

func getOriginalUserReportStatus(dict: NSDictionary, needOnlyStatus: Bool) -> String {
    
    let strStatus = dict.object(forKey: "status") as! String
    if needOnlyStatus {
        return strStatus
    } else {
        let strDelType = dict.object(forKey: "delivery_type") as! String
        if strDelType == DELIVERY_TYPE.CONTACTME.rawValue {
            if strStatus == CONTACTME_TYPE.PENDING.rawValue {
                return "In Progress"
            } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue {
                return "Verified"
            } else if strStatus == CONTACTME_TYPE.REJECT.rawValue {
                return "Rejected"
            } else if strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
                return "Complete"
            }
        } else if strDelType == DELIVERY_TYPE.POPSTATION.rawValue {
            if strStatus == POPSTATION_TYPE.PROCESSING.rawValue {
                return "In Progress"
            } else if strStatus == POPSTATION_TYPE.WAIT_FOR_ITEM_SUBMISSION.rawValue {
                return "Wait for item submission"
            } else if strStatus == POPSTATION_TYPE.SUBMITED.rawValue {
                return "Submited"
            } else if strStatus == POPSTATION_TYPE.VERIFY_PENDING.rawValue {
                return "Verified" //"Verification Pending"
            } else if strStatus == POPSTATION_TYPE.REJECTED.rawValue {
                return "Rejected"
            } else if strStatus == POPSTATION_TYPE.COMPLETE.rawValue {
                return "Complete"
            }
        } else if strDelType == DELIVERY_TYPE.COURIER.rawValue {
            if strStatus == COURIER_TYPE.PROCESSING.rawValue {
                return "In Progress"
            } else if strStatus == COURIER_TYPE.VERIFYED_DETAILS.rawValue {
                return "Verified Courier Details"
            } else if strStatus == COURIER_TYPE.PROCESSING_COLCTN.rawValue {
                return "Processing for collection"
            } else if strStatus == COURIER_TYPE.SUBMITED.rawValue {
                return "Submited"
            } else if strStatus == COURIER_TYPE.REJECT.rawValue {
                return "Rejected"
            } else if strStatus == COURIER_TYPE.CONFIRM.rawValue {
                return "Complete"
            }
        }
    }
    
    if needOnlyStatus {
        return ""
    } else {
        return "In Progress"
    }
}

func getStatusForAnsweredReport(dictReport: NSDictionary) -> String {
    let strStatus = getReportStatus(dict: dictReport, needOnlyStatus: true)
    if strStatus.count > 0 {
        if strStatus == CONTACTME_TYPE.PENDING.rawValue {
            return "In Progress"
        } else if strStatus == CONTACTME_TYPE.VERIFIED.rawValue {
            return "Verified"
        } else if strStatus == CONTACTME_TYPE.REJECT.rawValue {
            return "Rejected"
        } else if strStatus == CONTACTME_TYPE.COMPLETE.rawValue || strStatus == CONTACTME_TYPE.COMPLETE_SECOND.rawValue {
            return "Complete"
        } else {
            return "In Progress"
        }
    } else {
        return "In Progress"
    }
}

public func convert(arrData:NSMutableArray,toString:(_ strData:String)->())
{
    let jsonData = try? JSONSerialization.data(withJSONObject: arrData, options: [])
    let jsonString = String(data: jsonData!, encoding: .utf8)
    let newString = (jsonString! as NSString).replacingOccurrences(of: "\\", with: "/")
    toString(newString)
}

public func convertDictionaryToJSONString(dic:NSDictionary) -> String? {
    do{
        let jsonData: Data? = try JSONSerialization.data(withJSONObject: dic, options: [])
        var myString: String? = nil
        if let aData = jsonData {
            myString = String(data: aData, encoding: .utf8)
        }
        return myString
    }catch{
        print(error)
    }
    return ""
}

func setCursorColor() {
    var color = UIColor()
    if AppUtilities.sharedInstance.userType == SEEKER {
        color = REDCOLOR
    } else {
        color = BLUECOLOR
    }
    UITextField.appearance().tintColor = color
    UITextView.appearance().tintColor = color
}

func getThumbnailImage(forUrl url: URL) -> UIImage? {
    let asset: AVAsset = AVAsset(url: url)
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    imageGenerator.appliesPreferredTrackTransform = true
    do {
        let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
        return UIImage(cgImage: thumbnailImage)
    } catch let error {
        print(error)
    }
    
    return nil
}

func setNoDataLabel(tableView:UITableView, array:NSMutableArray, text:String) -> Int
{
    var numOfSections = 0
    
    if array.count != 0 {
        tableView.backgroundView = nil
        numOfSections = 1
    } else {
        let noDataLabel = UILabel()
        noDataLabel.frame = CGRect(x: 10, y: 0, width: tableView.frame.size.width-20, height: tableView.frame.size.height)
        noDataLabel.text = text
        noDataLabel.numberOfLines = 0
        noDataLabel.textColor = UIColor.lightGray.withAlphaComponent(0.5)
        noDataLabel.textAlignment = NSTextAlignment.center
        tableView.backgroundView = noDataLabel
        tableView.separatorStyle = .none
    }
    
    return numOfSections
}
func randomString() -> String
{
    var text = ""
    text = text.appending(CurrentTimeStamp)
    text = text.replacingOccurrences(of: ".", with: "")
    return text
}

func cleanUserDefaultData() {
    /*
    AppUtilities.sharedInstance.userType = FINDER
    AppUtilities.sharedInstance.saveData(data: AppUtilities.sharedInstance.userType as AnyObject, key: kUserType)
    setCursorColor()*/
    APP_DELEGATE.totalUnreadCnt = 0
    UserDefaultManager.removeCustomObject(key: UD_UserData)
    UserDefaultManager.removeCustomObject(key: UD_IsLoggedIn)
    UserDefaultManager.removeCustomObject(key: UD_UserId)
    UserDefaultManager.removeCustomObject(key: UD_UserProfileURL)
    UserDefaultManager.removeCustomObject(key: UD_UserEmail)
}
func dictionaryOfFilteredBy(dict: NSDictionary) -> NSDictionary {
    
    let replaced: NSMutableDictionary = NSMutableDictionary(dictionary : dict)
    let blank: String = ""
    
    for (key, _) in dict
    {
        let object = dict[key] as AnyObject
        
        if (object.isKind(of: NSNull.self))
        {
            replaced[key] = blank as AnyObject?
        }
        else if (object is [AnyHashable: AnyObject])
        {
            replaced[key] = dictionaryOfFilteredBy(dict: object as! NSDictionary)
        }
        else if (object is [AnyObject])
        {
            replaced[key] = arrayOfFilteredBy(arr: object as! NSArray)
        }
        else
        {
            replaced[key] = "\(object)" as AnyObject?
        }
    }
    return replaced
}

func arrayOfFilteredBy(arr: NSArray) -> NSArray {
    
    let replaced: NSMutableArray = NSMutableArray(array: arr)
    let blank: String = ""
    
    for i in 0..<arr.count
    {
        let object = arr[i] as AnyObject
        
        if (object.isKind(of: NSNull.self))
        {
            replaced[i] = blank as AnyObject
        }
        else if (object is [AnyHashable: AnyObject])
        {
            replaced[i] = dictionaryOfFilteredBy(dict: arr[i] as! NSDictionary)
        }
        else if (object is [AnyObject])
        {
            replaced[i] = arrayOfFilteredBy(arr: arr[i] as! NSArray)
        }
        else
        {
            replaced[i] = "\(object)" as AnyObject
        }
        
    }
    return replaced
}

//MARK:- Helper
/*
public func TableEmptyMessage(modulename:String, tbl:UITableView)
{
    let uiview = UIView(frame: Frame_XYWH(0, 0, tbl.frame.size.width, tbl.frame.size.height))
    let messageLabel = UILabel(frame: Frame_XYWH(0, 0, tbl.frame.size.width, 50))
    messageLabel.font = UIFont.init(name: FT_Light, size: 15)
    messageLabel.text = "\("No " + modulename + " Available.")"
    messageLabel.textColor = UIColor.lightGray
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = .center;
    if(modulename.count > 0)
    {
        messageLabel.setViewBottomBorder(borderColor: UIColor.lightGray)
    }
    uiview.addSubview(messageLabel)
    tbl.backgroundView = uiview;
    //tbl.separatorStyle = .singleLine;
}*/
func checkSearchBarActive(searchFriends:UISearchBar) -> Bool
{
    if searchFriends.isFirstResponder && searchFriends.text != "" {
        return true
    }
    else if(searchFriends.text != "")
    {
        return true
    }
    else {
        return false
    }
}
//MARK:-  Check Device is iPad or not
public  var isPad: Bool {
    return UIDevice.current.userInterfaceIdiom == .pad
}

public var isPhone: Bool {
    return UIDevice.current.userInterfaceIdiom == .phone
}

public var isStatusBarHidden: Bool
{
    get {
        return UIApplication.shared.isStatusBarHidden
    }
    set {
        UIApplication.shared.isStatusBarHidden = newValue
    }
}

public var mostTopViewController: UIViewController? {
    get {
        return UIApplication.shared.keyWindow?.rootViewController
    }
    set {
        UIApplication.shared.keyWindow?.rootViewController = newValue
    }
}

//MARK:- iOS version checking Functions
public var appDisplayName: String? {
    return Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
}
public var appBundleID: String? {
    return Bundle.main.bundleIdentifier
}
public func IOS_VERSION() -> String {
    return UIDevice.current.systemVersion
}
public var statusBarHeight: CGFloat {
    return UIApplication.shared.statusBarFrame.height
}
public var applicationIconBadgeNumber: Int {
    get {
        return UIApplication.shared.applicationIconBadgeNumber
    }
    set {
        UIApplication.shared.applicationIconBadgeNumber = newValue
    }
}
public var appVersion: String? {
    return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
}
public func SCREENWIDTH() -> CGFloat
{
    let screenSize = UIScreen.main.bounds
    return screenSize.width
}

public func SCREENHEIGHT() -> CGFloat
{
    let screenSize = UIScreen.main.bounds
    return screenSize.height
}

func getVideoThumbnail(videoURL:URL,withSeconds:Bool = false) -> UIImage?
{
    let timeSeconds = 2
    
    let asset = AVAsset(url: videoURL)
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    imageGenerator.appliesPreferredTrackTransform = true
    
    var time = asset.duration
    
    if(withSeconds)
    {
        time.value = min(time.value, CMTimeValue(timeSeconds))
    }
    else
    {
        time = CMTimeMultiplyByFloat64(time, 0.5)
    }
    
    do {
        let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
        return UIImage(cgImage: imageRef)
    }
    catch _ as NSError
    {
        return nil
    }
}

//MARK:- SwiftMessage
public func showLoaderHUD(strMessage:String)
{
    let activityData = ActivityData()
    NVActivityIndicatorView.DEFAULT_TYPE = .ballPulse
    NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
    NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE = ""
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
//    overlayView.frame = (mostTopViewController?.view)!.frame
//    overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//    spinner = RPLoadingAnimationView(
//        frame: CGRect(origin: CGPoint.zero, size: (mostTopViewController?.view)!.bounds.size),
//        type: .rotatingCircle,
//        colors: loadercolor,
//        size: CGSize.init(width: 200.0, height: 200.0)
//    )
//    overlayView.addSubview(spinner)
//    (mostTopViewController?.view)!.addSubview(overlayView)
//    spinner.setupAnimation()
}

public func hideLoaderHUD()
{
    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    spinner.removeFromSuperview()
    overlayView.removeFromSuperview()
}

public func hideMessage()
{
    SwiftMessages.hide()
}

public func showMessageWithRetry(_ bodymsg:String ,_ msgtype:Int,buttonTapHandler: ((_ button: UIButton) -> Void)?)
{
    hideBanner()
    let view: MessageView  = try! SwiftMessages.viewFromNib()
    view.configureContent(title: "", body: bodymsg, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Retry", buttonTapHandler: buttonTapHandler)
    view.configureDropShadow()
    var config = SwiftMessages.defaultConfig
    config.presentationContext = .window(windowLevel: UIWindowLevelNormal)
    config.duration = .seconds(seconds: 7)
    view.configureTheme(.warning, iconStyle:  .light)
    view.titleLabel?.isHidden = true
    view.button?.isHidden = false
    view.iconImageView?.isHidden = false
    view.iconLabel?.isHidden = false
    SwiftMessages.show(config: config, view: view)
}

public func showMessage(_ bodymsg:String)
{
    let view: MessageView  = try! SwiftMessages.viewFromNib()
    view.configureContent(title: "", body: bodymsg, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Hide", buttonTapHandler: { _ in SwiftMessages.hide() })
    view.configureDropShadow()
    var config = SwiftMessages.defaultConfig
    config.presentationContext = .window(windowLevel: UIWindowLevelNormal)
    config.duration = .seconds(seconds: 2)
    view.configureTheme(.warning, iconStyle:  .light)
    view.titleLabel?.isHidden = true
    view.button?.isHidden = true
    view.iconImageView?.isHidden = true
    view.iconLabel?.isHidden = true
    SwiftMessages.show(config: config, view: view)
}

//MARK:- Network indicator
public func ShowNetworkIndicator(xx :Bool)
{
    runOnMainThreadWithoutDeadlock {
        UIApplication.shared.isNetworkActivityIndicatorVisible = xx
    }
}

//MARK : Length validation
public func TRIM(string: Any) -> String
{
    return (string as AnyObject).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
}

public func validateAmount(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if Double(txtVal.text!)! <= 0
    {
        txtVal.shake()
        showMessage(msg);
        return false
    }
    return true
}

public func compareAmount(_ txtVal: UITextField, amount: String, withMessage msg: String) -> Bool {
    if Double(txtVal.text!)! > Double(amount)!
    {
        txtVal.shake()
        showMessage(msg);
        return false
    }
    return true
}

public func validateTxtFieldLength(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count == 0
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg);
        return false
    }
    return true
}

public func validateTxtViewLength(_ txtVal: UITextView, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count == 0
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateTerms(_ terms: Bool, withMessage msg: String) -> Bool
{
    if terms == false
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validateMinTxtFieldLength(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count < 6
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg);
        return false
    }
    print(">6 length")
    return true
}

public func validateMaxTxtFieldLength(_ txtVal: UITextField, lenght:Int,msg: String) -> Bool
{
    if TRIM(string: txtVal.text ?? "").count > lenght
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validateTxtLength(_ txtVal: String, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal).count == 0
    {
        showMessage(msg);
        return false
    }
    return true
}

public func passwordMismatch(_ txtVal: UITextField, _ txtVal1: UITextField, withMessage msg: String) -> Bool
{
    if TRIM(string: txtVal.text ?? "") != TRIM(string: txtVal1.text ?? "")
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validateEmailAddress(_ txtVal: UITextField ,withMessage msg: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    if(emailTest.evaluate(with: txtVal.text) != true)
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validatePhoneNo(_ txtVal: UITextField ,withMessage msg: String) -> Bool
{
    let PHONE_REGEX = "^[0-9]{6,}$"
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    if(phoneTest.evaluate(with: txtVal.text) != true)
    {
        showMessage(msg);
        return false
    }
    return true
}

public func isBase64(stringBase64:String) -> Bool
{
    let regex = "([A-Za-z0-9+/]{4})*" + "([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)"
    let test = NSPredicate(format:"SELF MATCHES %@", regex)
    if(test.evaluate(with: stringBase64) != true)
    {
        return false
    }
    return true
}

public func validateImage(_ txtVal: String, withMessage msg: String) -> Bool {
    if txtVal == "0"
    {
       showMessage(msg);
        return false
    }
    return true
}

public func validateButtonLabel(_ txtVal: String,btnname:UIButton, withMessage msg: String) -> Bool {
    if btnname.titleLabel?.text == txtVal
    {
        showMessage(msg);
        return false
    }
    return true
}

//MARK:- - Get image from image name
public func Set_Local_Image(imageName :String) -> UIImage
{
    return UIImage(named:imageName)!
}

//MARK:- FONT
public func FontWithSize(_ fname: String,_ fsize: Int) -> UIFont
{
    return UIFont(name: fname, size: CGFloat(fsize))!
}

//MARK:- COLOR RGB
public func Color_RGBA(_ R: Int,_ G: Int,_ B: Int,_ A: Int) -> UIColor
{
    return UIColor(red: CGFloat(R)/255.0, green: CGFloat(G)/255.0, blue: CGFloat(B)/255.0, alpha :CGFloat(A))
}
public func RGBA(_ R: Int,_ G: Int,_ B: Int,_ A: CGFloat) -> UIColor
{
    return UIColor(red: CGFloat(R)/255.0, green: CGFloat(G)/255.0, blue: CGFloat(B)/255.0, alpha :A)
}

//MARK:- SET FRAME
public func Frame_XYWH(_ originx: CGFloat,_ originy: CGFloat,_ fwidth: CGFloat,_ fheight: CGFloat) -> CGRect
{
    return CGRect(x: originx, y:originy, width: fwidth, height: fheight)
}

public func randomColor() -> UIColor {
    let r: UInt32 = arc4random_uniform(255)
    let g: UInt32 = arc4random_uniform(255)
    let b: UInt32 = arc4random_uniform(255)
    
    return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1.0)
}

struct Platform
{
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}

//MARK:- - Log trace
public func DLog<T>(message:T,  file: String = #file, function: String = #function, lineNumber: Int = #line ) {
    #if DEBUG
        if let text = message as? String {
            
            print("\((file as NSString).lastPathComponent) -> \(function) line: \(lineNumber): \(text)")
        }
    #endif
}

//Mark : string to dictionary
public func convertStringToDictionary(str:String) -> [String: Any]? {
    if let data = str.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

//MARK:- - Check string is available or not
public func isLike(source: String , compare: String) ->Bool
{
    var exists = true
    ((source).lowercased().range(of: compare) != nil) ? (exists = true) :  (exists = false)
    return exists
}

//MARK:- - Calculate heght of label
public func calculatedHeight(string :String,withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat
{
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = string.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
    return boundingBox.height
}

public func calculatedWidth(string :String,withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat
{
    let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingBox = string.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
    return boundingBox.width
}

//MARK:- Mile to Km
public func hideBanner()
{
    SwiftMessages.hide()
}

public func mileToKilometer(myDistance : Int) -> Float
{
    return Float(myDistance) * 1.60934
}

//MARK:- Kilometer to Mile
public func KilometerToMile(myDistance : Double) -> Double {
    return (myDistance) * 0.621371192
}

//MARK:- NULL to NIL
public func NULL_TO_NIL(value : AnyObject?) -> AnyObject? {
    
    if value is NSNull {
        return "" as AnyObject?
    } else {
        return value
    }
}

//MARK:- Time Ago Function
func timeAgoSinceDate(date:Date, numericDates:Bool) -> String
{
    let calendar = NSCalendar.current
    let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
    let now = NSDate()
    let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now as Date, options: [])
    if (components.year! >= 2)
    {
        return "\(components.year!)" + "y ago"
    }
    else if (components.year! >= 1)
    {
        if (numericDates){
            return "1y ago"
        } else {
            return "Last year"
        }
    }
    else if (components.month! >= 2) {
        return "\(components.month!)" + "m ago"
    }
    else if (components.month! >= 1){
        if (numericDates){
            return "1m ago"
        } else {
            return "Last month"
        }
    }
    else if (components.weekOfYear! >= 2) {
        return "\(components.weekOfYear!)" + "w ago"
    }
    else if (components.weekOfYear! >= 1){
        if (numericDates){
            return "1w ago"
        } else {
            return "Last week"
        }
    }
    else if (components.day! >= 2) {
        return "\(components.day!)" + "d ago"
    }
    else if (components.day! >= 1){
        if (numericDates){
            return "1d ago"
        } else {
            return "Yesterday"
        }
    }
    else if (components.hour! >= 2) {
        return "\(components.hour!)" + "h ago"
    }
    else if (components.hour! >= 1){
        if (numericDates){
            return "1h ago"
        } else {
            return "An hour ago"
        }
    }
    else if (components.minute! >= 2)
    {
        return "\(components.minute!)" + "m ago"
    } else if (components.minute! > 1){
        if (numericDates){
            return "1m ago"
        } else {
            return "A minute ago"
        }
    }
    else if (components.second! >= 3) {
        return "\(components.second!)" + "s ago"
    } else {
        return "Just now"
    }
}


//MARK:-Check Internet connection
func isConnectedToNetwork() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    })
    else
    {
        return false
    }
    
    var flags : SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    let available =  (isReachable && !needsConnection)
    if(available)
    {
        return true
    }
    else
    {
        showMessage(InternetNotAvailable)
        return false
    }
}

//MARK:- Animation
func addActivityIndicatior(activityview:UIActivityIndicatorView,button:UIButton)
{
    activityview.isHidden = false
    activityview.startAnimating()
     button.isEnabled = false
    button.backgroundColor = RGBA(181, 131, 0, 0.4)
}
func hideActivityIndicatior(activityview:UIActivityIndicatorView,button:UIButton)
{
    activityview.isHidden = true
    activityview.stopAnimating()
    button.isEnabled = true
    button.backgroundColor = RGBA(181, 131, 0, 1.0)
}
func animateview(vw1 : UIView,vw2:UIView)
{
    UIView.animate(withDuration: 0.1,
                   delay: 0.1,
                   options: UIViewAnimationOptions.curveEaseIn,
                   animations: { () -> Void in
                    vw1.alpha = 0;
                    vw2.alpha = 1;
    }, completion: { (finished) -> Void in
        vw1.isHidden = true;
    })
}

//MARK:- Country code
func setDefaultCountryCode() -> String
{
    let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String
    return "+" + getCountryPhonceCode(countryCode!)
}

func fixOrientationOfImage(image: UIImage) -> UIImage?
{
    if image.imageOrientation == .up
    {return image}
    var transform = CGAffineTransform.identity
    switch image.imageOrientation
    {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: image.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.rotated(by:  CGFloat(Double.pi / 2))
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: image.size.height)
            transform = transform.rotated(by:  -CGFloat(Double.pi / 2))
        default:
            break
    }
    switch image.imageOrientation
    {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: image.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        default:
            break
    }
    guard let context = CGContext(data: nil, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: image.cgImage!.bitsPerComponent, bytesPerRow: 0, space: image.cgImage!.colorSpace!, bitmapInfo: image.cgImage!.bitmapInfo.rawValue) else {
        return nil
    }
    context.concatenate(transform)
    switch image.imageOrientation
    {
    case .left, .leftMirrored, .right, .rightMirrored:
        context.draw(image.cgImage!, in: CGRect(x: 0, y: 0, width: image.size.height, height: image.size.width))
    default:
        context.draw(image.cgImage!, in: CGRect(origin: .zero, size: image.size))
    }
    guard let CGImage = context.makeImage() else {
        return nil
    }
    return UIImage(cgImage: CGImage)
}
func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ session: AVAssetExportSession)-> Void)
{
    let urlAsset = AVURLAsset(url: inputURL, options: nil)
    let exportSession = AVAssetExportSession(asset: urlAsset, presetName:AVAssetExportPreset640x480)//AVAssetExportPresetMediumQuality
    exportSession!.outputURL = outputURL
    exportSession!.outputFileType = AVFileType.mp4
    exportSession!.shouldOptimizeForNetworkUse = true
    exportSession!.exportAsynchronously { () -> Void in
        handler(exportSession!)
    }
}
func getCountryPhonceCode (_ country : String) -> String
{
    var countryDictionary  = ["AF":"93",
                              "AL":"355",
                              "DZ":"213",
                              "AS":"1",
                              "AD":"376",
                              "AO":"244",
                              "AI":"1",
                              "AG":"1",
                              "AR":"54",
                              "AM":"374",
                              "AW":"297",
                              "AU":"61",
                              "AT":"43",
                              "AZ":"994",
                              "BS":"1",
                              "BH":"973",
                              "BD":"880",
                              "BB":"1",
                              "BY":"375",
                              "BE":"32",
                              "BZ":"501",
                              "BJ":"229",
                              "BM":"1",
                              "BT":"975",
                              "BA":"387",
                              "BW":"267",
                              "BR":"55",
                              "IO":"246",
                              "BG":"359",
                              "BF":"226",
                              "BI":"257",
                              "KH":"855",
                              "CM":"237",
                              "CA":"1",
                              "CV":"238",
                              "KY":"345",
                              "CF":"236",
                              "TD":"235",
                              "CL":"56",
                              "CN":"86",
                              "CX":"61",
                              "CO":"57",
                              "KM":"269",
                              "CG":"242",
                              "CK":"682",
                              "CR":"506",
                              "HR":"385",
                              "CU":"53",
                              "CY":"537",
                              "CZ":"420",
                              "DK":"45",
                              "DJ":"253",
                              "DM":"1",
                              "DO":"1",
                              "EC":"593",
                              "EG":"20",
                              "SV":"503",
                              "GQ":"240",
                              "ER":"291",
                              "EE":"372",
                              "ET":"251",
                              "FO":"298",
                              "FJ":"679",
                              "FI":"358",
                              "FR":"33",
                              "GF":"594",
                              "PF":"689",
                              "GA":"241",
                              "GM":"220",
                              "GE":"995",
                              "DE":"49",
                              "GH":"233",
                              "GI":"350",
                              "GR":"30",
                              "GL":"299",
                              "GD":"1",
                              "GP":"590",
                              "GU":"1",
                              "GT":"502",
                              "GN":"224",
                              "GW":"245",
                              "GY":"595",
                              "HT":"509",
                              "HN":"504",
                              "HU":"36",
                              "IS":"354",
                              "IN":"91",
                              "ID":"62",
                              "IQ":"964",
                              "IE":"353",
                              "IL":"972",
                              "IT":"39",
                              "JM":"1",
                              "JP":"81",
                              "JO":"962",
                              "KZ":"77",
                              "KE":"254",
                              "KI":"686",
                              "KW":"965",
                              "KG":"996",
                              "LV":"371",
                              "LB":"961",
                              "LS":"266",
                              "LR":"231",
                              "LI":"423",
                              "LT":"370",
                              "LU":"352",
                              "MG":"261",
                              "MW":"265",
                              "MY":"60",
                              "MV":"960",
                              "ML":"223",
                              "MT":"356",
                              "MH":"692",
                              "MQ":"596",
                              "MR":"222",
                              "MU":"230",
                              "YT":"262",
                              "MX":"52",
                              "MC":"377",
                              "MN":"976",
                              "ME":"382",
                              "MS":"1",
                              "MA":"212",
                              "MM":"95",
                              "NA":"264",
                              "NR":"674",
                              "NP":"977",
                              "NL":"31",
                              "AN":"599",
                              "NC":"687",
                              "NZ":"64",
                              "NI":"505",
                              "NE":"227",
                              "NG":"234",
                              "NU":"683",
                              "NF":"672",
                              "MP":"1",
                              "NO":"47",
                              "OM":"968",
                              "PK":"92",
                              "PW":"680",
                              "PA":"507",
                              "PG":"675",
                              "PY":"595",
                              "PE":"51",
                              "PH":"63",
                              "PL":"48",
                              "PT":"351",
                              "PR":"1",
                              "QA":"974",
                              "RO":"40",
                              "RW":"250",
                              "WS":"685",
                              "SM":"378",
                              "SA":"966",
                              "SN":"221",
                              "RS":"381",
                              "SC":"248",
                              "SL":"232",
                              "SG":"65",
                              "SK":"421",
                              "SI":"386",
                              "SB":"677",
                              "ZA":"27",
                              "GS":"500",
                              "ES":"34",
                              "LK":"94",
                              "SD":"249",
                              "SR":"597",
                              "SZ":"268",
                              "SE":"46",
                              "CH":"41",
                              "TJ":"992",
                              "TH":"66",
                              "TG":"228",
                              "TK":"690",
                              "TO":"676",
                              "TT":"1",
                              "TN":"216",
                              "TR":"90",
                              "TM":"993",
                              "TC":"1",
                              "TV":"688",
                              "UG":"256",
                              "UA":"380",
                              "AE":"971",
                              "GB":"44",
                              "US":"1",
                              "UY":"598",
                              "UZ":"998",
                              "VU":"678",
                              "WF":"681",
                              "YE":"967",
                              "ZM":"260",
                              "ZW":"263",
                              "BO":"591",
                              "BN":"673",
                              "CC":"61",
                              "CD":"243",
                              "CI":"225",
                              "FK":"500",
                              "GG":"44",
                              "VA":"379",
                              "HK":"852",
                              "IR":"98",
                              "IM":"44",
                              "JE":"44",
                              "KP":"850",
                              "KR":"82",
                              "LA":"856",
                              "LY":"218",
                              "MO":"853",
                              "MK":"389",
                              "FM":"691",
                              "MD":"373",
                              "MZ":"258",
                              "PS":"970",
                              "PN":"872",
                              "RE":"262",
                              "RU":"7",
                              "BL":"590",
                              "SH":"290",
                              "KN":"1",
                              "LC":"1",
                              "MF":"590",
                              "PM":"508",
                              "VC":"1",
                              "ST":"239",
                              "SO":"252",
                              "SJ":"47",
                              "SY":"963",
                              "TW":"886",
                              "TZ":"255",
                              "TL":"670",
                              "VE":"58",
                              "VN":"84",
                              "VG":"284",
                              "VI":"340"]
    let cname = country.uppercased()
    if countryDictionary[cname] != nil
    {
        return countryDictionary[cname]!
    }
    else
    {
        return cname
    }
    //        let myfrndObj = CoreDBManager.sharedDatabase.getMyFriendsStory(userid: UserDefaultManager.getStringFromUserDefaults(key: UD_UserId))
    //   let objNCountSorted = myfrndObj.sorted (by: {$0.story_id! > $1.story_id!})
    //        let objNCountSorted = myfrndObj.sorted(by: { $0.created_date?.compare($1.created_date!) == .orderedDescending })
    //        let groupedUser = objNCountSorted.filterDuplicate { ($0.user_id)} as NSArray
}

func Color_Hex(hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
func viewSlideInFromBottom(toTop views: UIView)
{
    let transition = CATransition()
    transition.duration = 0.8
    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    transition.type = kCATransitionPush
    transition.subtype = kCATransitionFromTop
    views.layer.add(transition, forKey: nil)
}
func viewSlideInFromTop(toBottom views: UIView)
{
    let transition = CATransition()
    transition.duration = 0.5
    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    transition.type = kCATransitionPush
    transition.subtype = kCATransitionFromBottom
    views.layer.add(transition, forKey: nil)
}

func sendUploadIdProof(image: UIImage,
                       strIsIDProof: String,
                       responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void) {
    
    showLoaderHUD(strMessage: "")
    let dictParam: NSDictionary = ["id":AppUtilities.sharedInstance.getUserID(),
                                   "is_id_proof":strIsIDProof]
    
    HttpRequestManager.sharedInstance.requestWithPostMultipartParam(
        endpointurl: Server_URL,
        service:APIUploadIdProof,
        parameters: dictParam,
        image: image,
        imageParam: "id_proof",
        showLoader:false)
    {(error,responseDict) -> Void in
        hideMessage()
        if error != nil
        {
            responseData(error! as NSError,nil)
        }
        else
        {
            if responseDict!["success"] as? String == "1"
            {
                let dictUserInfo: NSMutableDictionary = (UserDefaults.standard.object(forKey: UD_UserData) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                dictUserInfo.setObject("2", forKey: "is_id_submited" as NSCopying)
                AppUtilities.sharedInstance.saveData(data: dictUserInfo.copy() as! NSDictionary, key: UD_UserData)
            }
            responseData(nil,responseDict)
        }
    }
}


func sendUpdateStatus(dictParam: NSDictionary,
                      responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void) {
    HttpRequestManager.sharedInstance.requestWithPostJsonParam(
        endpointurl: Server_URL,
        service:APIUpdateStatus,
        parameters: dictParam,
        showLoader:true)
    {(error,responseDict) -> Void in
        hideMessage()
        if error != nil
        {
            responseData(error! as NSError,nil)
        }
        else
        {
            print(responseDict ?? "Not Found")
            responseData(nil,responseDict)
        }
    }
}

func sendGetUnreadNoti() {
    let dictParam: NSDictionary = ["user_id":AppUtilities.sharedInstance.getUserID()]
    HttpRequestManager.sharedInstance.requestWithPostJsonParam(
        endpointurl: Server_URL,
        service: APIGetUnreadCount,
        parameters: dictParam,
        showLoader: false)
    {(error,responseDict) -> Void in
        hideMessage()
        if error == nil
        {
            if responseDict!["success"] as? String == "1"
            {
                let dict = responseDict?.object(forKey: "data") as! NSDictionary
                print(dict.object(forKey: "unread_count") as! String)
                noti_unread_count = Int(dict.object(forKey: "unread_count") as? String ?? "0")!
                NotificationCenter.default.post(name: Notification.Name("updateUnreadCount"), object: nil)
            }
        }
    }
}

func sendReadNoti(strID: String) {
    let dictParam: NSDictionary = ["user_id":AppUtilities.sharedInstance.getUserID(),
                                   "id":strID]
    HttpRequestManager.sharedInstance.requestWithPostJsonParam(
        endpointurl: Server_URL,
        service: APIReadNotification,
        parameters: dictParam,
        showLoader: false)
    {(error,responseDict) -> Void in
        hideMessage()
        if error == nil
        {
            print(responseDict ?? "")
            if responseDict!["success"] as? String == "1"
            {
                noti_unread_count -= 1
                if noti_unread_count < 0 { noti_unread_count = 0 }
                NotificationCenter.default.post(name: Notification.Name("updateUnreadCount"), object: nil)
                sendGetUnreadNoti()
            }
        }
    }
}

//MARK:- DeepLinking

func shareContent_DeepLinking(dicMess : NSDictionary) -> BranchUniversalObject {
    let branchObj : BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "itemID")
    branchObj.title = "\(APPNAME)"
    branchObj.contentDescription = "\(APPNAME)"
    let strDicValue = convertDictionaryToJSONString(dic: dicMess)
    branchObj.contentMetadata.customMetadata["data"] = strDicValue ?? " "
    branchObj.contentMetadata.customMetadata["linktype"] = "report"
    
    return branchObj
}

func get_BranchLinkProperties() -> BranchLinkProperties {
    let linkProperties : BranchLinkProperties = BranchLinkProperties()
    return linkProperties
}
