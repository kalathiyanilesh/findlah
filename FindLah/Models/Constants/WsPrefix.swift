//
//  Constants.swift
//
//  Created by C237 on 30/10/17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

let GET = "GET"
let POST = "POST"
let MEDIA = "MEDIA"

let DEFAULT_TIMEOUT:TimeInterval = 60.0

let Server_URL = "http://18.216.113.43/lost_found/web_services/service.php?op="
//let Server_URL = SERVICE_URL + "WebService/service"
//let Upload_User_IMG = SERVICE_URL + "upload/upload_user_image"
//let Upload_IMG = SERVICE_URL + "upload/upload_image"
//let Profile_Pic_URL = ""
//let Get_Profile_Pic_URL = ""

//MARK: Response Keys
let kData = "data"
let kMessage = "message"
let kSuccess = "success"
let kToken = "token"
let kCode = "code"
let kSecret_log_id = "secret_log_id"

//MARK - API MESSAGES
let APICountryList = "Loading Countries"
let APIEmailCheck = "Checking Email Avaibility"
let APISendingCode = "Sending Verification Code"
let APIVerificationCode = "Verifying Code"
let APISignUpMessage = "REGISTERING"
let APILoginCountryMessage = "Loading Countries"
let APISocialLoginMessage = "Login In"
let APISocialLogOutMessage = "Logout"
let APIPasswordRecovery = "Requesting Password"
let APIUpdateProfileMessage = "Updating Profile Details"
let APIUpdateProfilePicMessage = "Updating Profile Pic"
let APIUpdatePasswordMessage = "Updating Password"
let APINotificationMessage = "Loading Notification"
let APIMenuMessage = "Loading Menu"

//API Services
let APICategory = "get_category"
let APISignUp = "register"
let APILogin = "login"
let APIUploadIdProof = "upload_id_proof"
let APIForgotPassword = "send_forgot_password_link"
let APIChangePassword = "change_password"
let APIEditProfile = "edit_profile"
let APIVerifyUser = "verify_mobile"
let APISendOTP = "send_otp_via_email_or_phone"
let APIGetReport = "get_report"
let APIDeleteReport = "delete_report"
let APIAddEditReport = "add_or_edit_report"
let APIGetCountReport = "get_count_report"
let APIAddReportQA = "add_report_que_ans"
let APIPayment = "send_payment"
let APIGenerateNonce = "generate_nonce"
let APIGetOtherUserProfile = "get_user_profile"
let APIAddRewardReq = "add_reward_request"
let APIGetNotification = "get_notification_list"
let APIGetPOPStation = "get_popstations"
let APIUpdateStatus = "update_status"
let APIGetReportByID = "get_report_by_id"
let APIGetUnreadCount = "get_unread_notification_count"
let APIReadNotification = "read_notification"
let APIGetBankList = "get_bank_list"
