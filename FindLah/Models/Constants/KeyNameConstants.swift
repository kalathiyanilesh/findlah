//
//  KeyNamesConstants.swift
//  ELFramework
//
//  Created by Admin on 14/12/17.
//  Copyright © 2017 EL. All rights reserved.
//

import Foundation
import UIKit

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

let loadercolor = [UIColor.white]

let APPCOLOR = Color_Hex(hex: "#0060FF")

let BLUECOLOR = Color_Hex(hex: "#0060FF") // "#0C5CFA"
let REDCOLOR = Color_Hex(hex: "#FF7676") // E62E25

//MARK:- USER TYPR

public let FINDER: String = "finder"
public let SEEKER: String = "seeker"
public let kUserType: String = "usertype"
public let kUserOTP: String = "userotp"
public let LimitItem = 10

public let UserProfilePlaceHolder = UIImage(named:"user_profile")

//MARK:- APP NAME
public let APPNAME :String = "FindLah"

//MARK:- REQUEST KEYS
public let PlatformName = "1"//"IOS"

//MARK:- STORY BOARD NAMES
public let SB_MAIN :String = "Main"
public let SB_SISU :String = "SignInSignUp"
public let SB_LOFI :String = "LostFind"


//MARK:- MESSAGE OF APP
public let kPHOTOIDNRIC = "You need to submit a photo of your ID or NRIC, front and back to authenticate your user profile in the next step."
public let kLATESTPHONEBILL = "You need to submit a photo of your latest Phone Bill Statement with your Name, Address and Mobile Number indicated clearly to authenticate your user profile in the next step."
public let kIMPNOTICE = "Important Notice"
public let kBILLINGBILL = "tellHimForBillingBill"
public let kHIMID = "tellHimForID"

//MARK:- VIEW CONTROLLER ID
public let idSignInVC = "SignInVC"
public let idSignUpVC = "SignUpVC"
public let idTermsConditionVC = "TermsConditionVC"
public let idVarifyEmailPhoneVC = "VarifyEmailPhoneVC"
public let idAddOTPVC = "AddOTPVC"
public let idSWRevealViewController = "SWRevealViewController"
public let idFoundersVC = "FoundersVC"
public let idLostersVC = "LostersVC"
public let idFoundedItemsVC = "FoundedItemsVC"
public let idNotificationVC = "NotificationVC"
public let idPaymentVC = "PaymentVC"
public let idWhoBuddyVC = "WhoBuddyVC"
public let idUserProfile = "UserProfile"
public let idQuestionVC = "QuestionVC"
public let idAskQuestionVC = "AskQuestionVC"
public let idAddFoundItemVC = "AddFoundItemVC"
public let idDetailsVC = "DetailsVC"
public let idHomeVC = "HomeVC"
public let idDisplayAlertVC = "DisplayAlertVC"
public let idAlertVC = "AlertVC"
public let idForgotPassVC = "ForgotPassVC"
public let idChangePasswordVC = "ChangePasswordVC"
public let idEditProfile = "EditProfile"
public let idLoginNav = "loginnav"
public let idRewardsVC = "RewardsVC"
public let idFinderSeekerMapVC = "FinderSeekerMapVC"
public let idFilterVC = "FilterVC"

//MARK:- Plist name
let UserPlaceholderImage:UIImage = UIImage.init(named: "ic_profile")!

//MARK:- USER DEFAULTS KEY
let UD_IsLoggedIn:String = "UDLoggedIn"
let UD_UserName:String = "UDUserName"
let UD_UserMobileNo:String = "UDMobileNo"
let UD_UserId:String = "UDUserId"
let UD_UserProfileURL:String = "UDUserProfileURL"
let UD_UserEmail:String = "UDUserEmail"
let UD_UserAddresss:String = "UDUserAddress"
let UD_UserUnitNo:String = "UDUserUnitNo"
let UD_UserData:String = "UDUserData"
let UD_DeviceToken:String = "UDDeviceToken"
let CatHint:String = "categoryhint"
let BankList:String = "banklist"

//MARK:- NOTIFICATION CENTER NAMES
let NC_DismissPopUp = "NC_DismissPopUp"

//MARK:- COREDATA ENTITY NAMES
let ENTITY_CHAT = "CD_Messages"

//MARK:- FONT NAMES
let FT_Light = "Montserrat-Light"
let FT_Medium = "Montserrat-Medium"
let FT_Regular = "Montserrat-Regular"
let FT_Bold = "Montserrat-Bold"

//MARK:- COLOR NAMES
let themePinkColor = Color_Hex(hex: "#ff317e")
let themeOrangeColor = Color_Hex(hex: "#ff8932")

//MARK - MESSAGES
let ServerResponseError = "Server Response Error"
let RetryMessage = "Something went wrong please try again..."
let PasswordSent = "Password is sent to your email."
let InternetNotAvailable = "Internet connection appears to be offline."
let EnterEmail = "Email address is required."
let EnterFname = "First Name is required."
let EnterLname = "Last Name is required."
let EnterCountry = "Country Name is required."
let EnterValidEmail = "Enter valid email address."
let EnterPassword = "Password is required."
let EnterPasswordInvalid = "Enter valid password."
let EnterOLDPassword = "Old Password is required."
let EnterNEWPassword = "New Password is required."
let EnterMinLengthPassword = "Minimum length of password is 6 character."
let EnterMinLengthMobile = "Minimum length of mobile is 6 character."
let EnterConfirmPassword = "Confirm password is required."
let EnterTerms = "You must be agree with Terms & Conditions."
let PasswordMismatch = "Password Mismatch."
let PasswordChange = "Password Changed Successfully"
let EnterPhone = "Mobile number is required."
let EnterValidPhone = "Enter Valid mobile number."
let EnterAddress = "Address is required."
let EnterUserName = "User name is required."
let EnterDOB = "Date of Birth is required."
let EnterVerification = "Verification Code is required."
let EnterCity = "City Name is required."
let PasswordRecovery = "Please check your mail box"
let EnterBetweenDate = "Please add valid age range."
let EnterGroupName = "Group name is required."
let EnterGroupDesc = "Description is required."
let EnterForgotPassMsg = "E-mail is required."
let ChooseCategory = "Choose category."
let ChooseSubCategory = "Choose subcategory."
let isFetchLocation = "isFetchLocation"
let ChooseImage = "Select your profile picture."
let EnterDescription = "Description is required."
let EnterCategory = "Main category is required."
let EnterRegion = "Region is required."
let EnterReward = "Reward Amount is required."
let EnterMinReward = "Reward Amount is not valid."
let EnterQuestion = "Minimum 1 question is required."
let EnterAnswer = "Answer is required."
let EnterFrom = "From date is required."
let EnterTo = "To date is required."
let EnterCardDetail = "Enter valid card detail."
let EnterAmount = "Amount is required."
let EnterValidAmount = "Enter valid Amount."
let EnterBankName = "Bank name is required."
let EnterAccountNo = "Account number is required."
let EnterPOPStation = "POPStation is required."
let EnterCourierAdd = "Courier address is required."
let EnterCourierDate = "Courier date is required."
let EnterCourierFormTime = "Courier from time is required."
let EnterCourierToTime = "Courier to time is required."


// PUSH TYPES
let PROOF_VERIFIED = "IdProof"
let UPDATE_STATUS = "update_status"
let PROOF_VERIFICATION_REJECT = "proof_verification_failed"
let PROOF_VERIFICATION_SUCCESS = "proof_verification_success"
let PROOF_VERIFICATION_PENDING = "proof_verification_pending"
let REPORT_STATUS_ONLINE = "Report_status_online"
let REPORT_STATUS_OFFLINE = "Report_status_offline"
let WIN_REWARD_AMOUNT = "WinRewardAmount"







