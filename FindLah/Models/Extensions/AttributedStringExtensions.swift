//
//  AttributedStringExtensions.swift
//  OneLifeToGo
//
//  Created by Admin on 06/04/18.
//  Copyright © 2018 el. All rights reserved.
//

import Foundation

public extension NSAttributedString{
    
    internal convenience init?(html: String) {
        guard let data = html.data(using: String.Encoding.utf16, allowLossyConversion: false) else {
            return nil
        }
        
        guard let attributedString = try?  NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) else {
            return nil
        }
        
        self.init(attributedString: attributedString)
    }
    
}
