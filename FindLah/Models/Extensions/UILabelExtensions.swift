//
//  UILabelExtensions.swift
//  CookerApp
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 EL. All rights reserved.
//

import Foundation
import UIKit

public extension UILabel
{
    func addLabelLeftPadding(_ image: UIImage,Text:String)
    {
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = image
        imageAttachment.bounds = CGRect(x: -10, y: -10, width: image.size.width, height: image.size.height)
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        let completeText = NSMutableAttributedString(string: "")
        completeText.append(attachmentString)
        let  textAfterIcon = NSMutableAttributedString(string: " " + Text)
        completeText.append(textAfterIcon)
        self.textAlignment = .left;
        self.attributedText = completeText;
    }
    
    func addShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 1.5
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
    }
}
