//
//  ImagePicker.swift
//  CookerCustomerApp
//
//  Created by Admin on 29/12/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Photos

protocol ImagePickerDelegate {
    func pickImageComplete(_ imageData: UIImage,sender:String)
}

class ImagePicker: NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    var delegate: ImagePickerDelegate?
    var strUserType = ""
    var senderName = String()
    let imagePicker = UIImagePickerController()
    var alertVC = UIAlertController()
    static let sharedInstance: ImagePicker = {
        let instance = ImagePicker()
        return instance
    }()
    
    override init() {
        super.init()
        
    }
    func selectImage(sender:String,presentComplete:@escaping (Bool)->())
    {
        alertVC = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alertVC.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
        senderName = sender
        let lblTitle:UILabel = UILabel(frame: CGRect(x: 0, y: 15.0, width: SCREENWIDTH() - 20, height: 25))
        lblTitle.font = FontWithSize(FT_Regular, 20)
        lblTitle.textAlignment = .center
        lblTitle.text = "Select Image"
        alertVC.view.addSubview(lblTitle)
        
        let btnGalleryImage:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        btnGalleryImage.setBackgroundImage(UIImage(named: "galleryicon"), for: .normal)
        btnGalleryImage.addTarget(self, action: #selector(ImagePicker.btnGalleryImage), for: UIControlEvents.touchUpInside)
        
        let btnCameraImage:UIButton = UIButton(frame: CGRect(x: 90, y: 0, width: 70, height: 70))
        btnCameraImage.setBackgroundImage(UIImage(named: "cameraicon"), for: .normal)
        btnCameraImage.addTarget(self, action: #selector(ImagePicker.btnCameraImage), for: UIControlEvents.touchUpInside)
        
        let view = UIView(frame: CGRect(x: (SCREENWIDTH() / 2) - 90, y: 60, width: 190, height: 80))
        view.addSubview(btnGalleryImage)
        view.addSubview(btnCameraImage)
        alertVC.view.addSubview(view)
        
        let alertControllerHeight:NSLayoutConstraint = NSLayoutConstraint(item: alertVC.view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 200)
        alertVC.view.addConstraint(alertControllerHeight);
        mostTopViewController?.present(alertVC, animated: true, completion: {
            presentComplete(true)
        })
    }
    func selectImage(sender:String)
    {
        alertVC = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alertVC.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
        senderName = sender
        let lblTitle:UILabel = UILabel(frame: CGRect(x: 0, y: 15.0, width: SCREENWIDTH() - 20, height: 25))
        lblTitle.font = FontWithSize(FT_Regular, 20)
        lblTitle.textAlignment = .center
        lblTitle.text = "Select Image"
        alertVC.view.addSubview(lblTitle)
        
        var usertype = "finder"
        if (UserDefaults.standard.object(forKey: kUserType) != nil) {
            usertype = UserDefaults.standard.object(forKey: kUserType) as! String
        }
   
        let btnGalleryImage:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        if usertype == "seeker" {
            btnGalleryImage.setBackgroundImage(UIImage(named: "redgallery"), for: .normal)
        } else {
            btnGalleryImage.setBackgroundImage(UIImage(named: "blueGallery"), for: .normal)
        }
        btnGalleryImage.addTarget(self, action: #selector(ImagePicker.btnGalleryImage), for: UIControlEvents.touchUpInside)
        
        let btnCameraImage:UIButton = UIButton(frame: CGRect(x: 90, y: 0, width: 70, height: 70))
        if usertype == "seeker" {
            btnCameraImage.setBackgroundImage(UIImage(named: "redcamera"), for: .normal)
        } else {
            btnCameraImage.setBackgroundImage(UIImage(named: "blueCamera"), for: .normal)
        }
        btnCameraImage.addTarget(self, action: #selector(ImagePicker.btnCameraImage), for: UIControlEvents.touchUpInside)
        
        if strUserType == FINDER {
            btnGalleryImage.setBackgroundImage(UIImage(named: "blueGallery"), for: .normal)
            btnCameraImage.setBackgroundImage(UIImage(named: "blueCamera"), for: .normal)
        }
        if strUserType == SEEKER {
            btnGalleryImage.setBackgroundImage(UIImage(named: "redgallery"), for: .normal)
            btnCameraImage.setBackgroundImage(UIImage(named: "redcamera"), for: .normal)
        }

        let view = UIView(frame: CGRect(x: (SCREENWIDTH() / 2) - 90, y: 60, width: 190, height: 80))
        view.addSubview(btnGalleryImage)
        view.addSubview(btnCameraImage)
        alertVC.view.addSubview(view)

        let alertControllerHeight:NSLayoutConstraint = NSLayoutConstraint(item: alertVC.view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 200)
        alertVC.view.addConstraint(alertControllerHeight);
        mostTopViewController?.present(alertVC, animated: true)
    }
    
     @objc func btnGalleryImage() {
        
        alertVC.dismiss(animated: true) {
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            mostTopViewController?.present(self.imagePicker, animated: true, completion: nil)
        }
        
        /*
        alertVC.dismiss(animated: true) {
            let alert = UIAlertController(style: .actionSheet)
            alert.addPhotoLibraryPicker(
                flow: .vertical,
                paging: false,
                selection: .single(action: { image in
                    let img = self.getAssetThumbnailImage(asset: image!)
                    self.delegate?.pickImageComplete(img,sender: self.senderName)
                }))
            alert.addAction(title: "Cancel", style: .cancel)
            alert.show()
        }*/
    }
    
    func getAssetThumbnailImage(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    @objc func btnCameraImage() {
        alertVC.dismiss(animated: true) {
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .camera
            mostTopViewController?.present(self.imagePicker, animated: true, completion: nil)
        }
    }

    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            let imgData : NSData = UIImageJPEGRepresentation(pickedImage,0.5)! as NSData
            delegate?.pickImageComplete(UIImage.init(data: imgData as Data!)!,sender: senderName)
        }
        mostTopViewController?.dismiss(animated: true, completion: nil)
    }
}
